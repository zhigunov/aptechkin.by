<?php

require_once('../config.php');
$link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

if (!$link) {
    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}


$handle = fopen("photos-mapping.txt", "r");
if ($handle) {
    $_t = 0;
    while (($line = fgets($handle)) !== false) {
        $row = explode("\t", $line);
        $id = $row[0];
        $image = trim($row[1]);


        $sql = "UPDATE oc_product SET image='" . $image . "' WHERE product_id=" . $id;

        if ($link->query($sql) === TRUE) {
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . $link->error . "\n";
        }
    }


    echo "Uploading finished\n";
} else {
    echo "Mappgin file missed\n";
}

mysqli_close($link);

echo "Exit\n";