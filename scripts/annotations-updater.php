<?php

require_once('../config.php');
$link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

if (!$link) {
    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
$lettersCount = 53;
for ($i = 1; $i <= $lettersCount; $i++) {
    $annotationString = file_get_contents('annotations/letter_' . $i . '.json');
    $annotations = json_decode($annotationString, true);
    foreach ($annotations as $annotation) {
        $title = $annotation['title'];
        $description = $annotation['description'];
        $description = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $description);
        $description = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $description);
        $description = preg_replace('#<!--  AdRiver code START(.*?)AdRiver code END  -->#is', '', $description);
//        preg_replace("/<!--  AdRiver code START(.*(\n)?)(.*(\n)?)(.*(\n)?)(.*(\n)?)(.*(\n)?)(.*(\n)?)(.*(\n)?)AdRiver code END  -->/", "", $description);

        $query = sprintf("UPDATE oc_product_description SET instruction='%s' WHERE name LIKE '%s'",
            mysqli_real_escape_string($link, $description),
            '%' . mysqli_real_escape_string($link, $title) . '%');
        $asd = 1;
        if ($link->query($query) === TRUE) {
            echo "Record updated successfully" . "\n";
        } else {
            echo "Error updating record: " . $link->error . "\n";
        }
    }

    echo "Uploading finished for letter_$i \n";
}

echo "Uploading finished\n";

mysqli_close($link);

echo "Exit\n";