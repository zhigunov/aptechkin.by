<?php
class ControllerModuleBrandsblocks extends Controller {
	public function index($setting) {
		$this->load->language('module/brandsblocks');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/manufacturer');

		$this->load->model('tool/image');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['name'] = $setting['name'];
		$data['manufacturers'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['manufacturer'])) {
			$manufacturers = array_slice($setting['manufacturer'], 0, (int)$setting['limit']);

			foreach ($manufacturers as $manufacturer_id) {
				$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

				if ($manufacturer_info) {
					if ($manufacturer_info['image']) {
						$image = $this->model_tool_image->resize($manufacturer_info['image'], $setting['width'], $setting['height']/3);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']/3);
					}
					$data['manufacturers'][] = array(
						'manufacturer_id'  => $manufacturer_info['manufacturer_id'],
						'thumb'       => $image,
						'name'        => $manufacturer_info['name'],
						'description' => $manufacturer_info['description'],
						'color_brands' => $manufacturer_info['color_brands'],
						'href'        => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer_info['manufacturer_id']),
						'height'	  => $setting['height'],
						'width'		  => $setting['width']
					);
				}
			}
		}

		if ($data['manufacturers']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/brandsblocks.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/brandsblocks.tpl', $data);
			} else {
				return $this->load->view('default/template/module/brandsblocks.tpl', $data);
			}
		}
	}
}