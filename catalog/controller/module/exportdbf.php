<?php
class ControllerModuleExportdbf extends Controller {
	private $error = array();

	public function index() {
		
	}

	public function addfile($event_data) {
		
		if ($event_data['order_status_id'] == 1){
			$def = array(
					array("POSTAV","C",20),
					array("NAIM","C",100),
					array("FIRMA","C",100),
					array("COUNTRY","C",50),
					array("KOLVO","N", 15, 0),
					array("CENAROC","N", 15, 2),
					array("PRICE","N", 15, 2),
					array("TYPE","C",25),
					array("KODTOV","N", 15, 0),
					array("PHONE","C", 25),
					array("ADDRESSAPT","C", 50),
					array("NUMBERAPT","C", 15),
			);
			
			
			$this->load->model('checkout/order');

			$order_info = $this->model_checkout_order->getOrder($event_data['order_id']);
			$adress = $order_info['payment_address_1'];
			$apteka_id_arr = explode("Апт№", $adress);
			$apteka_id = $apteka_id_arr[1];
			$telephone = $order_info['telephone'];
			
			$company = $order_info['payment_company'];

			$this->load->model('catalog/pharmacies');
			$pharmacies = $this->model_catalog_pharmacies->getPharmacies();
			
						
			if (!dbase_create('admin/export/' . $apteka_id . $company . $event_data['order_id'] . '.dbf', $def)) {
				echo "Ошибка, не получается создать базу данных\n";
			}

			$this->load->model('account/order');

			$products_from_order = array();
			
			$products_from_order = $this->model_account_order->getOrderProducts($event_data['order_id']);
			
			$db = dbase_open('admin/export/' . $apteka_id . $company . $event_data['order_id'] . '.dbf', 2);
			
			foreach ($products_from_order as $product){
				if ($db) {
				
					$this->load->model('catalog/product');
					$productextraattr = array();
					//_dump($this->model_catalog_product->getProduct($product['product_id']));
					foreach ($this->model_catalog_product->getProduct($product['product_id']) as $key => $value){
						if ($key == 'sku') {
							$productextraattr['sku'] = $value;
						}
						if ($key == 'manufacturer') {
							$productextraattr['manufacturer'] = $value;
						}
					}
					$temp = array();
					$temp = $this->model_catalog_product->getProductAttributes($product['product_id']);
					//var_dump($temp);
					foreach ($temp as $attribute_group){
						
						foreach ($attribute_group['attribute'] as $attribute) {
						
							if ($attribute['name'] == 'Страна'){
								$productextraattr['country'] = $attribute['text'];
							}
							if ($attribute['name'] == 'Поставщик'){
								$productextraattr['postav'] = $attribute['text'];
							}
							if ($attribute['name'] == 'CENAROC'){
								$productextraattr['CENAROC'] = $attribute['text'];
							}
						}
					}
								
					$this->load->model('catalog/product');		
					foreach ($this->model_catalog_product->getCategories($product['product_id']) as $item) {
						$categoryId = $item['category_id'];
					}
					$this->load->model('catalog/category');
					$category_info = $this->model_catalog_category->getCategory($categoryId);
					$categoryName = $category_info['name'];
					
					
					dbase_add_record($db, array(
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['postav']),
						iconv('UTF-8//IGNORE', 'CP866', $product['name']),
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['manufacturer']),
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['country']),
						(int)$product['quantity'],
						(float)$productextraattr['CENAROC'],
						(float)$product['price'],
						iconv('UTF-8//IGNORE', 'CP866', $categoryName),
						$productextraattr['sku'],
						iconv('UTF-8//IGNORE', 'CP866', $telephone),
						iconv('UTF-8//IGNORE', 'CP866', $adress),
						iconv('UTF-8//IGNORE', 'CP866', $apteka_id))
					);

				}
			}
			dbase_close($db);

		 	$filename = 'admin/export/' . $apteka_id . $company . $event_data['order_id'] . '.dbf';
			$ftp_server = 'unifarm.by';
			$ftp_user_name = 'aptechkin';
			$ftp_user_pass = '@pTecHUni18611';
			//имя файла на сервере после того, как вы его загрузите
			$name = $apteka_id . $company . $event_data['order_id'] . '.dbf';

			$conn_id = ftp_connect($ftp_server);
			
			// входим при помощи логина и пароля
			$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

			ftp_pasv($conn_id, true);

			// проверяем подключение
			if ((!$conn_id) || (!$login_result)) {
			       echo "FTP connection has failed!";
			       echo "Attempted to connect to $ftp_server for user: $ftp_user_name";
			       exit;
			   } 		

			// загружаем файл
			$upload = ftp_put($conn_id, '/'.$name, $filename, FTP_BINARY);
			// проверяем статус загрузки
			if (!$upload) {
			       echo "Error: FTP upload has failed!";
			   } 
			ftp_close($conn_id);



















			if (!dbase_create('admin/historyDBF/' . $apteka_id . $company . $event_data['order_id'] . '.dbf', $def)) {
				echo "Ошибка, не получается создать базу данных\n";
			}

			$this->load->model('account/order');

			$products_from_order = array();
			
			$products_from_order = $this->model_account_order->getOrderProducts($event_data['order_id']);
			
			$db = dbase_open('admin/historyDBF/' . $apteka_id . $company . $event_data['order_id'] . '.dbf', 2);
			
			foreach ($products_from_order as $product){
				if ($db) {
				
					$this->load->model('catalog/product');
					$productextraattr = array();
					//_dump($this->model_catalog_product->getProduct($product['product_id']));
					foreach ($this->model_catalog_product->getProduct($product['product_id']) as $key => $value){
						if ($key == 'sku') {
							$productextraattr['sku'] = $value;
						}
						if ($key == 'manufacturer') {
							$productextraattr['manufacturer'] = $value;
						}
					}
					$temp = array();
					$temp = $this->model_catalog_product->getProductAttributes($product['product_id']);
					//var_dump($temp);
					foreach ($temp as $attribute_group){
						
						foreach ($attribute_group['attribute'] as $attribute) {
						
							if ($attribute['name'] == 'Страна'){
								$productextraattr['country'] = $attribute['text'];
							}
							if ($attribute['name'] == 'Поставщик'){
								$productextraattr['postav'] = $attribute['text'];
							}
							if ($attribute['name'] == 'CENAROC'){
								$productextraattr['CENAROC'] = $attribute['text'];
							}
						}
					}
								
					$this->load->model('catalog/product');		
					foreach ($this->model_catalog_product->getCategories($product['product_id']) as $item) {
						$categoryId = $item['category_id'];
					}
					$this->load->model('catalog/category');
					$category_info = $this->model_catalog_category->getCategory($categoryId);
					$categoryName = $category_info['name'];
					
					
					dbase_add_record($db, array(
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['postav']),
						iconv('UTF-8//IGNORE', 'CP866', $product['name']),
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['manufacturer']),
						iconv('UTF-8//IGNORE', 'CP866', $productextraattr['country']),
						(int)$product['quantity'],
						(float)$productextraattr['CENAROC'],
						(float)$product['price'],
						iconv('UTF-8//IGNORE', 'CP866', $categoryName),
						$productextraattr['sku'],
						iconv('UTF-8//IGNORE', 'CP866', $telephone),
						iconv('UTF-8//IGNORE', 'CP866', $adress),
						iconv('UTF-8//IGNORE', 'CP866', $apteka_id))
					);

				}
			}
			dbase_close($db);

		}
	}
}