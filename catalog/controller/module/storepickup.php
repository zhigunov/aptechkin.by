<?php
class ControllerModuleStorePickup extends Controller {
	public $APIKey = 'AIzaSyDAWXZR2uzff6zaVUOmizC4m4pvETT3ERo';//AIzaSyBQfBlsuZWXpP_FKXgk9erPDMNZpiGIQLA';
	
	public function index() {
		// Load the needed language files
		$this->load->language('module/storepickup');
		
		// Load the needed models
		$this->load->model('setting/setting');
		
		// Get the module settings
		$moduleSettings = $this->model_setting_setting->getSetting('storepickup', 0);
		$moduleData = isset($moduleSettings['StorePickup']) ? $moduleSettings['StorePickup'] : array();
		
		// Check if the module is enabled
		if (isset($moduleData['Enabled']) && $moduleData['Enabled'] == 'no') {
			$this->response->redirect($this->url->link('common/home'));	
		}
		
		// Get the breadcrumbs
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/storepickup')
		);
		
		// Set the title and add additional scripts
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('https://maps.googleapis.com/maps/api/js?key='.$this->APIKey);
		
		// Set the variables which will be used in the front-end page
		$data['heading_title'] 			= $this->language->get('heading_title');
		$data['button_continue'] 		= $this->language->get('button_continue');
		$data['continue'] 				= $this->url->link('common/home');
		$data['language_id']			= $this->config->get('config_language_id');
		$stores							= !empty($moduleData['StoreData']) ? $moduleData['StoreData'] : array();
		$data['stores']					= array();
		$data['cities']					= array();
		foreach ($stores as $store) {
			if (isset($store) && ($store['Enabled']=='yes')) {
				$data['stores'][] 		= $store;	
			}
			$fl = 0;
			foreach ($data['cities'] as $city) {
				if (trim($store['City'][$data['language_id']]) == trim($city)){
					$fl = 1;
				}
			}
			if (($fl == 0) && ($store['Enabled']=='yes')){
				$data['cities'][] = $store['City'][$data['language_id']];
			}
		}
		
		$data['template']				= '/template/module/storepickup.tpl';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/storepickup.css')) {
			$this->document->addStyle('catalog/view/theme/'.$this->config->get('config_template').'/stylesheet/storepickup.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/storepickup.css');
		}
		
		// OpenCart layouts & header and footer
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		// Load the template
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . $data['template'])) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . $data['template'], $data));
		} else {
			$this->response->setOutput($this->load->view('default/' . $data['template'], $data));
		}
	}
}