<?php
class ControllerModuleiBlogWidget extends Controller {
	
	private $moduleName = 'iBlog';
	private $moduleNameSmall = 'iblog';
	private $moduleData_module = 'iblog_module';
	private $moduleModel = 'model_module_iblog';
	
	public function index($setting) {
		$this->language->load('module/'.$this->moduleNameSmall);
		$this->load->model('module/'.$this->moduleNameSmall);
		$this->load->model('setting/setting');

    	$data['heading_title']				= $this->language->get('heading_title');
		$data['no_featured']				= $this->language->get('no_featured');
		$data['no_posts']					= $this->language->get('no_posts');
		$data['iblog_button']				= $this->language->get('iblog_button');

		$data['posts']						= array();
		$data['post_id']					= !empty($this->request->get['post_id']) ? $this->request->get['post_id'] : 0;
		$posts								= $this->{$this->moduleModel}->getPosts();
		$data['featured_post']				= $this->{$this->moduleModel}->getFeaturedPost();
		$data['custom_css']					= $setting['custom_css'];
		$data['featured']					= $setting['featured'];


		foreach ($posts as $post) {
			foreach ($data['featured_post'] as $featured) {
				if ($post['iblog_post_id'] == $featured){
					$data['posts'][] = array(
						'post_id'	=> $post['iblog_post_id'],
						'title'		=> $post['title'],
						'image'		=> $post['image'],
						'excerpt'	=> $post['excerpt'],
						'href'		=> $this->url->link('module/'.$this->moduleNameSmall.'/post', 'post_id=' . $post['iblog_post_id'])
					);	
				}
			}
		}
		//СМС
		$dir = 'admin/export';
		$files = $this->myscandir($dir);
		//var_dump($files);

		foreach ($files as $file){
			//var_dump($this->getExtension1($file));
			if ($this->getExtension1($file) == 'txt'){
				$name_file = explode('.', $file);

				$order_mas = explode("A", $name_file[0]);
				if(isset($order_mas[1])){
					$order = $order_mas[1];
				}else{
					$order_mas = explode("B", $name_file[0]);
					if(isset($order_mas[1])){
						$order = $order_mas[1];
					}else{
						continue;
					}
				}
				
				//var_dump($order);
				$this->load->model('checkout/order');
				$order_info1 = $this->model_checkout_order->getOrder($order);
				$status = $order_info1['order_status_id'];
				//var_dump($order_info1);
				// echo '<pre>';
				// var_dump($status);
				if ($status != 3){
					$this->model_checkout_order->addOrderHistory($order, 3);
				}
			}
		}
		//
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/'.$this->moduleNameSmall.'-widget.tpl')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/'.$this->moduleNameSmall.'.css');
			return $this->load->view($this->config->get('config_template').'/template/module/'.$this->moduleNameSmall.'-widget.tpl', $data);
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/'.$this->moduleNameSmall.'.css');
			return $this->load->view('default/template/module/'.$this->moduleNameSmall.'-widget.tpl', $data);
		}  
  	}
  	//смс
  	function myscandir($dir, $sort=0)
	{
		$list = scandir($dir, $sort);

		// если директории не существует
		if (!$list) return false;

		// удаляем . и ..
		if ($sort == 0) unset($list[0],$list[1]);
		else unset($list[count($list)-1], $list[count($list)-1]);
		return $list;
	}

	function getExtension1($filename) {
		$res = explode('.', $filename);
		if (isset($res[1])){
			return $res[1];
		}
	}
	//
}
?>