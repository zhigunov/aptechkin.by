<?php

class ControllerFeedWebApi extends Controller {

	private $debug = false;

	public function categories() {
		$this->init();
		$this->load->model('catalog/category');
		$json = array('success' => true);

		# -- $_GET params ------------------------------
		
		if (isset($this->request->get['parent'])) {
			$parent = $this->request->get['parent'];
		} else {
			$parent = 0;
		}

		if (isset($this->request->get['level'])) {
			$level = $this->request->get['level'];
		} else {
			$level = 999;
		}

		# -- End $_GET params --------------------------


		$json['categories'] = $this->getCategoriesTree($parent, $level);

		if ($this->debug) {
			echo '<pre>';
			print_r($json);
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}

	public function category() {
		$this->init();
		$this->load->model('catalog/category');
		$this->load->model('tool/image');

		$json = array('success' => true);

		# -- $_GET params ------------------------------
		
		if (isset($this->request->get['id'])) {
			$category_id = $this->request->get['id'];
		} else {
			$category_id = 0;
		}

		# -- End $_GET params --------------------------

		$category = $this->model_catalog_category->getCategory($category_id);
		
		$json['category'] = array(
			'id'                    => $category['category_id'],
			'name'                  => $category['name'],
			'description'           => $category['description'],
			'href'                  => $this->url->link('product/category', 'category_id=' . $category['category_id'])
		);

		if ($this->debug) {
			echo '<pre>';
			print_r($json);
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}
		
	public function products() {
		$this->init();
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$json = array('success' => true, 'products' => array());


		# -- $_GET params ------------------------------
		if (isset($this->request->get['product_name'])) {
			$str = $this->request->get['product_name'];
		} else {
			$str = 'disable_filter';
		}

		if (isset($this->request->get['category'])) {
			$category_id = $this->request->get['category'];
		} else {
			$category_id = 0;
		}

		# -- End $_GET params --------------------------

		if($str != 'disable_filter'){
			$filter_mpn = ($this->config->get('config_ajaxadvancedsearch_mpn')) ? true : false;
			$filter_manufacturer = false;
			$filter_isbn = false;
			$filter_jan = false;
			$filter_ean = false;
			$filter_upc = false;
			$filter_sku = false;
			$filter_model = false;
			$filter_tag = false;

			$filterdata=array(
					'filter_name' => $str,
					'filter_mpn' => $filter_mpn,
					'filter_manufacturer' => $filter_manufacturer,
					'filter_isbn' => $filter_isbn,
					'filter_jan' => $filter_jan,
					'filter_ean' => $filter_ean,
					'filter_upc' => $filter_upc,
					'filter_sku' => $filter_sku,
					'filter_model' => $filter_model,
					'filter_tag' => $filter_tag,
					'start' => 0,
					'limit' => 10000,
				);

			if($category_id != 0){
				$filterdata['filter_category_id'] = $category_id;
			}

			$products = $this->model_catalog_product->ajaxLiveSearch($filterdata);

			foreach ($products as $product) {

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if ((float)$product['special']) {
					$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				$prescription = false;
				$country = null;
				$attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);
				foreach($attribute_groups as $attribute_group) { 
		            foreach($attribute_group['attribute'] as $attribute) {
				    	if ($attribute['attribute_id'] == 16){
				    		if($attribute['text'] == 'Да'){
				    			$prescription = true;
				    		}else{
				    			$prescription = false;
				    		}
				    	}
				    	if ($attribute['attribute_id'] == 12){
				    		$country = $attribute['text'];
				    	}
				    }
				}

				if ($product['quantity'] > 2){
		    		$format_count = 'Более 2 шт.';
		    	}elseif($product['quantity'] == 0){
		    		$format_count = 'Нет в наличии';
		    	}else{
		    		$format_count = $product['quantity'] . ' шт.';
		    	}

				$json['products'][] = array(
					'id'                    => $product['product_id'],
					'name'                  => $product['name'],
					'description'           => $product['description'],
					'price'                 => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'href'                  => $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'thumb'                 => $image,

					'manufacturer'			=> $product['manufacturer'],
					'manufacturer_country'	=> $country, 
					'custom_fields'			=> array( 
													array(
												      'type' => 'В наличии',
												      'value' => $format_count
												    ),
											  	),
					'prescription'			=> $prescription,

					'special'               => $special,
					'rating'                => $product['rating']
				);
			}
		}else{
			$products = $this->model_catalog_product->getProducts(array(
				'filter_category_id'	=> $category_id
			));

			foreach ($products as $product) {

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if ((float)$product['special']) {
					$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				$prescription = false;
				$country = null;
				$attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);
				foreach($attribute_groups as $attribute_group) { 
		            foreach($attribute_group['attribute'] as $attribute) {
				    	if ($attribute['attribute_id'] == 16){
				    		if($attribute['text'] == 'Да'){
				    			$prescription = true;
				    		}else{
				    			$prescription = false;
				    		}
				    	}
				    	if ($attribute['attribute_id'] == 12){
				    		$country = $attribute['text'];
				    	}
				    }
				}

				if ($product['quantity'] > 2){
		    		$format_count = 'Более 2 шт.';
		    	}elseif($product['quantity'] == 0){
		    		$format_count = 'Нет в наличии';
		    	}else{
		    		$format_count = $product['quantity'] . ' шт.';
		    	}

				$json['products'][] = array(
					'id'                    => $product['product_id'],
					'name'                  => $product['name'],
					'description'           => $product['description'],
					'price'                 => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'href'                  => $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'thumb'                 => $image,
					'manufacturer'			=> $product['manufacturer'],
					'manufacturer_country'	=> $country, 
					'custom_fields'			=> array( 
													array(
												      'type' => 'В наличии',
												      'value' => $format_count
												    ),
											  	),
					'prescription'			=> $prescription,
					'special'               => $special,
					'rating'                => $product['rating']
				);
			}
		}

		if ($this->debug) {
			echo '<pre>';
			print_r($json);
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}

	public function product() {
		$this->init();
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$json = array('success' => true);

		# -- $_GET params ------------------------------
		
		if (isset($this->request->get['id'])) {
			$product_id = $this->request->get['id'];
		} else {
			$product_id = 0;
		}

		# -- End $_GET params --------------------------

		$product = $this->model_catalog_product->getProduct($product_id);

		# product image
		if ($product['image']) {
			$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
		} else {
			$image = '';
		}

		#additional images
		$additional_images = $this->model_catalog_product->getProductImages($product['product_id']);
		$images = array();

		foreach ($additional_images as $additional_image) {
			$images[] = $this->model_tool_image->resize($additional_image, $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
		}

		#specal
		if ((float)$product['special']) {
			$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
		} else {
			$special = false;
		}

		#discounts
		$discounts = array();
		$data_discounts =  $this->model_catalog_product->getProductDiscounts($product['product_id']);

		foreach ($data_discounts as $discount) {
			$discounts[] = array(
				'quantity' => $discount['quantity'],
				'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product['tax_class_id'], $this->config->get('config_tax')))
			);
		}

		#options
		$options = array();

		foreach ($this->model_catalog_product->getProductOptions($product['product_id']) as $option) { 
			if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') { 
				$option_value_data = array();
				
				foreach ($option['option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product['tax_class_id'], $this->config->get('config_tax')));
						} else {
							$price = false;
						}
						
						$option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}
				
				$options[] = array(
					'product_option_id' => $option['product_option_id'],
					'option_id'         => $option['option_id'],
					'name'              => $option['name'],
					'type'              => $option['type'],
					'option_value'      => $option_value_data,
					'required'          => $option['required']
				);					
			} elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
				$options[] = array(
					'product_option_id' => $option['product_option_id'],
					'option_id'         => $option['option_id'],
					'name'              => $option['name'],
					'type'              => $option['type'],
					'option_value'      => $option['option_value'],
					'required'          => $option['required']
				);						
			}
		}

		#minimum
		if ($product['minimum']) {
			$minimum = $product['minimum'];
		} else {
			$minimum = 1;
		}

		$results = $this->model_catalog_product->getProductAnalogs();
		
		$productsss = array();

		$country = null;
		$prescription = false;
		$srok_godnosti = null;
		$active_ferment = null;

		$attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);
		foreach($attribute_groups as $attribute_group) { 
            foreach($attribute_group['attribute'] as $attribute) {
		    	if ($attribute['attribute_id'] == 12){
		    		$country = $attribute['text'];
		    	}
		    	if ($attribute['attribute_id'] == 16){
		    		if($attribute['text'] == 'Да'){
		    			$prescription = true;
		    		}else{
		    			$prescription = false;
		    		}
		    	}
		    	if ($attribute['attribute_id'] == 17){
		    		$srok_godnosti = $attribute['text'];
		    	}
		    	if ($attribute['attribute_id'] == 15){
		    		$active_ferment = $attribute['text'];
		            foreach($results as $result){
		            	if(stristr($attribute['text'], $result['text']) !== FALSE || stristr($result['text'], $attribute['text']) !== FALSE) {
		                	if ($result['product_id'] != $product['product_id']){
		                		$productsss[] = $result['product_id'];
		                	}
		                }
		            }
		    	}
		    }
		}
		if ($product['quantity'] > 2){
    		$format_count = 'Более 2 шт.';
    		$available = true;
    	}elseif($product['quantity'] == 0){
    		$format_count = 'Нет в наличии';
    		$available = false;
    	}else{
    		$format_count = $product['quantity'] . ' шт.';
    		$available = true;
    	}

    	if ($product['image']) {
			$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
		} else {
			$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
		}
		
		$data['analogs'] = array();
		foreach ($productsss as $analog_id) {
			$analog = $this->model_catalog_product->getProduct($analog_id);

			if ($analog['image']) {
				$image = $this->model_tool_image->resize($analog['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($analog['price'], $analog['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			if ((float)$analog['special']) {
				$special = $this->currency->format($this->tax->calculate($analog['special'], $analog['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$analog['special'] ? $analog['special'] : $analog['price']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$analog['rating'];
			} else {
				$rating = false;
			}

			$data['analogs'][] = array(
				'id'  => $analog_id,
				'manufacturer'=> $analog['manufacturer'],
				'attributies' => $this->model_catalog_product->getProductAttributes($analog_id),
				'image_url'       => $image,
				'name'        => $analog['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($analog['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'minimum'     => $analog['minimum'] > 0 ? $analog['minimum'] : 1,
				'rating'      => $analog['rating'],
				'href'        => $this->url->link('product/product&product_id=' . $analog['product_id'])
			);
		}

		$json['product'] = array(
			'id'                            => $product['product_id'],
			'name'                          => trim($product['name']),
			'available'						=> $available,
			'active_ferment'				=> $active_ferment,
			'count'  						=> $product['quantity'],
			'image_url'						=> $image,
			'custom_fields'					=> array( 
													array(
												      'type' => 'В наличии',
												      'value' => $format_count
												    ),
												    array(
												      'type' => 'Срок годности',
												      'value' => $srok_godnosti
												    )
											  	),
			'prescription'					=> $prescription,
			'manufacturer'                  => $product['manufacturer'],
			'manufacturer_country'			=> trim($country),
			'price'                         => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
			'analogues'						=> $data['analogs']
		);
		
		if ($this->debug) {
			echo '<pre>';
			print_r($json);
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}


	/**
	 * Generation of category tree
	 * 
	 * @param  int    $parent  Prarent category id
	 * @param  int    $level   Depth level
	 * @return array           Tree
	 */
	private function getCategoriesTree($parent = 0, $level = 1) {
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		
		$result = array();

		$categories = $this->model_catalog_category->getCategories($parent);

		if ($categories && $level > 0) {
			$level--;

			foreach ($categories as $category) {

				if ($category['image']) {
					$image = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
				} else {
					$image = false;
				}

				$result[] = array(
					'category_id'   => $category['category_id'],
					'parent_id'     => $category['parent_id'],
					'name'          => $category['name'],
					'image'         => $image,
					'href'          => $this->url->link('product/category', 'category_id=' . $category['category_id']),
					'categories'    => $this->getCategoriesTree($category['category_id'], $level)
				);
			}

			return $result;
		}
	}

	/**
	 * 
	 */
	private function init() {

		$this->response->addHeader('Content-Type: application/json');

		if (!$this->config->get('web_api_status')) {
			$this->error(10, 'API is disabled');
		}

		if ($this->config->get('web_api_key') && (!isset($this->request->get['key']) || $this->request->get['key'] != $this->config->get('web_api_key'))) {
			$this->error(20, 'Invalid secret key');
		}
	}

	/**
	 * Error message responser
	 *
	 * @param string $message  Error message
	 */
	private function error($code = 0, $message = '') {

		# setOutput() is not called, set headers manually
		header('Content-Type: application/json');

		$json = array(
			'success'       => false,
			'code'          => $code,
			'message'       => $message
		);

		if ($this->debug) {
			echo '<pre>';
			print_r($json);
		} else {
			echo json_encode($json);
		}
		
		exit();
	}


	public function pharmacies() {

		$this->load->model('setting/setting');
		
		// Get the module settings
		$moduleSettings = $this->model_setting_setting->getSetting('storepickup', 0);
		$moduleData = isset($moduleSettings['StorePickup']) ? $moduleSettings['StorePickup'] : array();
		$stores = !empty($moduleData['StoreData']) ? $moduleData['StoreData'] : array();
		foreach ($stores as $store) {
			

			$json['pharmacies'][] = array(
			'id'                    => $store['id'],

			'number'                => trim($store['Number']),
			'company'           	=> trim($store['Idseti'][2]),

			'city'                  => trim($store['City'][2]),
			'address'           	=> trim($store['Address'][2]),
			'work_time'				=> trim($store['Regime'][2]),
			'phones'				=> $store['Phone'],
			// 'director'				=> trim($pharmacy['director']),
			'coordinates'			=> array('lon' => $store['Longtitude'], 'lat' => $store['Latitude'])
			
		);
		}
		
		
		$this->response->setOutput(json_encode($json));
	}

	public function login() {
		$this->init();
		
		# -- $_GET params ------------------------------
		if (isset($this->request->get['telephone'])) {
			$telephone = $this->request->get['telephone'];
		} else {
			$telephone = '';
		}

		if (isset($this->request->get['password'])) {
			$password = $this->request->get['password'];
		} else {
			$password = '';
		}
		# -- End $_GET params --------------------------


		$this->load->language('checkout/checkout');

		$json = array();


		if (!$json) {
			$this->load->model('account/customer');

			// Check how many login attempts have been made.
			$login_info = $this->model_account_customer->getLoginAttempts($this->request->get['telephone']);

			if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
				$json['error']['warning'] = $this->language->get('error_attempts');
			}

			// Check if customer has been approved.
			$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->get['telephone']);

			if ($customer_info && !$customer_info['approved']) {
				$json['error']['warning'] = $this->language->get('error_approved');
			}

			if (!isset($json['error'])) {
				if (!$this->customer->login($telephone, $password)) {
					$json['error']['warning'] = $this->language->get('error_login');

					$this->model_account_customer->addLoginAttempt($this->request->get['telephone']);
				} else {
					$this->model_account_customer->deleteLoginAttempts($this->request->get['telephone']);
				}
			}
		}

		if (!$json) {
			// Trigger customer pre login event
			$this->event->trigger('pre.customer.login');

			// Unset guest
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);

			// Trigger customer post login event
			$this->event->trigger('post.customer.login');
			$json = array();
			$json['success'] = true;
			$json['data'] = array("customer_id" => $this->customer->getId(),
							    "store_id" => $customer_info['store_id'],
							    "firstname" => $customer_info['firstname'],
							    "lastname" => $customer_info['lastname'],
							    "email" => $customer_info['email'],
							    "telephone" => $customer_info['telephone'],
							    //"cart" => "a:0:{}",
							    //"wishlist" => "a:0:{}",
							    "customer_group_id" => $customer_info['customer_group_id'],
							    "ip" => $customer_info['ip'],
							    "status" => $customer_info['status'],
							    "approved" => $customer_info['approved'],
							    "date_added" => $customer_info['date_added'],
							    "session" => $this->session->getId()
				);
		}

		$this->response->setOutput(json_encode($json));
	}





	public function register() {
		$this->init();
		
		# -- $_GET params ------------------------------
		if (isset($this->request->get['telephone'])) {
			$telephone = $this->request->get['telephone'];
		} else {
			$telephone = '';
		}
		# -- End $_GET params --------------------------

		$password = rand(1000, 9999);
		$data['telephone'] = $telephone;
		$data['password'] = $password;
		$this->load->model('account/customer');
		$json = array();
		if ($this->model_account_customer->getCustomerByTelephone($telephone)) {
			$json['exist'] = true;
			$this->model_account_customer->editPassword($this->request->get['telephone'], $data['password']);
				/*if( $curl = curl_init() ) {
				  	curl_setopt($curl, CURLOPT_URL, 'https://userarea.sms-assistent.by/api/v1/send_sms/plain');
				  	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				  	curl_setopt($curl, CURLOPT_POST, true);
				  	curl_setopt($curl, CURLOPT_POSTFIELDS, "user=RED_mark&password=DK878s12&recipient=$telephone&message=Ваш пароль для входа: $password&sender=CMC");
				  	$status = curl_exec($curl);
				  	curl_close($curl);

			  	}*/
			  	$fp = fopen("register.txt", "a"); // Открываем файл в режиме записи 
				$mytext = "$telephone : " . "$password \r\n"; // Исходная строка
				$test = fwrite($fp, $mytext); // Запись в файл
				fclose($fp); //Закрытие файла
		}
		
		if (!$json) {
			$customer_id = $this->model_account_customer->addCustomertel($data);
		
			$fp = fopen("register.txt", "a"); // Открываем файл в режиме записи 
			$mytext = "$telephone : " . "$password \r\n"; // Исходная строка
			$test = fwrite($fp, $mytext); // Запись в файл
			fclose($fp); //Закрытие файла

			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $customer_id,
				'name' => $this->request->get['telephone']
			);

			$this->model_account_activity->addActivity('register', $activity_data);
			$json['exist'] = false;
		}
		
		$json['success'] = true;
				
		$this->response->setOutput(json_encode($json));
	}

	public function cart_add() {
		$this->load->language('api/cart');

		$json = array();

		if (isset($this->request->get['product_id'])) {
			$this->load->model('catalog/product');

			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

			if ($product_info) {
				if (isset($this->request->get['quantity'])) {
					$quantity = $this->request->get['quantity'];
				} else {
					$quantity = 1;
				}
	
				if (isset($this->request->post['option'])) {
					$option = array_filter($this->request->post['option']);
				} else {
					$option = array();
				}

				$product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);

				foreach ($product_options as $product_option) {
					if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
						$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
					}
				}

				if (!isset($json['error']['option'])) {
					$this->add_api($this->request->get['product_id'], $quantity, $option, $recurring_id = 0, $this->request->get['session_id'], $this->request->get['customer_id']);

					$json['success'] = true;
					$json['message'] = $this->language->get('text_success');

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				}
			} else {
				$json['success'] = false;
				$json['message'] = $this->language->get('error_store');
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}



	public function cart_remove() {
		$this->load->language('api/cart');

		$json = array();

		// Remove
		if ((isset($this->request->get['product_id'])) && (isset($this->request->get['customer_id']))) {
			$this->remove_api($this->request->get['product_id'], $this->request->get['customer_id']);

			$json['success'] = true;

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
		}else{
			$json['success'] = false;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}




	public function cart_products() {
		$this->load->language('api/cart');

		$this->load->model('tool/image');
		$json = array();


		if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
			$json['error']['stock'] = $this->language->get('error_stock');
		}

		// Products
		$json['products'] = array();

		$products = $this->getProductsS($this->request->get['customer_id']);

		foreach ($products as $product) {
			$product_total = 0;

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				$option_data[] = array(
					'product_option_id'       => $option['product_option_id'],
					'product_option_value_id' => $option['product_option_value_id'],
					'name'                    => $option['name'],
					'value'                   => $option['value'],
					'type'                    => $option['type']
				);
			}
			$prescription = false;
			$this->load->model('catalog/product');
			$attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);
			foreach($attribute_groups as $attribute_group) { 
	            foreach($attribute_group['attribute'] as $attribute) {
			    	if ($attribute['attribute_id'] == 12){
			    		$country = $attribute['text'];
			    	}
			    	if ($attribute['attribute_id'] == 16){
			    		if($attribute['text'] == 'Да'){
			    			$prescription = true;
			    		}else{
			    			$prescription = false;
			    		}
			    	}
			    }
			}

			$json['products'][] = array(
				'cart_id'    => $product['cart_id'],
				'product_id' => $product['product_id'],
				'name'       => $product['name'],
				'thumb'		 => $image,
				'prescription'	=> $prescription,
				'quantity'   => $product['quantity'],
				'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				'manufacturer'                  => $product['manufacturer'],
				'manufacturer_country'			=> trim($country),
				'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
				'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
				'reward'     => $product['reward']
			);
		}

		$json['totals'] = array();
		$sum = 0;

		foreach ($json['products'] as $product) {
			$sum += $product['total'];
		}
		$json['totals'][] = array(
				'title' => "Сумма",
				'text'  => $this->currency->format($sum)
			);


		if (!empty($json['products'])){
			$json = array_merge( array( 'success' => true ), $json );
		}else{
			$json = array();
			$json['success'] = false;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
		
	public function get_order() {
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		
		$json = array();

		if (isset($this->request->get['order_id'])) {
			$order_id = (int)$this->request->get['order_id'];

			// $json['orders'] = $this->getOrdersList($customer_id);

			$order_cart = $this->getOrderCart($order_id);
			$count = 0;
			$sum = 0;
			foreach ($order_cart as $value) {
				$count += $value['quantity'];
				$sum += $value['price']*$value['quantity'];
			}

			$orders_data = array();
			$order_query = $this->db->query("SELECT date_added, payment_address_1 FROM `" . DB_PREFIX . "order` WHERE order_id = '" . $order_id . "'");

			$json = array(
				'date' 				=> $order_query->row['date_added'],
				'products' 			=> $this->getOrderCart($order_id),
				'order_info'		=> 'Итого: ' . $count . 'шт. (' . $sum . 'р)'
			);

			$apteka_id_arr = explode("Апт№", $order_query->row['payment_address_1']);
			$apteka_id = $apteka_id_arr[1];


			$this->load->model('setting/setting');
		
			$moduleSettings = $this->model_setting_setting->getSetting('storepickup', 0);
			$moduleData = isset($moduleSettings['StorePickup']) ? $moduleSettings['StorePickup'] : array();
			$stores = !empty($moduleData['StoreData']) ? $moduleData['StoreData'] : array();
			foreach ($stores as $store) {
				if($store['id'] == $apteka_id)
				$json['pharmacy_address'] = trim($store['Address'][2]);
				$json['pharmacy_phones'] = $store['Phone'];
				$json['pharmacy_schedule'] = trim($store['Regime'][2]);
			}

			$json = array_merge( array( 'success' => true ), $json );
		}else{
			$json = array();
			$json['success'] = false;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function orders_get() {
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		
		$json = array();

		// Products
		$json['orders'] = array();

		if (isset($this->request->get['customer_id'])) {
			$customer_id = (int)$this->request->get['customer_id'];

			$json = array();
			$json['orders'] = $this->getOrdersList($customer_id);
			
			$json = array_merge( array( 'success' => true ), $json );
		}else{
			$json = array();
			$json['success'] = false;
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getOrdersList($customer_id) {
		$orders_data = array();
		$order_query = $this->db->query("SELECT o.order_id, o.firstname, o.lastname, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . $customer_id . "'");
		
		foreach($order_query->rows as $order)
		{
			$order_cart = $this->getOrderCart($order['order_id']);
			$count = 0;
			$sum = 0;
			foreach ($order_cart as $value) {
				$count += $value['quantity'];
				$sum += $value['price']*$value['quantity'];
			}
			$order_data = array(
				'order_id' => $order['order_id'],
				'order_status' => $order['status'],
				'date_added' => $order['date_added'],
				// 'order_cart' => $this->getOrderCart($order['order_id'])
				'order_info'	=> $count . 'шт. (' . $sum . 'р)'
			);
			
			$orders_data[] = array('order' => $order_data);
		}
		return $orders_data;
	}

	public function getOrderCart($order_id) {
		$this->load->model('tool/image');
		$this->load->model('catalog/product');

		$orderCart_data = array();
		$orderCart_query = $this->model_account_order->getOrderProducts($order_id);
		
		foreach($orderCart_query as $orderProduct) 
		{
			$product = $this->model_catalog_product->getProduct($orderProduct['product_id']);

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			$prescription = false;
			$country = null;
			$attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);
			foreach($attribute_groups as $attribute_group) { 
	            foreach($attribute_group['attribute'] as $attribute) {
			    	if ($attribute['attribute_id'] == 16){
			    		if($attribute['text'] == 'Да'){
			    			$prescription = true;
			    		}else{
			    			$prescription = false;
			    		}
			    	}
			    	if ($attribute['attribute_id'] == 12){
			    		$country = $attribute['text'];
			    	}
			    }
			}

			if ($product['quantity'] > 2){
	    		$format_count = 'Более 2 шт.';
	    	}elseif($product['quantity'] == 0){
	    		$format_count = 'Нет в наличии';
	    	}else{
	    		$format_count = $product['quantity'] . ' шт.';
	    	}

			$orderCart_data[]  = array(
				'id'                    => $product['product_id'],
				'name'                  => $product['name'],
				'description'           => $product['description'],
				'price'                 => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
				'href'                  => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'thumb'                 => $image,

				'manufacturer'			=> $product['manufacturer'],
				'manufacturer_country'	=> $country, 
				'custom_fields'			=> array( 
												array(
											      'type' => 'В наличии',
											      'value' => $format_count
											    ),
										  	),
				'prescription'			=> $prescription,
				'quantity' => $orderProduct['quantity'],
				'special'               => $special,
				'rating'                => $product['rating']
			);



			// $orderCart_data[] = array(
			// 	'product_id' => $orderProduct['product_id'],
			// 	'name' => $orderProduct['name'],
			// 	'description' => $t['description'],
			// 	'quantity' => $orderProduct['quantity'],
			// 	'price' => $orderProduct['price']
			// );
		}
		return $orderCart_data;
	}


	public function add_api($product_id, $quantity = 1, $option = array(), $recurring_id = 0, $session_id, $customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

		if (!$query->row['total']) {
			$this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$customer_id . "', session_id = '" . $this->db->escape($session_id) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE customer_id = '" . (int)$customer_id . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
		}
	}

	public function remove_api($product_id, $customer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$customer_id . "'");
	}

	


	public function getProductsS($customer_id) {
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "'");

		foreach ($cart_query->rows as $cart) {
			$stock = true;

			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

			if ($product_query->num_rows && ($cart['quantity'] > 0)) {
				$option_price = 0;
				$option_points = 0;
				$option_weight = 0;

				$option_data = array();

				foreach (json_decode($cart['option']) as $product_option_id => $value) {
					$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($option_query->num_rows) {
						if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
							$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_value_query->num_rows) {
								if ($option_value_query->row['price_prefix'] == '+') {
									$option_price += $option_value_query->row['price'];
								} elseif ($option_value_query->row['price_prefix'] == '-') {
									$option_price -= $option_value_query->row['price'];
								}

								if ($option_value_query->row['points_prefix'] == '+') {
									$option_points += $option_value_query->row['points'];
								} elseif ($option_value_query->row['points_prefix'] == '-') {
									$option_points -= $option_value_query->row['points'];
								}

								if ($option_value_query->row['weight_prefix'] == '+') {
									$option_weight += $option_value_query->row['weight'];
								} elseif ($option_value_query->row['weight_prefix'] == '-') {
									$option_weight -= $option_value_query->row['weight'];
								}

								if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
									$stock = false;
								}

								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => $value,
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => $option_value_query->row['option_value_id'],
									'name'                    => $option_query->row['name'],
									'value'                   => $option_value_query->row['name'],
									'type'                    => $option_query->row['type'],
									'quantity'                => $option_value_query->row['quantity'],
									'subtract'                => $option_value_query->row['subtract'],
									'price'                   => $option_value_query->row['price'],
									'price_prefix'            => $option_value_query->row['price_prefix'],
									'points'                  => $option_value_query->row['points'],
									'points_prefix'           => $option_value_query->row['points_prefix'],
									'weight'                  => $option_value_query->row['weight'],
									'weight_prefix'           => $option_value_query->row['weight_prefix']
								);
							}
						} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
							foreach ($value as $product_option_value_id) {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}

									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}

									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
										$stock = false;
									}

									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $product_option_value_id,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);
								}
							}
						} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => '',
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
								'weight_prefix'           => ''
							);
						}
					}
				}

				$price = $product_query->row['price'];

				// Product Discounts
				$discount_quantity = 0;

				$cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "'");

				foreach ($cart_2_query->rows as $cart_2) {
					if ($cart_2['product_id'] == $cart['product_id']) {
						$discount_quantity += $cart_2['quantity'];
					}
				}

				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				// Downloads
				$download_data = array();

				$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$cart['product_id'] . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

				foreach ($download_query->rows as $download) {
					$download_data[] = array(
						'download_id' => $download['download_id'],
						'name'        => $download['name'],
						'filename'    => $download['filename'],
						'mask'        => $download['mask']
					);
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
					$stock = false;
				}

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($recurring_query->num_rows) {
					$recurring = array(
						'recurring_id'    => $cart['recurring_id'],
						'name'            => $recurring_query->row['name'],
						'frequency'       => $recurring_query->row['frequency'],
						'price'           => $recurring_query->row['price'],
						'cycle'           => $recurring_query->row['cycle'],
						'duration'        => $recurring_query->row['duration'],
						'trial'           => $recurring_query->row['trial_status'],
						'trial_frequency' => $recurring_query->row['trial_frequency'],
						'trial_price'     => $recurring_query->row['trial_price'],
						'trial_cycle'     => $recurring_query->row['trial_cycle'],
						'trial_duration'  => $recurring_query->row['trial_duration']
					);
				} else {
					$recurring = false;
				}

				$manufacturer = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_query->row['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");


				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'name'            => $product_query->row['name'],
					'model'           => $product_query->row['model'],
					'shipping'        => $product_query->row['shipping'],
					'image'           => $product_query->row['image'],
					'option'          => $option_data,
					'download'        => $download_data,
					'quantity'        => $cart['quantity'],
					'minimum'         => $product_query->row['minimum'],
					'subtract'        => $product_query->row['subtract'],
					'manufacturer'    => $manufacturer->row['manufacturer'],
					'stock'           => $stock,
					'price'           => ($price + $option_price),
					'total'           => ($price + $option_price) * $cart['quantity'],
					'tax'             => 0,
					'reward'          => $reward * $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
					'tax_class_id'    => $product_query->row['tax_class_id'],
					'weight'          => ($product_query->row['weight'] + $option_weight) * $cart['quantity'],
					'weight_class_id' => $product_query->row['weight_class_id'],
					'length'          => $product_query->row['length'],
					'width'           => $product_query->row['width'],
					'height'          => $product_query->row['height'],
					'length_class_id' => $product_query->row['length_class_id'],
					'recurring'       => $recurring
				);
			} else {
				$this->remove($cart['cart_id']);
			}
		}

		return $product_data;
	}





	public function addOrder(){
		# -- $_GET params ------------------------------
		if (isset($this->request->get['customer_id'])) {
			$customer_id = $this->request->get['customer_id'];
		} else {
			$customer_id = '';
		}
		
		if (isset($this->request->get['pharmacy_id'])) {
			$pharmacy_id = $this->request->get['pharmacy_id'];
		} else {
			$pharmacy_id = '';
		}
		# -- End $_GET params --------------------------




		$this->load->model('setting/setting');
		
		// Get the module settings
		$moduleSettings = $this->model_setting_setting->getSetting('storepickup', 0);
		$moduleData = isset($moduleSettings['StorePickup']) ? $moduleSettings['StorePickup'] : array();
		$stores = !empty($moduleData['StoreData']) ? $moduleData['StoreData'] : array();
		foreach ($stores as $store) {
			if($pharmacy_id == $store['id']){
				$payment_address = trim($store['Address'][2]) . ' Апт№' . trim($store['Number']);
				$payment_company = trim($store['Idseti'][2]);
			}

		}




		
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomer($customer_id);
		$total = 0;
		foreach ($this->getProductsS($customer_id) as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		$order = array(
			'invoice_prefix' => $this->config->get('config_invoice_prefix'),
			'store_id' => $this->config->get('config_store_id'),
			'store_name' => $this->config->get('config_name'),
			'store_url' => $this->config->get('config_url'),
			'customer_id' => (int)$customer_id,
			'customer_group_id' => $customer_info['customer_group_id'],
			'firstname' => $customer_info['firstname'],
			'lastname' => $customer_info['lastname'],
			'email' => $customer_info['email'],
			'telephone' => $customer_info['telephone'],
			'fax' => '',
			'shipping_firstname' => '',
			'shipping_lastname' => '',
			'shipping_company' => '',
			'shipping_address_1' => '',
			'shipping_address_2' => '',
			'shipping_city' => '',
			'shipping_postcode' => '',
			'shipping_country' => 0,
			'shipping_country_id' => '',
			'shipping_zone' => 0,
			'shipping_zone_id' => '',
			'shipping_address_format' => '',
			'shipping_method' => '',
			'shipping_code' => '',
			'payment_firstname' => '',
			'payment_lastname' => '',
			'payment_company' => $payment_company,
			'payment_address_1' => $payment_address,
			'payment_address_2' => '',
			'payment_city' => '',
			'payment_postcode' => '',
			'payment_country' => '',
			'payment_country_id' => '',
			'payment_zone' => '',
			'payment_zone_id' => '',
			'payment_address_format' => '',
			'payment_method' => 'Оплата при доставке',
			'payment_code' => 'cod',
			'payment_company_id' => 0,
			'payment_tax_id' => 0,
			'comment' => '',
			'affiliate_id' => '0',
			'commission' => '0.00',
			'marketing_id' => 0, 
			'tracking' => '', 
			'language_id' => (int)$this->config->get('config_language_id'),
			'currency_id' => $this->currency->getId(),
			'currency_code' => $this->currency->getCode(),
			'currency_value' => $this->currency->getValue($this->currency->getCode()),
			'ip' => $this->request->server['REMOTE_ADDR'],
			'forwarded_ip' => '',
			'accept_language' => '',
			'products' => $this->getProductsS($customer_id),
			'vouchers' => array(),
			'total' => $total,
			'totals' => array(
				array(
					'code' => 'sub_total',
					'title' => 'Сумма',
					'value' => $total,
					'sort_order' => '1',
				),
				array(
					'code' => 'total',
					'title' => 'Итого',
					'value' => $total,
					'sort_order' => '9',
				)
			),
		);
		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$order['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
		} else {
			$order['user_agent'] = '';
		}

		$this->load->model('checkout/order');
		$order_id = $this->model_checkout_order->addOrder($order);

		$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('cod_order_status_id'));


		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "'");


		$json = array();
		$json['success'] = true;
		
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
