<?php
class ControllerToolImport extends Controller {
	private $error = array();

	public function index() {


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$ftp_server = 'unifarm.by';
		$ftp_user_name = 'aptechkin';
		$ftp_user_pass = '@pTecHUni18611';
	
		$remote_file = 'import/specpric.DBF';
		$remote_file1 = 'import/specpric.dbf';
		$local_file = 'admin/export/import/specpric.DBF';

		// открываем файл для записи
		$handle = fopen($local_file, 'w');

		// установка соединения
		$conn_id = ftp_connect($ftp_server);

		// вход с именем пользователя и паролем
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		// пытаемся скачать файл и сохранить его в $handle
		if (ftp_fget($conn_id, $handle, $remote_file, FTP_BINARY)) {
			
		} else {
			ftp_fget($conn_id, $handle, $remote_file1, FTP_BINARY);
		}

		// закрытие соединения и локального файла
		ftp_close($conn_id);
		fclose($handle);


		// Открываем созданный dbf-файл
	    $dbh = dbase_open("admin/export/import/specpric.DBF",2);
	    if(!$dbh){
	    	$dbh = dbase_open("admin/export/import/specpric.dbf",2);
	    }
	    if(!$dbh) exit("Ошибка - невозможно открыть файл");
	    // Число записей в файле
	    $numrecords = dbase_numrecords($dbh);
	    // В цикле заполняем массив $arr записями из файла
	    for($i = 1; $i < $numrecords; $i++)
	    {
	      $arr[] = dbase_get_record_with_names($dbh, $i);
	    }
		//var_dump($arr);
	    foreach ($arr as $arkey => $arvalue) {
	      	foreach ($arvalue as $key => $value) {
		      	if ($key == "POSTAV"||$key == "NAIM"||$key == "FIRMA"||$key == "COUNTRY"||$key == "TYPE"||$key == "ATX3"||$key == "MNN"||$key == "SRGOD"){
		      		$arr[$arkey][$key] = iconv('CP866', 'UTF-8//IGNORE', $value);

		      	}
				if ($key == "USLOV"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "БезРецепта"){
						$arr[$arkey][$key] = "Нет";
					}
					if(trim($value) == "RX"){
						$arr[$arkey][$key] = "Да";
					}
				}
				if ($key == "SRGOD"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == ".  ."){
						$arr[$arkey][$key] = "Не имеет огрничений";
					}
				}
				if ($key == "TYPE"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "Лек"){
						$arr[$arkey][$key] = "Лекарственные средства";
					}
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "НДС"){
						$arr[$arkey][$key] = "БАД, косметика и др.";
					}
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "ИМН"){
						$arr[$arkey][$key] = "Изделия медицинского назанчения";
					}
				}
				if (trim(iconv('CP866', 'UTF-8//IGNORE', $arvalue['TYPE'])) != 'Лек'){
					$arr[$arkey]['MNN'] = "";
				}
			}
	    }

	    // Закрываем dbf-файл
	    dbase_close($dbh);

		$this->load->model('tool/import');
		$manufacturers = array();
		$manufacturers = $this->model_tool_import->getManufacturers();

		$manufacturers_to_add = array();
		$flman = 0;
		$flman_to_add = 0;
		foreach ($arr as $arkey => $arvalue) {
	      	foreach ($arvalue as $key => $value) {
		      	if ($key == "FIRMA"){
		      		foreach ($manufacturers as $keym => $valuem) {
		      			if(strcasecmp($keym, trim($value)) == 0){
		      				$flman = 1;
			      		}
			      	}
			      	if ($flman == 0){
			      		foreach ($manufacturers_to_add as $value_to_add) {
			      			if (strcasecmp($value_to_add, trim($value)) == 0){
			      				$flman_to_add = 1;
			      			}
			      		}
			      		if ($flman_to_add == 0){
			      			$manufacturers_to_add[] = addslashes(trim($value));
			      		}
			      	}
			      	$flman = 0;
					$flman_to_add = 0;
		      	}
		    }
	    }
	    //var_dump($manufacturers_to_add);
	    $database =& $this->db;
	    //var_dump($manufacturers_to_add);
		$this->model_tool_import->storeManufacturersIntoDatabase($database, $manufacturers_to_add);
		//$this->model_tool_import->delManufacturersIntoDatabase($database);


		$categories = $this->model_tool_import->getCategories();

		$categories_to_add = array();
		$flcat = 0;
		$flcat_to_add = 0;
		foreach ($arr as $arkey => $arvalue) {
			foreach ($arvalue as $key => $value) {
				if ($key == "TYPE"){
					foreach ($categories as $keyc => $valuec) {
						if($keyc == trim($value)){
							$flcat = 1;
						}
					}
					if ($flcat == 0){
						foreach ($categories_to_add as $value_to_add) {
							if ($value_to_add['name'] == trim($value)){
								$flcat_to_add = 1;
							}
						}
						if ($flcat_to_add == 0){
							$categories_to_add[] = array('parent' => 0, 'name' => addslashes(trim($value)));
						}
					}
					$flcat = 0;
					$flcat_to_add = 0;
				}
			}
		}
		$this->model_tool_import->storeCategoriesIntoDatabase($database, $categories_to_add);
		$categories = $this->model_tool_import->getCategories();
		$categories_to_add = array();
		foreach ($arr as $arkey => $arvalue) {
			foreach ($arvalue as $key => $value) {
				if ($key == "ATX3"){
					foreach ($categories as $keyc => $valuec) {
						if($keyc == trim($value)){
							$flcat = 1;
						}
					}
					if ($flcat == 0){
						foreach ($categories_to_add as $value_to_add) {
							if ($value_to_add['name'] == trim($value)){
								$flcat_to_add = 1;
							}
						}
						if ($flcat_to_add == 0){
							$trim = trim($arvalue['TYPE']);
							$parent = $this->model_tool_import->getCategoryId($trim);

							$categories_to_add[] = array('parent' => $parent, 'name' => addslashes(trim($value)));
						}
					}
					$flcat = 0;
					$flcat_to_add = 0;
				}
			}
		}

		$database =& $this->db;

		$this->model_tool_import->storeCategoriesIntoDatabase($database, $categories_to_add);
		//$this->model_tool_import->delCategoriesIntoDatabase($database);

		//var_dump($arr);
		$true = $this->model_tool_import->storeProductsIntoDatabase($database, $arr);
        if ($true = TRUE){
			$data['result_import'] = 'Импорт успешно завершен';
		}else{
			$data['result_import'] = 'При импорте произошла ошибка';
		}




		//103.by

	    $filename = "admin/import103by/103.DBF";
		//Столбцы
		$def = array(
		array("ID","N",10,0),
		array("NAIM","C",128,0),
		array("FIRMA","C",128,0),
		array("COUNTRY","C",128,0),
		array("PRICE","C",32,0),
		array("KOLVO","C",32,0),
		array("HREF","C",255,0)
		);
		//Создем файл
		 $DBF = dbase_create($filename, $def);
		
		$arr103 = array();
		foreach ($arr as $arkey => $arvalue) {
	      	foreach ($arvalue as $key => $value) {
				if ($key == "KOLVO"){
					if($value > 2){
						$arr103[$arkey][$key] = 10;
						$arr103[$arkey][$key] = iconv('UTF-8', 'CP866//IGNORE', $arr103[$arkey][$key]);
					}else{
						$arr103[$arkey][$key] = $value;
					}
				}elseif($key == "NAIM"||$key == "FIRMA"||$key == "COUNTRY"){
					$arr103[$arkey][$key] = iconv('UTF-8', 'CP866//IGNORE', $value);
				}elseif($key == "PRICE"){
					$arr103[$arkey][$key] = str_replace('.', ',', (string)$value);
				}else{
					$arr103[$arkey][$key] = $value;
				}
			}
			// $href = $this->url->link('product/product', 'product_id=' . $arr103[$arkey]['KODTOV']) . '&partner=103.by';
			$href = 'http://aptechkin.by/index.php?route=product/product&product_id=' . $arr103[$arkey]['KODTOV'] . '&partner=103.by';
			//Добавляем строку в файл dbf
			 dbase_add_record($DBF, array($arr103[$arkey]['KODTOV'], $arr103[$arkey]['NAIM'], $arr103[$arkey]['FIRMA'], $arr103[$arkey]['COUNTRY'], $arr103[$arkey]['PRICE'], $arr103[$arkey]['KOLVO'], $href));
	    }
	    // var_dump($arr103);
		//Закрываем указатель на файл
		 dbase_close($DBF);

		//имя файла, который нужно загрузить
		$ftp_server = 'ftp.103.by';
		$ftp_user_name = 'aptechkin';
		$ftp_user_pass = 'E9HxiXiXpQxg';
		//имя файла на сервере после того, как вы его загрузите
		$name = 'specpric.DBF';

		$conn_id = ftp_connect($ftp_server);
		
		// входим при помощи логина и пароля
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		ftp_pasv($conn_id, true);

		// проверяем подключение
		if ((!$conn_id) || (!$login_result)) {
		       echo "FTP connection has failed!";
		       echo "Attempted to connect to $ftp_server for user: $ftp_user_name";
		       exit;
		   } 		

		// загружаем файл
		$upload = ftp_put($conn_id, '/'.$name, $filename, FTP_BINARY);
		// проверяем статус загрузки
		if (!$upload) {
		       echo "Error: FTP upload has failed!";
		   } 
		ftp_close($conn_id);


  		$data['arr'] = array( );
  		foreach ($arr as $ar) {
  			$data['arr'][] = $ar;
  		}





		

		$this->load->model('checkout/order');

		$orders = $this->db->query("SELECT * FROM oc_order WHERE 1");
		foreach ($orders->rows as $order) {
			$order_info1 = $this->model_checkout_order->getOrder($order['order_id']);
			$status = $order_info1['order_status_id'];
			if($status == 1){
				$ftp_server = 'unifarm.by';
				$ftp_user_name = 'aptechkin';
				$ftp_user_pass = '@pTecHUni18611';

				$conn_id = ftp_connect($ftp_server);
				$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

				$files = ftp_nlist($conn_id, ".");
				// var_dump($files);
				foreach ($files as $file) {
					$name_file = explode('.', $file);
					if($name_file[1] == 'txt'){
						$order_mas = explode("A", $name_file[0]);
						$order_ftp = $order_mas[1];
						
						if($order_ftp == $order['order_id']){
							$this->model_checkout_order->addOrderHistory($order_ftp, 3);
							$local_file = 'admin/export/'.$file;
							$handle = fopen($local_file, 'w');
							ftp_fget($conn_id, $handle, $remote_file, FTP_ASCII, 0);
							fclose($handle);
							var_dump($order_ftp);
						}
					}

				}
				ftp_close($conn_id);
			}
		}
		

  	






		















		$this->response->setOutput($data['result_import']);
	}
}

