<?php 
$_['text_tax']      = 'Ex Tax:';
$_['text_sale'] = 'Sale';
$_['text_new'] = 'New';
$_['text_sale_detail'] = 'Save: %s';
$_['text_contact_us'] = 'Contact Us';

$_['text_color_tools'] = 'Live Theme Editor';
$_['text_selectors']   = 'Layout Selectors';
$_['text_elements']   = 'Layout Elements';


$_['text_sidebar_left'] = 'Sidebar Left';
$_['text_sidebar_right'] = 'Sidebar Right';

$_['text_about_us'] = 'About Us';

$_['quick_view']      = 'Быстрый просмотр';

$_['text_item']      = 'item';
$_['text_review']      = 'Reviews';

$_['text_payments']      = 'Payments';
$_['text_download']      = 'Download App';

$_['text_setting']      = 'setting';
$_['text_account_skin']      = 'account';
$_['text_account_2kin']      = 'account';

$_['text_latest']  = 'Latest'; 
$_['text_mostviewed']  = 'Most Viewed'; 
$_['text_featured']  = 'Featured'; 
$_['text_bestseller']  = 'Best Seller'; 
$_['text_special']  = 'Special'; 
// Deals Language
$_['text_sale'] = 'Sale';
$_['text_years'] = 'Years';
$_['text_months'] = 'Months';
$_['text_weeks'] = 'Weeks';
$_['text_days'] = 'Day';
$_['text_hours'] = 'Hrs';
$_['text_minutes'] = 'Mins';
$_['text_seconds'] = 'Secs';
$_['text_finish'] = 'Expired';

//language feedback form
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Message';
$_['entry_submit']   = 'Submit';

$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Message must be between 10 and 1000 characters!';

$_['text_success']   = 'Your message has been successfully sent to the store owner!</p>';

// list_categories
$_['view_more']   = 'View more';

?>