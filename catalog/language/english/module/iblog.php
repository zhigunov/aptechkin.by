<?php
$_['heading_title']				= 'Новости';
$_['no_featured']				= 'No featured posts available.';
$_['no_posts']					= 'No posts available.';

// Text
$_['text_author']				= 'Автор: ';
$_['text_date_created']			= 'Дата создания: ';
$_['text_error']				= 'Нет новостей!';

$_['iblog_index_heading']		= 'Последние новости';
$_['text_iblog_empty']			= 'Нет новостей!';
$_['text_display']				= 'Display:';
$_['text_list']					= 'Список';
$_['text_grid']					= 'Таблица';
$_['text_sort']					= 'Сортировать:';
$_['text_limit']				= 'Показывать:';
$_['text_default']				= 'По умолчанию';
$_['text_name_asc']				= 'По возрастанию (A - Я)';
$_['text_name_desc']			= 'По убыванию (Я - A)';
$_['iblog_button']				= 'Читать';

$_['iblog_keywords']			= 'Тэги:';
$_['no_keywords']				= 'Нет ключевых слов:';

$_['search_string']				= 'Поиск новостей:';
$_['search_button']				= 'Поиск';
$_['search_placeholder']		= 'Поиск';

$_['heading_title_search']		= 'Поиск';
$_['text_keyword']				= 'Write your keywords here';
$_['text_critea']				= 'Поиск Criteria';
$_['entry_search']				= 'Поиск:';
$_['entry_description']			= 'Поиск в описании';
$_['text_search']				= 'Результаты поиска';
$_['text_empty']				= 'Введите другой критерий поиска';
?>