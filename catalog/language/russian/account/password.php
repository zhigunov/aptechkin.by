<?php
$_ = array (
  'heading_title' => 'Изменить пароль',
  'text_account' => 'Личный Кабинет',
  'text_password' => 'Изменение пароля',
  'text_success' => 'Ваш пароль успешно изменен!',
  'entry_password' => 'Новый пароль',
  'entry_confirm' => 'Подтвердите пароль',
  'error_password' => 'Пароль должен быть от 4 до 20 символов!',
  'error_confirm' => 'Пароли не совпадают!',
);
