<?php
$_ = array (
  'text_home' => 'Главная',
  'text_wishlist' => 'Закладки (%s)',
  'text_shopping_cart' => 'Корзина',
  'text_category' => 'Категории',
  'text_account' => 'Личный кабинет',
  'text_register' => 'Регистрация',
  'text_login' => 'Вход/регистрация',
  'text_order' => 'История заказов',
  'text_transaction' => 'Транзакции',
  'text_download' => 'Загрузки',
  'text_logout' => 'Выход',
  'text_checkout' => 'Оформление заказа',
  'text_search' => 'Поиск',
  'text_all' => 'Смотреть Все',

);

/**
 *Ajax advanced search starts
 */
$_['ajaxadvancedsearch_model'] = 'Модель :';
$_['ajaxadvancedsearch_manufacturer'] = 'Производитель :';
$_['ajaxadvancedsearch_price'] = 'Цена :';
$_['ajaxadvancedsearch_stock'] = 'Остаток :';
$_['ajaxadvancedsearch_rating'] = 'Рейтинг :';
/**
 *Ajax advanced search ends
 */
