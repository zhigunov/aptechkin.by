<?php
class ModelToolImport extends Model {	
	public function getManufacturers() {
		// find all manufacturers already stored in the database
		$manufacturer_ids = array();
		$sql  = "SELECT ms.manufacturer_id, ms.store_id, m.`name` FROM `".DB_PREFIX."manufacturer_to_store` ms ";
		$sql .= "INNER JOIN `".DB_PREFIX."manufacturer` m ON m.manufacturer_id=ms.manufacturer_id";
		$result = $this->db->query( $sql );
		$manufacturers = array();
		foreach ($result->rows as $row) {
			$manufacturer_id = $row['manufacturer_id'];
			$store_id = $row['store_id'];
			$name = $row['name'];
			if (!isset($manufacturers[$name])) {
				$manufacturers[$name] = array();
			}
			if (!isset($manufacturers[$name]['manufacturer_id'])) {
				$manufacturers[$name]['manufacturer_id'] = $manufacturer_id;
			}
			if (!isset($manufacturers[$name]['store_ids'])) {
				$manufacturers[$name]['store_ids'] = array();
			}
			if (!in_array($store_id,$manufacturers[$name]['store_ids'])) {
				$manufacturers[$name]['store_ids'][] = $store_id;
			}
		}
		return $manufacturers;
	}

	public function getManufacturerId(&$manufacturerName) {
		$database =& $this->db;
		$sql2 = "SELECT `manufacturer_id` FROM `".DB_PREFIX."manufacturer` WHERE `name` = '$manufacturerName' ;";
			$result = $database->query( $sql2 );
			if ($result->rows) {
				foreach ($result->rows as $row) {
					$manufacturer_id = (int)$row['manufacturer_id'];
				}
			}
		return $manufacturer_id;
	}











	public function getCategories() {
		// find all manufacturers already stored in the database
		$manufacturer_ids = array();
		$sql  = "SELECT ms.category_id, ms.store_id, m.`name` FROM `".DB_PREFIX."category_to_store` ms ";
		$sql .= "INNER JOIN `".DB_PREFIX."category_description` m ON m.category_id=ms.category_id";
		$result = $this->db->query( $sql );
		$categories = array();
		foreach ($result->rows as $row) {
			$category_id = $row['category_id'];
			$store_id = $row['store_id'];
			$name = $row['name'];
			if (!isset($categories[$name])) {
				$categories[$name] = array();
			}
			if (!isset($categories[$name]['$category_id'])) {
				$categories[$name]['$category_id'] = $category_id;
			}
			if (!isset($categories[$name]['store_ids'])) {
				$categories[$name]['store_ids'] = array();
			}
			if (!in_array($store_id,$categories[$name]['store_ids'])) {
				$categories[$name]['store_ids'][] = $store_id;
			}
		}
		return $categories;
	}

	public function getCategoryId(&$categoryName) {
		$database =& $this->db;
		$sql2 = "SELECT `category_id` FROM `".DB_PREFIX."category_description` WHERE `name` = '$categoryName' ;";
		$result = $database->query( $sql2 );
		if ($result->rows) {
			foreach ($result->rows as $row) {
				$category_id = (int)$row['category_id'];
			}
		}
		return $category_id;
	}

	public function storeCategoriesIntoDatabase( &$database, &$categories)
	{
		$sort_order = 0;
		$store_id = 0;
		$languageId = $this->getDefaultLanguageId($database);
		foreach ($categories as $category) {
			$categoryName = trim($category['name']);
			$parent = trim($category['parent']);
			$sql1  = "INSERT INTO `".DB_PREFIX."category` (`parent_id`, `top`, `column`, `status`, `sort_order`) VALUES ($parent, 1, 1, 1, $sort_order);";
			$database->query($sql1);
			$sql2  = "SELECT MAX(category_id) FROM `".DB_PREFIX."category`;";
			$result = $database->query( $sql2 );
			if ($result->rows) {
				foreach ($result->rows as $row) {
					$category_id = (int)$row['MAX(category_id)'];
				}
			}
			$sql3  = "INSERT INTO `".DB_PREFIX."category_description` (`category_id`, `language_id`, `name`, `meta_title`) VALUES ($category_id , $languageId, '$categoryName', '$categoryName');";
			$database->query($sql3);
			$sql4 = "INSERT INTO `".DB_PREFIX."category_to_store` (`category_id`, `store_id`) VALUES ($category_id, $store_id);";
			$database->query($sql4);
			$sql5 = "INSERT INTO `".DB_PREFIX."category_path` (`category_id`, `path_id`, `level`) VALUES ($category_id, $category_id, 0);";
			$database->query($sql5);
			$sql5 = "INSERT INTO `".DB_PREFIX."category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES ($category_id, $store_id, 0);";
			$database->query($sql5);



		}
		return TRUE;
	}

	public function delCategoriesIntoDatabase( &$database)
	{
		$sql = "DELETE FROM `".DB_PREFIX."category`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."category_description`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."category_filter`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."category_path`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."category_to_layout`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."category_to_store`;\n";
		$this->multiquery( $database, $sql );

		return TRUE;
	}



	public function getAttributeId($attributeName) {
		$database =& $this->db;
		$sql2 = "SELECT `attribute_id` FROM `".DB_PREFIX."attribute_description` WHERE `name` = '$attributeName' ;";
		$result = $database->query( $sql2 );
		if ($result->rows) {
			foreach ($result->rows as $row) {
				$attribute_id = (int)$row['attribute_id'];
			}
		}
		return $attribute_id;
	}









	public function getDefaultLanguageId( &$database ) {
		$code = $this->config->get('config_language');
		$sql = "SELECT language_id FROM `".DB_PREFIX."language` WHERE code = '$code'";
		$result = $database->query( $sql );
		$languageId = 1;
		if ($result->rows) {
			foreach ($result->rows as $row) {
				$languageId = $row['language_id'];
				break;
			}
		}
		return $languageId;
	}



	public function multiquery( &$database, $sql ) {
		foreach (explode(";\n", $sql) as $sql) {
			$sql = trim($sql);
			if ($sql) {
				$database->query($sql);
			}
		}
	}



	public function storeProductsIntoDatabase( &$database, &$products ) 
	{
		$languageId = $this->getDefaultLanguageId($database);
		foreach ($products as $product){
			$product_id = $product['KODTOV'];
			$sql = "SELECT * FROM `".DB_PREFIX."product_add_date` WHERE product_id= $product_id";
			$result = $database->query($sql);
			if($result->num_rows == 0){
				$sql1 = "INSERT INTO `".DB_PREFIX."product_add_date` (`product_id`,`date_add`) VALUES ($product_id, CURDATE()) ";
				$database->query($sql1);
			}

		}
		$sql = "DELETE FROM `".DB_PREFIX."product`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."product_description` WHERE language_id=$languageId;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."product_to_category`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."product_to_store`;\n";
		$sql .= "DELETE FROM `".DB_PREFIX."product_attribute`;\n";
		$this->multiquery( $database, $sql );
			
		foreach ($products as $product) {
			$productId = $product['KODTOV'];
			$productName = $database->escape(trim($product['NAIM']));
			$categoryName = $product['ATX3'];
			$categoryId = $this->getCategoryId($categoryName);
			$parentCategoryName = $product['TYPE'];
			$parentCategoryId = $this->getCategoryId($parentCategoryName);
			$quantity = $product['KOLVO'];
			$model = $database->escape(trim($product['NAIM']));
			$manufacturerName = addslashes(trim($product['FIRMA']));
			$manufacturerId = $this->getManufacturerId($manufacturerName);
			//$manufacturerId = 0;
			$price = trim($product['PRICE']);
			$status = "YES";
			$status = ((strtoupper($status)=="TRUE") || (strtoupper($status)=="YES") || (strtoupper($status)=="ENABLED")) ? 1 : 0;
			$productDescription = $database->escape(trim($product['NAIM']));
			$meta_description = $database->escape(trim($product['NAIM']));
			$sku = $database->escape($product['KODTOV']);
			$deals_and_newproduct = $database->query("SELECT * FROM " . DB_PREFIX . "product_to_deals_and_newproduct WHERE product_id = $productId");
			if(isset($deals_and_newproduct->row['deals'])){
				$deals = $deals_and_newproduct->row['deals'];
			}else{
				$deals = 0;
			}
			if(isset($deals_and_newproduct->row['newproduct'])){
				$newproduct = $deals_and_newproduct->row['newproduct'];
			}else{
				$newproduct = 0;
			}



			$sql1  = "INSERT INTO `".DB_PREFIX."product` (`product_id`,`quantity`, `manufacturer_id`, `sku`, `model`,`price`, `deals`,`newproduct`,`sort_order` ,`status` ,`date_added`) VALUES ($productId,$quantity,$manufacturerId,'$sku','$model', $price,$deals, $newproduct, 1, $status, CURDATE());";

			$sql2 = "INSERT INTO `".DB_PREFIX."product_description` (`product_id`, `language_id`, `name`,`description`,`meta_title`) VALUES ($productId, $languageId, '$productName','$productDescription','$meta_description');";
			
			$database->query($sql1);
			$database->query($sql2);

			$sql3 = "INSERT INTO `".DB_PREFIX."product_to_store` (`product_id`, `store_id`) VALUES ($productId, 0);";
			
			$database->query($sql3);

			$sql4 = "INSERT INTO `".DB_PREFIX."product_to_category` (`product_id`, `category_id`) VALUES ($productId, $categoryId);";

			$database->query($sql4);

			$sql4 = "INSERT INTO `".DB_PREFIX."product_to_category` (`product_id`, `category_id`) VALUES ($productId, $parentCategoryId);";

			$database->query($sql4);
			
			$postav = $product['POSTAV'];
			$postavId = $this->getAttributeId('Поставщик');
			$country = $product['COUNTRY'];
			$countryId = $this->getAttributeId('Страна');
			$CENAROC = $product['CENAROC'];
			$CENAROCId = $this->getAttributeId('CENAROC');
			$recept = trim($product['USLOV']);
			$receptId = $this->getAttributeId('Рецептурный препарат');
			$activV = trim($product['MNN']);
			$activVId = $this->getAttributeId('Активное вещество');
			$godnost = trim($product['SRGOD']);
			$godnostId = $this->getAttributeId('Срок годности');

			$sql5 = "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $postavId, $languageId, '$postav');\n";
			$sql5 .= "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $CENAROCId, $languageId, $CENAROC);\n";
			$sql5 .= "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $countryId, $languageId, '$country');\n";
			if (!empty($recept)){
				$sql5 .= "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $receptId, $languageId, '$recept');\n";
			}
			if (!empty($activV)){
				$sql5 .= "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $activVId, $languageId, '$activV');\n";
			}
			if (!empty($godnost)){
				$sql5 .= "INSERT INTO `".DB_PREFIX."product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES ($productId, $godnostId, $languageId, '$godnost');\n";
			}

			$this->multiquery( $database, $sql5 );
		}
		return TRUE;
	}

	public function storeManufacturersIntoDatabase( &$database, &$manufacturers) 
	{
		$sort_order = 0;	
		$store_id = 0;
		foreach ($manufacturers as $manufacturer) {
			$manufacturerName = $manufacturer;	
			$sql1  = "INSERT INTO `".DB_PREFIX."manufacturer` (`name`, `sort_order`) VALUES ('$manufacturerName', $sort_order);";
			$database->query($sql1);
		
			$sql2 = "SELECT `manufacturer_id` FROM `".DB_PREFIX."manufacturer` WHERE `name` = '$manufacturerName' ;";
			$result = $database->query( $sql2 );
			if ($result->rows) {
				foreach ($result->rows as $row) {
					$manufacturer_id = (int)$row['manufacturer_id'];
				}
			}

			$sql3 = "INSERT INTO `".DB_PREFIX."manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES ($manufacturer_id, $store_id);";
			$database->query($sql3);
		}
		return TRUE;
	}


	public function delManufacturersIntoDatabase( &$database) 
	{
		$sql = "DELETE FROM `".DB_PREFIX."manufacturer`;";
		$database->query($sql);

		return TRUE;
	}



}