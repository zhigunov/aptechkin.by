 <div class="sidebar-offcanvas sidebar  visible-xs visible-sm">
  <div class="offcanvas-inner panel panel-offcanvas">
    <div class="offcanvas-heading">
        <button type="button" class="btn btn-primary" data-toggle="offcanvas"> <span class="fa fa-times"></span></button>
    </div>
    <div class="offcanvas-body panel-body">
          <div id="offcanvasmenu"></div> 
          <?php echo $this->renderModule('category'); ?>
    </div>
  </div>
</div> 
<script type="text/javascript">
  $("#offcanvasmenu").html($("#bs-megamenu").html());
</script>
