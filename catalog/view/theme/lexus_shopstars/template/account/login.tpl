<?php  echo $header; ?> <?php require( ThemeControlHelper::getLayoutPath( 'common/mass-header.tpl' )  ); ?>
<div class="main-columns container">
    
  <?php require( ThemeControlHelper::getLayoutPath( 'common/mass-container.tpl' )  ); ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php if( $SPAN[0] ): ?>
			<aside id="sidebar-left" class="col-md-<?php echo $SPAN[0];?>">
				<?php echo $column_left; ?>
			</aside>	
		<?php endif; ?> 
  
   <div id="sidebar-main" class="col-md-<?php echo $SPAN[1];?>"><div id="content">
    <div class="content-inner">
    <?php echo $content_top; ?>    
      <div class="row"> 
          <?php /* ?>        
          <div class="col-sm-6">
              <h3><?php echo $text_new_customer; ?></h3>
              <p><strong><?php echo $text_register; ?></strong></p>
              <p><?php echo $text_register_account; ?></p>
              <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
          </div>
          <?php */ ?>
          <div class="col-sm-12">
              <h3><?php echo $text_returning_customer; ?></h3>
              <p id="sendsms"><strong>Смс с паролем придет на указанный телефон в течение нескольких минут<strong></p>
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="col-sm-10">
                  <div class="form-group">
                    <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                    <input type="text" name="telephone" value="" placeholder="Телефон" id="input-telephone" class="form-control" />
                  </div>
                </div>
                <div class="col-sm-2">
                  <!-- <a href="" data-loading-text="Идет отправка смс" id="apisms">Получить пароль по смс</a> -->
                </div>
                <div class="col-sm-10">
                  <div class="form-group">
                    <p><i class="fa fa-info-circle"></i> Если вы регистрируетесь впервые или забыли пароль, укажите номер телефона и нажмите "Получить пароль по смс".<br>
Если у вас есть пароль,укажите ваш номер телефона и нажмите "Есть пароль"</p></div>
                    <input type="button" id="havepass" value="Есть пароль" class="btn btn-primary" />
                    <input type="button" id="apisms" value="Получить пароль по смс" data-loading-text="Идет отправка смс" class="btn btn-primary" />
                    <p style="margin: 0 0;">&nbsp</p>
                    <label style="display: none;" class="control-label" for="input-password" id="label-input-password"><?php echo $entry_password; ?></label>
                    <input style="display: none;" type="password" name="password" value="<?php echo $password; ?>" placeholder="Пароль" id="input-password" class="form-control" />
                    <p style="margin: 0 0;">&nbsp</p>
                    
                 
                    <input style="display: none;" type="submit" value="<?php echo $button_login; ?>" id="button-submit" class="btn btn-primary" />
                    <?php if ($redirect) { ?>
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                    <?php } ?>
                  </div>
                 <div class="col-sm-2">
                 <p style="margin: 0 0;">&nbsp</p>

                 <!-- <a href="" id="havepass">Есть пароль</a>
                  <a href="" data-loading-text="Идет отправка смс" id="apisms">Получить пароль по смс</a> -->
                </div>
              </form>
          </div>
          
        </div>
      </div>
      <?php echo $content_bottom; ?>
      </div>
   </div> 
<?php if( $SPAN[2] ): ?>
	<aside id="sidebar-right" class="col-md-<?php echo $SPAN[2];?>">	
		<?php echo $column_right; ?>
	</aside>
<?php endif; ?></div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
jQuery(function($){
   $("#input-telephone").mask("+375 (99) 999-99-99");
});
</script>

<script>
  // Register
  $(document).delegate('#apisms', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/register/savetel',
      type: 'post',
      data: $('#input-telephone'),
      dataType: 'json',
      beforeSend: function() {
        $('#apisms').button('loading');
      },
      complete: function() {
        $('#apisms').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');
        $('#sendsms').css('display', "block");
        $('#label-input-password').css('display', "block");
        $('#input-password').css('display', "block");
        $('#button-submit').css('display', "block");
        //alert(json['password']);


      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
    return false;
  });

  $(document).delegate('#havepass', 'click', function() {
    $('#label-input-password').css('display', "block");
    $('#input-password').css('display', "block");
    $('#button-submit').css('display', "block");
  });

</script>