<?php 
    
    $mode = 'default';
    $cols = array( 'default' => array(5,7),
                   'horizontal' => array(4,7)
    ); 
    $cols = $cols[$mode];     
?>

<div class="product-info">
    <div class="bg-product-info">    
    <div class="row">
    
    <?php require( ThemeControlHelper::getLayoutPath( 'product/detail/preview/'.$mode.'.tpl' ) );  ?> 
   
  <div class="col-xs-12 col-sm-<?php echo $cols[1]; ?> col-md-<?php echo $cols[1]; ?> col-lg-<?php echo $cols[1]; ?>">
  <div class="product-info-bg">

    <?php if (isset($attribute_groups)) { ?>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                <?php if ($attribute['name'] == 'Рецептурный препарат'){ ?>
                    <?php if ($attribute['text'] == 'Да'){ ?>
                      <div style="height: 40px; background-color: #FA7272; margin-right: 36px; padding-left: 5px; margin-bottom: 15px;"><p style="color: white;font-size: 20px; line-height: 40px; text-align: center;"> <i class="fa fa-inverse fa-exclamation-triangle"></i> <?php echo $attribute['name']; ?></p></div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <h3 class="title-product"><?php echo $heading_title; ?></h3>
            <?php if ($review_status) { ?>
            <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($rating < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                        <?php } ?>
                    <?php } ?>
                    <a href="#review-form" class="popup-with-form" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $reviews; ?></a> / <a href="#review-form"  class="popup-with-form" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $text_write; ?></a>

            </div>
        <?php } ?>

        <ul class="list-unstyled">
        <?php /*if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
        <?php } */?>

        <?php if ($discounts) { ?>
            <li>
            </li>
            <?php foreach ($discounts as $discount) { ?>
                <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
        <?php } ?>
        </ul>

        <?php if ($stock) { 
            if ($stock >= 2){ ?>
                <ul class="list-unstyled">
                    <li><span class="check-box text-success"><i class="fa fa-check"></i></span> <b><?php echo $text_stock; ?></b> Более 2 шт.</li>
                </ul>
            <?php }elseif ($stock == 2){ ?>
                <ul class="list-unstyled">
                    <li><span class="check-box text-success"><i class="fa fa-check"></i></span> <b><?php echo $text_stock; ?></b> 2 шт.</li>
                </ul>
            <?php }else{ ?>
                <ul class="list-unstyled">
                    <li><span class="check-box text-success"><i class="fa fa-check"></i></span> <b><?php echo $text_stock; ?></b> 1 шт.</li>
                </ul>
            <?php } ?>
        <?php } ?>
        
        <div class="border-success">
            <ul class="list-unstyled">
                <?php if ($manufacturer) { ?>
                    <li><b><?php echo $text_manufacturer; ?></b> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                <?php } ?>
                
                <?php if (isset($attribute_groups)) { ?>
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                            <?php if ($attribute['name'] == 'Страна'){ ?>
                                <li><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></li>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                            <?php if ($attribute['name'] == 'Срок годности'){ ?>
                                <li><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></li>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                            <?php if ($attribute['name'] == 'Активное вещество'){ ?>
                                <li><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></li>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                            <?php if ($attribute['name'] == 'Рецептурный препарат'){ ?>
                                <li><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

                <?php if ($reward) { ?>
                    <li><b><?php echo $text_reward; ?></b> <?php echo $reward; ?></li>
                <?php } ?>
                <?php if ($points) { ?>
                    <li><b><?php echo $text_points; ?></b> <?php echo $points; ?></li>
                <?php } ?>
            </ul>
        </div>
        <div class="tags">
            <?php if ($tags) { ?>
              <p><?php echo "<b>".$text_tags."</b>"; ?>
                <?php for ($i = 0; $i < count($tags); $i++) { ?>
                <?php if ($i < (count($tags) - 1)) { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo trim($tags[$i]['tag']); ?></a>,
                <?php } else { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo trim($tags[$i]['tag']); ?></a>
                <?php } ?>
                <?php } ?>
              </p>
            <?php } ?>
        </div>
        
        <div class="clearfix"></div>
        
        <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
        <?php } ?> 

        <!-- AddThis Button BEGIN -->
        <!--<div class="addthis_toolbox addthis_default_style space-padding-tb-20"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>-->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
        <!-- AddThis Button END --> 
  </div>
        <div id="product">
            <?php if ($price) { ?>
                <div class="price detail space-30">
                    <ul class="list-unstyled">
                        <?php if (!$special) { ?>
                            <li>
                                <span class="price-new"> <?php echo $price; ?> </span>
                            </li>
                        <?php } else { ?>

                            <li> <span class="price-new"> <?php echo $special; ?> </span> <span class="price-old"><?php echo $price; ?></span> </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>

            <div class="product-extra">
                <label class="control-label pull-left qty"><?php echo $entry_qty; ?>:</label>
                <div class="quantity-adder pull-left">
                    <div class="quantity-number pull-left">
                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                    </div>
                    <span class="add-up add-action pull-left">
                        <i class="fa fa-caret-up"></i>
                    </span>
                    <span class="add-down add-action pull-left">
                        <i class="fa fa-caret-down"></i>
                    </span>
                    
                </div>
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                <div class="cart" id="button-for-swith">
                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-outline-inverse"><?php echo $button_cart; ?></button>
                </div> 
                <div class="action">
                    <div class="pull-left">  
                        <a data-toggle="tooltip" class="wishlist" title="<?php echo $button_wishlist; ?>" onclick="wishlist.addwishlist('<?php echo $product_id; ?>');"><i class="fa-fw fa fa-heart"></i><?php echo $button_wishlist; ?></a>
                    </div>
                    <div class="pull-left">
                        <a data-toggle="tooltip" class="compare" title="<?php echo $button_compare; ?>" onclick="compare.addcompare('<?php echo $product_id; ?>');"><i class="fa-fw fa fa-refresh"></i><?php echo $button_compare; ?></a>
                    </div>
                </div>

            </div>
            <?php if ($options) { ?>
                <h3><?php echo $text_option; ?></h3>
                <?php foreach ($options as $option) { ?>
                    <?php if ($option['type'] == 'select') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                        <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                        <?php } ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'radio') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'image') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'textarea') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                        </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'file') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                        </div>
                    <?php } ?>
                    
                    <?php if ($option['type'] == 'date') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group date">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>

                    <?php if ($option['type'] == 'datetime') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group datetime">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                    
                    <?php if ($option['type'] == 'time') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                      <div class="input-group time">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
                
                <h3><?php echo $text_payment_recurring ?></h3>
                <div class="form-group required">
                    <select name="recurring_id" class="form-control">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($recurrings as $recurring) { ?>
                            <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                        <?php } ?>
                    </select>
                    <div class="help-block" id="recurring-description"></div>
                </div>
            <?php } ?>
            
        </div>
    </div><!-- End div bg -->
</div>
</div>


<?php if( !empty($analogs) ) { ?>
<div id="blog-carousel" class="widget-blogs owl-carousel-play latest-posts latest-posts-v1 panel-default <?php if ( isset($stylecls)&&$stylecls) { ?>box-<?php echo $stylecls; ?><?php } ?>">

    <div class="panel-heading"><h4 class="panel-title">Аналогичные товары</h4></div>

    <div class="panel-body">
        <div class="carousel-inner owl-carousel" data-show="1" data-pagination="false" data-navigation="true">

                <?php  $pages = array_chunk( $analogs, 3); $span = 4; $cols = 3; ?>

                <?php foreach ($pages as  $k => $tblogs ) {   ?>
                    <div class="item <?php if($k==0) {?>active<?php } ?>">
                        <?php foreach( $tblogs as $i => $product ) {  $i=$i+1;?>
                            <?php if( $i%$cols == 1 ) { ?>
                            <div class="row products-row">
                            <?php } ?>

                                <div class="col-lg-<?php echo floor(12/$cols);?> col-md-<?php echo floor(12/$cols);?> col-sm-<?php echo floor(12/$cols);?>" style="padding-bottom: 40px;">
                                    <div class="blog-item">
                                        <?php 
                                          $objlang = $this->registry->get('language');
                                          $ourl = $this->registry->get('url');  
                                          $button_cart = $objlang->get("button_cart"); 
                                        ?>
                                        <div class="product-block">

                                            <?php if ($product['thumb']) {    ?>      
                                              <div class="image">
                                                <?php if( $product['special'] ) {   ?>
                                                  <span class="product-label sale-exist"><span class="product-label-special"><?php echo $objlang->get('text_sale'); ?></span></span>
                                                <?php } ?>

                                                <div class="product-img img">
                                                  <a class="img" title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>">
                                                    <img class="img-responsive" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
                                                  </a>
                                                  
                                                  <div class="zoom hidden-xs hidden-sm">
                                                      <?php if( isset($categoryPzoom) && $categoryPzoom ) { $zimage = str_replace( "cache/","", preg_replace("#-\d+x\d+#", "",  $product['thumb'] ));  ?>
                                                        <a data-toggle="tooltip" data-placement="top" href="<?php echo $zimage;?>" class="product-zoom info-view colorbox cboxElement" title="<?php echo $product['name']; ?>"><i class="fa fa-search"></i></a>
                                                      <?php } ?>
                                                  </div>          
                                                </div>
                                              </div>
                                            <?php } ?>
                                          <div class="product-meta">
                                              <div class="top">
                                                <h6 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
                                                <?php if ($product['rating']) { ?>
                                                  <div class="rating">
                                                    <?php for ($is = 1; $is <= 5; $is++) { ?>
                                                    <?php if ($product['rating'] < $is) { ?>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    <?php } else { ?>
                                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i>
                                                    </span>
                                                    <?php } ?>
                                                    <?php } ?>
                                                  </div>
                                                <?php }else{ ?>
                                                  <div class="rating">
                                                    <?php for ($is = 1; $is <= 5; $is++) { ?>
                                                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i>
                                                    </span>
                                                    <?php } ?>
                                                  </div>
                                                <?php } ?>        


                                                <?php if ($product['price']) { ?>
                                                <div class="price">
                                                  <?php if (!$product['special']) {  ?>
                                                    <span class="price-new"><?php echo $product['price']; ?></span>
                                                    <?php if( preg_match( '#(\d+).?(\d+)#',  $product['price'], $p ) ) { ?> 
                                                    <?php } ?>
                                                  <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?></span><br>
                                                    <span class="price-old"><?php echo $product['price']; ?></span> 
                                                    <?php if( preg_match( '#(\d+).?(\d+)#',  $product['special'], $p ) ) { ?> 
                                                    <?php } ?>

                                                  <?php } ?>
                                                </div>
                                                <?php } ?>

                                                <?php if (isset($product['manufacturer'])) { ?>
                                                  <p><?php echo $text_manufacturer; ?>: <?php echo $product['manufacturer']; ?></p>
                                                <?php }else{
                                                    $this->load->model('catalog/product');
                                                    $product = $this->model_catalog_product->getProduct($product['product_id']);
                                                    if (isset($product['manufacturer'])) { ?>
                                                      <p>Производитель: <?php echo $product['manufacturer']; ?></p>
                                                    <?php }
                                                  } ?>
                                                <?php if (isset($product['attributies'])) { ?>
                                                  <?php foreach ($product['attributies'] as $attribute_group) { ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Страна'){ ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                          <?php } ?>
                                                      <?php } ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Активное вещество'){ ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                          <?php } ?>
                                                      <?php } ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Рецептурный препарат'){ ?>
                                                            <?php if ($attribute['text'] == 'Да'){ ?>
                                                              <div style="background-color: #9b9c9c; margin-right: 36px; padding-left: 5px;"><p style="color: white;"> <i class="fa fa-inverse fa-exclamation-triangle"></i> <?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p></div>
                                                            <?php }else { ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                            <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  <?php } ?>
                                                <?php }else{
                                                  $product['attributies'] = $this->model_catalog_product->getProductAttributes($product['product_id']);
                                                  foreach ($product['attributies'] as $attribute_group) { ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Страна'){ ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                          <?php } ?>
                                                      <?php } ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Активное вещество'){ ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                          <?php } ?>
                                                      <?php } ?>
                                                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                          <?php if ($attribute['name'] == 'Рецептурный препарат'){ ?><?php if ($attribute['text'] == 'Да'){ ?>
                                                              <div style="background-color: #9b9c9c;"><p style="color: white; font-size: 12px; padding: 5px 5px 5px 3px;"> <i class="fa fa-inverse fa-exclamation-triangle"></i> <?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p></div>
                                                            <?php }else { ?>
                                                              <p><?php echo $attribute['name']; ?>: <?php echo $attribute['text']; ?></p>
                                                            <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  <?php } ?>
                                                  <?php } ?>

                                              </div>
                                                <div class="bottom">
                                                  <div class="action add-links clearfix">

                                                     <?php if( !isset($listingConfig['catalog_mode']) || !$listingConfig['catalog_mode'] ) { ?>
                                                    <div class="cart">            
                                                       <button data-loading-text="Loading..." class="btn-action" type="button" onclick="cart.addcart('<?php echo $product['product_id']; ?>');">
                                                         <i class="fa fa-shopping-cart"></i><span class="hidden-sm"><?php echo $button_cart; ?></span>
                                                      </button>
                                                    </div>
                                                  <?php } ?>

                                                    <div class="compare" style=" width: 14%; border: 0px solid;">     
                                                      <button class="btn-action" type="button" data-toggle="tooltip" data-placement="top" title="<?php echo $objlang->get("button_compare"); ?>" onclick="compare.addcompare('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button> 
                                                    </div>  
                                                    <div class="wishlist" style=" width: 14%; border: 0px solid;">
                                                      <button class="btn-action" type="button" data-toggle="tooltip" data-placement="top" title="<?php echo $objlang->get("button_wishlist"); ?>" onclick="wishlist.addwishlist('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button> 
                                                    </div> 
                                                  </div>

                                                </div>                         
                                          </div>  
                                        </div>                    
                                    





                                    </div>          
                                </div>

                            <?php if( $i%$cols == 0 || $i==count($tblogs) ) { ?>
                            </div>
                            <?php } ?>
                        <?php } //endforeach; ?>
                    </div>
                <?php } ?>
        </div>
        <?php // if( count($analogs) > 3 ) { ?>
        <div class="carousel-controls">
            <a class="carousel-control left" href="#blog-carousel"   data-slide="prev" style="display: block; left: -52px;"><i class="fa fa-angle-left"></i></a>
            <a class="carousel-control right" href="#blog-carousel"  data-slide="next" style="display: block;""><i class="fa fa-angle-right"></i></a>
        </div>      
        <?php // } ?>
    </div>
</div>
<?php } ?>




<div class="clearfix box-product-infomation tab-v3 tabs-left">
    <div class="col-sm-3 clearfix box-product-infomation tab-v3 tabs-left">
        <ul class="nav nav-tabs" role="tablist">
            <!-- <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?> -->
                <li class="active"><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
           <!--  <?php } ?> -->
            <?php if ($review_status) { ?>
                <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
            <?php if( $productConfig['enable_product_customtab'] && isset($productConfig['product_customtab_name'][$languageID]) ) { ?>
                <li><a href="#tab-customtab" data-toggle="tab"><?php echo $productConfig['product_customtab_name'][$languageID]; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="col-sm-9 tab-content text-left">
        <div class="tab-pane " id="tab-description"><?php echo $description; ?></div>
        <div class="tab-pane active" id="tab-specification"><?php echo $instruction; ?></div>
        <?php if ($review_status) { ?>

            <div class="tab-pane" id="tab-review">

                <div id="review" class="space-20">

                </div>
                <p> <a href="#review-form"  class="popup-with-form btn btn-sm btn-primary" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $text_write; ?></a></p>

               <div class="hide"> <div id="review-form" class="panel review-form-width">
                <form class="form-horizontal" id="form-review">
                 
                    <h2><?php echo $text_write; ?></h2>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                            <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                            <div class="help-block"><?php echo $text_note; ?></div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label"><?php echo $entry_rating; ?></label>
                            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                            <input type="radio" name="rating" value="1" />
                            &nbsp;
                            <input type="radio" name="rating" value="2" />
                            &nbsp;
                            <input type="radio" name="rating" value="3" />
                            &nbsp;
                            <input type="radio" name="rating" value="4" />
                            &nbsp;
                            <input type="radio" name="rating" value="5" />
                            &nbsp;<?php echo $entry_good; ?></div>
                    </div>
                    <?php echo $captcha; ?>
                    <div class="buttons">
                        <div class="pull-right">
                            <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                        </div>
                    </div>
                </form></div>
                </div>

            </div>
        <?php } ?>
        <?php if( $productConfig['enable_product_customtab'] && isset($productConfig['product_customtab_content'][$languageID]) ) { ?>
        <div id="tab-customtab" class="tab-content custom-tab">
            <div class="inner">
                <?php echo html_entity_decode( $productConfig['product_customtab_content'][$languageID], ENT_QUOTES, 'UTF-8'); ?>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
</div>
<?php if ($products) {  $heading_title = $text_related; $customcols = 4; ?>
<div class="panel-default product-grid"> <?php require( PAVO_THEME_DIR."/template/common/products_owl_module.tpl" );  ?> </div>
<?php } ?>
