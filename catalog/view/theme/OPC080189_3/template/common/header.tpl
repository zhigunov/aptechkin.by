<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,600,300,700" rel="stylesheet" type="text/css" />
<link href='//fonts.googleapis.com/css?family=Lato:400,100,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
<link href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/stylesheet.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/megnor/carousel.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/megnor/custom.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/megnor/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/megnor/lightbox.css" />

<?php if($direction=='rtl'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $mytemplate; ?>/stylesheet/megnor/rtl.css">
<?php }?>

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

<!-- Megnor www.templatemela.com - Start -->
<script type="text/javascript" src="catalog/view/javascript/megnor/parallex.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/custom.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jstree.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/carousel.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/megnor.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jquery.custom.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jquery.elevatezoom.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/lightbox/lightbox-2.6.min.js"></script>
<!-- Megnor www.templatemela.com - End -->

<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
<script src="catalog/view/javascript/megnor/tabs.js" type="text/javascript"></script>

<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>	
</head>

<?php if ($column_left && $column_right) { ?>
<?php $layoutclass = 'layout-3'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php if ($column_left){ ?>
<?php $layoutclass = 'layout-2 left-col'; ?>
<?php } elseif ($column_right) { ?>
<?php $layoutclass = 'layout-2 right-col'; ?>
<?php } ?>
<?php } else { ?>
<?php $layoutclass = 'layout-1'; ?>
<?php } ?>

<body class="<?php echo $class;echo " " ;echo $layoutclass; ?>">
<div class="main-header">

<!--<div id="top-contact">
	<div class="container">
		<p style="color:white; text-align: center;">Служба обработки заказов: <span class="phone"><a href="tel:+375297777777">+375(29)777-77-77</a></span>,
		<span class="phone"><a href="tel:+37547777777">+375(44)777-77-77</a></span>
		(круглосуточно, звонок по Беларуси бесплатный)</p>

	</div>
</div>-->

<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <?php /*?><li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li><?php */?>
		<!--<li><a href="<?php echo $wishlist; ?>" class="top-wishlist" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        <li class="dropdown myaccount"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $text_account; ?><span class="caret"></span></a>

			<ul class="dropdown-menu dropdown-menu-right myaccount-menu">
            <?php if ($logged) { ?>
            --><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <!--<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>-->
            <?php } else { ?>
            <!--<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>-->
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
			<!--<li class="inner-wishlist"><a href="<?php echo $wishlist; ?>" id="wishlist-tm-total" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
			<li class="inner-checkout"><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><?php echo $text_checkout; ?></a></li>
          </ul>
        </li>-->
        <!--<li><a class="top-checkout" href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><?php echo $text_checkout; ?></a></li>
      --></ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
	<div class="header_left">
      <div class="header-logo">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
	</div>
	<div class="header_right">
	  <!--<div class="contact_us"> <div class="telephone"></div> <div class="cms-data"><span class="header_call">Колл-центр </span> <span><?php echo $telephone; ?></span> </div></div>-->
	  <div class="header_cart"><?php echo $cart; ?></div>
	  <!--iv class="header_search"><?php echo $search; ?></div>
	--></div>
		<div class="header_search"><?php echo $search; ?></div>

  </div>
</header>
	<?php if($ajaxadvancesearch_status){ ?>
	<!--
    /**
        *Ajax advanced search starts
        */
    -->
	<script type="text/javascript" language="javascript"><!--
		// Ajax advanced search starts
		$(document).ready(function(){
			var afaxAdvancedSearch = $('input[name="search"]');
			var customAutocomplete = null;
			afaxAdvancedSearch.autocomplete({
				delay: 500,
				responsea : function (items){
					if (items.length) {
						for (i = 0; i < items.length; i++) {
							this.items[items[i]['value']] = items[i];
						}
					}
					var html='';
					if(items.length){
						$.each(items,function(key,item){
							if(item.product_id!=0){
								html += '<li data-value="' + item['value'] + '"><a href="#">';
								html += '<div class="ajaxadvance">';
								html += '<div class="image">';
								if(item.image){
									html += '<img title="'+item.name+'" src="'+item.image+'"/>';
								}
								html += '</div>';
								html += '<div class="content">';
								html += 	'<h3 class="name">'+item.label+'</h3>';
								if(item.model){
									html += 	'<div class="model">';
									html +=		'<?php echo $ajaxadvancedsearch_model; ?> '+ item.model;
									html +=		'</div>';
								}
								if(item.manufacturer){
									html += 	'<div class="manufacturer">';
									html +=		'<?php echo $ajaxadvancedsearch_manufacturer; ?> '+ item.manufacturer;
									html +=		'</div>';
								}
								if(item.price){
									html += 	'<div class="price"> <?php echo $ajaxadvancedsearch_price; ?> ';
									if (!item.special) {
										html +=			 item.price;
									} else {
										html +=			'<span class="price-old">'+ item.price +'</span> <span class="price-new">'+ item.special +'</span>';
									}
									html +=		'</div>';
								}
								if(item.stock_status){
									html += 	'<div class="stock_status">';
									html +=		'<?php echo $ajaxadvancedsearch_stock; ?> '+ item.stock_status;
									html +=		'</div>';
								}
								if (item.rating) {
									html +=		'<div class="ratings"> <?php echo $ajaxadvancedsearch_rating; ?>';
									for (var i = 1; i <= 5; i++) {
										if (item.rating < i) {
											html +=		'<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>';
										} else {
											html +=		'<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>';
										}
									}
									html +=		'</div>';
								}
								html +='</div>';
								html += '</div></a></li>'
							}
						});
					}
					if (html) {
						afaxAdvancedSearch.siblings('ul.dropdown-menu').show();
					} else {
						afaxAdvancedSearch.siblings('ul.dropdown-menu').hide();
					}

					$(afaxAdvancedSearch).siblings('ul.dropdown-menu').html(html);
				},
				source: function(request, response) {
					customAutocomplete = this;
					$.ajax({
						url: 'index.php?route=common/header/ajaxLiveSearch&filter_name=' +  encodeURIComponent(request),
						dataType : 'json',
						success : function(json) {
							customAutocomplete.responsea($.map(json, function(item) {
								return {
									label: item.name,
									name: item.name1,
									value: item.product_id,
									model: item.model,
									stock_status: item.stock_status,
									image: item.image,
									manufacturer: item.manufacturer,
									price: item.price,
									special: item.special,
									category: item.category,
									rating: item.rating,
									reviews: item.reviews,
								}
							}));
						}
					});
				},
				select : function (ui){
					return false;
				},
				selecta: function(ui) {
					if(ui.value){
						location = 'index.php?route=product/product/&product_id='+ui.value;
					}else{
						$('#search input[name=\'search\']').parent().find('button').trigger('click');
					}
					return false;
				},
				focus: function(event, ui) {
					return false;
				}
			});

			afaxAdvancedSearch.siblings('ul.dropdown-menu').delegate('a', 'click', function(){
				value = $(this).parent().attr('data-value');
				if (value && customAutocomplete.items[value]) {
					customAutocomplete.selecta(customAutocomplete.items[value]);
				}
			});
		});
		//Ajax advanced search ends
		//--></script>
	<style>
		#search .dropdown-menu {z-index: 666 !important; background: #fff; width: 100%;}
		#search .dropdown-menu li:nth-child(even){background: #FFFFFF;  border: 1px solid #dbdee1;}
		#search .dropdown-menu li:nth-child(odd){background: #E4EEF7;  border: 1px solid #fff;}
		#search .dropdown-menu li, .ui-menu .ui-menu-item { margin-bottom: 10px;}
		#search .dropdown-menu a {border-radius: 0; white-space: normal; }
		#search .ajaxadvance { width: 100%; min-height: <?php echo (int)$ajaxadvancesearch_imageheight+ (int)$ajaxadvancesearch_imageheight*20/100;?>px; }
		#search .ajaxadvance .name { margin:0; }
		#search .ajaxadvance .image { display:inline-flex; float: left; margin-right:10px; width: <?php echo (int)$ajaxadvancesearch_imagewidth;?>px; }
		#search .ajaxadvance .content { display:inline-block;	max-width: 300px;}
		#search .ajaxadvance .content > div { margin-top:5px;	}
		#search .ajaxadvance .price-old {color: #ff0000; text-decoration: line-through; }
		#search .ajaxadvance .highlight {color: #38b0e3; }
	</style>
	<!--
    /**
        *Ajax advanced search ends
        */
    -->
	<?php } ?>
<nav class="nav-container" role="navigation">
<div class="nav-inner container">
<!-- ======= Menu Code START ========= -->
<?php if ($categories) { ?>
<!-- Opencart 3 level Category Menu-->
	<div id="menu" class="main-menu">
  		<ul class="main-navigation">
  			<?php 	if(!isset($this->request->get['route']) || $this->request->get['route'] == 'common/home') { ?>
				<li> <a href="#"><?php echo $text_home; ?></a></li>
			<?php }else{  ?>
				<li> <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
			<?php } ?>
				<li class="level0"><a><span>Товары</span></a>
					<?php if ($categories) { ?>
						<?php $i = count($categories); ?>
						<span class="active_menu"></span>
						<div class="categorybg">
							<div class="categoryinner">
								<?php $i=1; ?>
								<?php foreach ($categories as $category_2) { ?>
									<ul>
										<li class="categorycolumn"><b><a class="submenu1" href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a></b>
											<?php if($category_2['children']) { ?>
												<?php $j = count($category_2['children']); ?>
												<?php if($j<=10) { ?>
													<div class="cate_inner_bg">
										  				<ul>
											 				<?php foreach ($category_2['children'] as $category_3) { ?>
											 					<li style="padding-right:6px;"><a class="submenu2" href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
															<?php } ?>
										  				</ul>
													</div>
												<?php } else { ?>
													<div>
										  				<ul>
										  					<?php $j=0; ?>
											 				<?php foreach ($category_2['children'] as $category_3) { ?>
											 					<li style="padding-right:6px;"><a class="submenu2" href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
											 					<?php if (++$j == 10) ?>
											 				<?php } ?>
											 				<li style="padding-right:6px;"><a class="submenu2" href="<?php echo $category_2['href']; ?>">More....</a></li>
										  				</ul>
													</div>
												<?php } ?>
										</li>
											<?php } ?>
									</ul>
								<?php } ?>
							</div>
						</div>
					<?php } ?>

				</li>
				<li class="level0"><a><span>Как сделать заказ</span></a></li>
				<li class="level0"><a><span>Пункты доставки</span></a></li>
				<li class="level0"><a><span>Скидки и акции</span></a></li>
			<?php if(isset($blog_enable)){   ?>
				<li>
					<a href="<?php echo $all_blogs; ?>">
						<span data-hover="<?php echo $text_blog; ?>"><?php echo $text_blog; ?></span>
					</a></li>
			<?php  } ?>

</div>
<?php } ?>
<!--  =============================================== Mobile menu start  =============================================  -->
<div id="res-menu" class="main-menu nav-container1">
	<div class="nav-responsive"><span>Menu</span><div class="expandable"></div></div>
    <ul class="main-navigation">
      <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>

        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>										
				<li>
				<?php if(count($category['children'][$i]['children'])>0){ ?>
					<a href="<?php echo $category['children'][$i]['href']; ?>" class="activSub" <?php /*?>onmouseover='JavaScript:openSubMenu("<?php echo $category['children'][$i]['id']; ?>")'<?php */?>><?php echo $category['children'][$i]['name'];?></a> 					
				<?php } else { ?>				
					<a href="<?php echo $category['children'][$i]['href']; ?>" <?php /*?>onmouseover='JavaScript:closeSubMenu()'<?php */?> ><?php echo $category['children'][$i]['name']; ?></a>
				<?php } ?>

				<?php if ($category['children'][$i]['children']) { ?>
				<?php /*?><div class="submenu" id="id_menu_<?php echo $category['children'][$i]['id']; ?>"><?php */?>
				<ul>
				<?php for ($wi = 0; $wi < count($category['children'][$i]['children']); $wi++) { ?>
					<li><a href="<?php echo $category['children'][$i]['children'][$wi]['href']; ?>"  ><?php echo $category['children'][$i]['children'][$wi]['name']; ?></a></li>
				 <?php } ?>
				</ul>
				<?php /*?></div><?php */?>
			  <?php } ?>		  
			</li>		
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>

      <?php } ?>
    </li>
    <?php } ?>
	<?php if(isset($blog_enable)){   ?>
       	<li> <a href="<?php echo $all_blogs; ?>"><?php echo $text_blog; ?></a></li>       
	 <?php  } ?>
    </ul>
	</div>
<!--  ================================ Mobile menu end   ======================================   --> 
<!-- ======= Menu Code END ========= -->
 
</div>
</nav> 
</div> 

