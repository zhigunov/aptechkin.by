<div class="row">
 <!-- <div class="col-sm-6">
    <h2><?php echo $text_new_customer; ?></h2>
    <p><?php echo $text_checkout; ?></p>
    <div class="radio">
      <label>
        <?php if ($account == 'register') { ?>
        <input type="radio" name="account" value="register" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="register" />
        <?php } ?>
        <?php echo $text_register; ?></label>
    </div>
    <?php if ($checkout_guest) { ?>
    <div class="radio">
      <label>
        <?php if ($account == 'guest') { ?>
        <input type="radio" name="account" value="guest" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="guest" />
        <?php } ?>
        <?php echo $text_guest; ?></label>
    </div>
    <?php } ?>
    <p><?php echo $text_register_account; ?></p>
    <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>-->
  <div class="col-sm-3" ></div>
  <div class="col-sm-6" >
    <h2><?php echo $text_returning_customer; ?></h2>
    <p id="sendsms">Смс с паролем придет на указанный телефон в течение нескольких минут</p>
    <div class="form-group">
      <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
      <input type="text" name="telephone" value="" placeholder="+375__-_______" id="input-telephone" class="form-control" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
      <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
      <div class="forget-password" ><a href="" id="apisms"><?php echo $text_forgotten; ?></a></div></div>
    <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>

<script>


  // Register
  $(document).delegate('#apisms', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/register/savetel',
      type: 'post',
      data: $('#collapse-checkout-option :input'),
      dataType: 'json',
      beforeSend: function() {
        $('#apisms').button('loading');
      },
      complete: function() {
        $('#apisms').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');
        $('#sendsms').css('display', "block");
        //alert(json['password']);


      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  return false;
  });
</script>
