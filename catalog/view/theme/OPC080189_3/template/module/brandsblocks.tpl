<div class="container popular" style="width:960px; padding-top: 60px;">
	<!-- <h1><?php echo $name; ?></h1>
	<p>Наведите на бренд для большей информации</p> -->
  <div class="row">
  <?php  foreach ($manufacturers as $manufacturer) {?>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 nopadding">
      <div onclick="location.href='<?php echo $manufacturer['href']; ?>'" class="product-thumb transition nomargin" style="cursor: pointer; background-color: <?php echo $manufacturer['color_brands']; ?> ; height:<?php echo $manufacturer['height'];?>px; width: <?php echo $manufacturer['width'];?>px;">
      <div>
        <div class="image noborder" >
          <a><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" title="<?php echo $manufacturer['name']; ?>" class="img-responsive" /></a>
		    </div>
      </div>
      <div>  
        <div class="description">
  		    <h3 class="white"><?php echo $manufacturer['name']; ?></h3>
		       <?php echo '<br>'; ?>
		      <p class="white"><?php echo $manufacturer['description']; ?></p>
	      </div>
        <!-- <div class="buttonbrendsblocks">
          <button type="button" onclick="location.href='<?php echo $manufacturer['href']; ?>'"><span class="">Подробнее</span></button>
        </div> -->
      </div>
    </div>
  </div>
    <?php } ?>
  </div>
</div>
