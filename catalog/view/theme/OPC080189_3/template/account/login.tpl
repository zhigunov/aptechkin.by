<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
	  <!--<h2><?php echo $heading_title; ?></h2>-->
        <h2></h2>
        <div class="col-sm-3">
        <!--  <div class="well">
            <h3><?php echo $text_new_customer; ?></h3>
            <p><strong><?php echo $text_register; ?></strong></p>
            <p><?php echo $text_register_account; ?></p>
            <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>-->
        </div>
        <div id="collapse-checkout-option" class="col-sm-6">
          <h3><?php echo $text_returning_customer; ?></h3>
          <p id="sendsms">Смс с паролем придет на указанный телефон в течение нескольких минут</p>
          <div class="form-group">
            <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <input type="text" name="telephone" value="" placeholder="+375__-_______" id="input-telephone" class="form-control" />
          </div>
          <div class="form-group">
            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
            <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
            <div class="forget-password" ><a href="" data-loading-text="Идет отправка смс" id="apisms"><?php echo $text_forgotten; ?></a></div></div>
          <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />

        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

<script>


  // Register
  $(document).delegate('#apisms', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/register/savetel',
      type: 'post',
      data: $('#input-telephone'),
      dataType: 'json',
      beforeSend: function() {
        $('#apisms').button('loading');
      },
      complete: function() {
        $('#apisms').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');
        $('#sendsms').css('display', "block");
        //alert(json['password']);


      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
    return false;
  });

  // Login
  $(document).delegate('#button-login', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/login/save',
      type: 'post',
      data: $('#collapse-checkout-option :input'),
      dataType: 'json',
      beforeSend: function() {
        $('#button-login').button('loading');
      },
      complete: function() {
        $('#button-login').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');

        if (json['redirect']) {
          location = json['redirect'];
        } else if (json['error']) {
          $('#collapse-checkout-option').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          // Highlight any found errors
          $('input[name=\'telephone\']').parent().addClass('has-error');
          $('input[name=\'password\']').parent().addClass('has-error');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script>