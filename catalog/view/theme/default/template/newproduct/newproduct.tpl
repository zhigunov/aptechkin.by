<? /* ?> <?php echo $header; ?>
<div class="main-columns container space-30">
  <div class="row">
    <div class="col-sm-3"> <?php echo $column_left; ?></div>
        	<?php if ($column_left && $column_right) { ?>
       			<?php $class = 'col-sm-6'; ?>
        	<?php } elseif ($column_left || $column_right) { ?>
        		<?php $class = 'col-sm-9'; ?>
        	<?php } else { ?>
        		<?php $class = 'col-sm-12'; ?>
        	<?php } ?>
          <?php echo $column_right; ?>
      <div id="content" class="<?php echo $class; ?>">
               <?php echo $content_top; ?>
               <?php echo $content_bottom; ?>
      </div>
  </div>
</div>
<?php echo $footer; ?>
<? */?>

<?php
 $helper =  ThemeControlHelper::getInstance( $this->registry );
echo $header; ?>
<?php require( ThemeControlHelper::getLayoutPath( 'common/mass-header.tpl' )  ); ?>
<div class="main-columns container">
    <div class="row"><?php if( $SPAN[0] ): ?>
        <aside id="sidebar-left" class="col-md-<?php echo $SPAN[0];?>">
            <?php echo $column_left; ?>
        </aside>
        <?php endif; ?>

        <div id="sidebar-main" class="col-md-<?php echo $SPAN[1];?>">
            <div id="content">
                <div class="pull-left">
                    <?php require( ThemeControlHelper::getLayoutPath( 'common/mass-container.tpl' )  ); ?>
                </div>
                <?php if ($thumb || $description) { ?>
                <div class="category-info clearfix hidden-xs hidden-sm">
                    <?php if ($thumb) { ?>
                    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" /></div>
                    <?php } ?>
                    <?php if ($description) { ?>
                    <div class="category-description wrapper">
                        <?php echo $description; ?>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php echo $content_top; ?>



                <?php if ($products) { ?>
                <div class="panel-heading" style="padding-top: 0px">
                    <h4 class="panel-title"><?php echo $heading_title; ?></h4>
                </div>
                <?php require( ThemeControlHelper::getLayoutPath( 'common/product_collection.tpl' ) );  ?>
                <?php } ?>

                <?php if (!$categories && !$products) { ?>
                <div class="content"><div class="wrapper"><?php echo $text_empty; ?></div></div>
                <div class="buttons">
                    <div class="right"><a href="<?php echo $continue; ?>" class="button btn btn-default"><?php echo $button_continue; ?></a></div>
                </div>
                <?php } ?>


                <?php echo $content_bottom; ?></div>
        </div>
        <?php if( $SPAN[2] ): ?>
        <aside id="sidebar-right" class="col-md-<?php echo $SPAN[2];?>">
            <?php echo $column_right; ?>
        </aside>
        <?php endif; ?></div>
</div>
<?php echo $footer; ?>