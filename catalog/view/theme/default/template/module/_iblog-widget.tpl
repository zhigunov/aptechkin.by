<?php if(!empty($custom_css)): ?>
	<style>
		<?php echo htmlspecialchars_decode($custom_css); ?>
    </style>
<?php endif; ?>
	<!-- <?php if ($featured_post!== false && $featured=='yes') { ?>
    	<div class="panel panel-default iblog-widget">
			<div class="panel-heading"><div class="bundle-title"><a href="<?php echo $featured_post['href']; ?>"><?php echo $featured_post['title']; ?></a></div></div>
			<div class="panel-body iblog-box-content">
				<?php if (!empty($featured_post['image'])) : ?>
					<a href="<?php echo $featured_post['href']; ?>"><img src="<?php echo $featured_post['image']; ?>" class="iblog-featured-image" alt="<?php echo $featured_post['title']; ?>" /></a>
				<?php endif; ?>
                <div class="iblog-featured-description">
                	<?php echo $featured_post['excerpt']; ?>
                </div>
                <div class="iblog-button">
                	<a href="<?php echo $featured_post['href']; ?>" class="btn btn-primary"><?php echo $iblog_button; ?></a>
                </div>
			</div>
		</div>
	<?php } ?> -->

<!-- <div class="panel panel-default iblog-panel">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo $heading_title; ?></h4>
	</div>
	<div class="panel-content iblog-box-content">
	<?php var_dump($posts); ?>
        <?php if (!empty($posts)) { ?>
			<ul class="iblog-box-post">
				<?php foreach ($posts as $post) { ?>
					<li>
						<div class="iblog-post">
							<a href="<?php echo $post['href']; ?>"<?php echo ($post['post_id'] == $post_id) ? ' class="active"' : ''; ?>><?php echo $post['title']; ?></a>
						</div>
					</li>
				<?php } ?>
			</ul>
		<?php } else { ?>
				<div class="iblog-noposts"><?php echo $no_posts; ?></div>
		<?php } ?>
	</div>
</div> -->


<?php // var_dump($posts); ?>
<?php if( !empty($posts) ) { ?>
<div id="blog-carousel" class="widget-blogs owl-carousel-play latest-posts latest-posts-v1 panel-default <?php echo $addition_cls?> <?php if ( isset($stylecls)&&$stylecls) { ?>box-<?php echo $stylecls; ?><?php } ?>">

	<div class="panel-heading"><h4 class="panel-title"><?php echo $heading_title?></h4></div>

	<div class="panel-body">
		<div class="carousel-inner owl-carousel" data-show="1" data-pagination="false" data-navigation="true">

				<?php  $pages = array_chunk( $posts, 3); $span = 4; $cols = 3; ?>

				<?php foreach ($pages as  $k => $tblogs ) {   ?>
					<div class="item <?php if($k==0) {?>active<?php } ?>">
						<?php foreach( $tblogs as $i => $blog ) {  $i=$i+1;?>
							<?php if( $i%$cols == 1 ) { ?>
							<div class="row products-row">
							<?php } ?>

								<div class="col-lg-<?php echo floor(12/$cols);?> col-md-<?php echo floor(12/$cols);?> col-sm-<?php echo floor(12/$cols);?>">
						  			<div class="blog-item">
										<div class="latest-posts-body">
											<div class="latest-posts-img-profile">
												<?php if( $blog['image']  )  { ?>
													<div class="latest-posts-image effect">
														<a href="<?php echo $blog['href'];?>" class="blog-article">
															<img src="image/<?php echo $blog['image'];?>" title="<?php echo $blog['title'];?>" alt="<?php echo $blog['title'];?>" class="img-responsive"/>
														</a>								
													</div>
												<?php } ?>							
											</div>
											<div class="latest-posts-meta">
												<h4 class="latest-posts-title">
													<a href="<?php echo $blog['href'];?>" title="<?php echo $blog['title'];?>"><?php echo $blog['title'];?></a>
												</h4>						
												<!-- <div class="blog-meta">						
													<span class="created">
														<span class="day"><?php echo date("d",strtotime($blog['created']));?></span>
														<span class="month"><?php echo date("M",strtotime($blog['created']));?></span> 								
													</span>		
													<span class="comment_count"><i class="fa fa-comments"></i> <?php echo $blog['comment_count'];?></span>
									
												</div> -->

												<div class="shortinfo">
													<?php $blog['excerpt'] = strip_tags($blog['excerpt']); ?>
													<?php echo utf8_substr( $blog['excerpt'],0, 150 );?>...
												</div>
												<div class="blog-readmore">
													<a href="<?php echo $blog['href'];?>">Читать далее</a>
												</div>
											</div>						
										</div>						
									</div>   		
								</div>

						  	<?php if( $i%$cols == 0 || $i==count($tblogs) ) { ?>
							</div>
							<?php } ?>
						<?php } //endforeach; ?>
					</div>
				<?php } ?>
		</div>
		<?php if( count($posts) > 3 ) { ?>
		<div class="carousel-controls">
			<a class="carousel-control left" href="#blog-carousel"   data-slide="prev"><i class="fa fa-angle-left"></i></a>
			<a class="carousel-control right" href="#blog-carousel"  data-slide="next"><i class="fa fa-angle-right"></i></a>
		</div>		
		<?php } ?>
	</div>
</div>
<?php } ?>