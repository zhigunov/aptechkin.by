<?php echo $header; ?>
<?php require( ThemeControlHelper::getLayoutPath( 'common/mass-header.tpl' )  ); ?>
<div class="container">

    <div class="row">
    	<?php // echo $column_left; ?>
    	<?php if ($column_left && $column_right) { ?>
   			<?php $class = 'col-sm-6'; ?>
    	<?php } elseif ($column_left || $column_right) { ?>
    		<?php $class = 'col-sm-12'; ?>
    	<?php } else { ?>
    		<?php $class = 'col-sm-12'; ?>
    	<?php } ?>
    	<div id="content" class="<?php echo $class; ?>">
        	<?php echo $content_top; ?>
   			<h1><?php echo $heading_title; ?></h1>
                       
            <div class="buttons">
    			<span style="font-size: 18px;">Ваш город: </span>
                <select id="city_select" class="form-control" style="display: inline; width: auto; font-size: 14px;">
                <?php foreach ($cities as $city) { ?>
                  <?php if ($city == "Минск"){ ?>
                    <option selected="selected"><?php echo $city; ?></option>
                  <?php }else{ ?>
                    <option><?php echo $city; ?></option>
                  <?php } ?>
                <?php } ?>
                </select>
            </div>
            <div class="map-index row">
            	<div class="col-xs-12 col-sm-4 storepickup-data">
                	<ul class="list-group store-list" data-name="store-select-dropdown">
                    	<?php $i=0; foreach ($stores as $store) { ?>
                            <li class="list-group-item store-list-item<?php echo ($i==0) ? ' active' : ''; ?>" data-value="store-<?php echo $store['id']; ?>">    
                                <h3 class="store-name"><?php echo $store['Name'][$language_id]; ?></h3>
                                <p><?php echo $store['Address'][$language_id]; ?>&nbsp;</p>
                                <p><?php echo $store['Regime'][$language_id]; ?>&nbsp;</p>
                                <div class="store-office">
                                    <span class="phone-label"><i class="fa fa-phone"></i></span>
                                    <a href="tel:<?php echo $store['Phone']; ?>"><span class="phone-number"><?php echo $store['Phone']; ?></span></a>
                                </div>
                            </li>
                        <?php $i++; } ?>
					</ul>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="widget-map-holder">
                        <div id="currency_widget_map"></div>
                    </div>
                    <script type="text/javascript">
                    (function($) {
                        var prev_infowindow =false;
                        function initialize_offices_map() {
									<?php $i=0; foreach ($stores as $store) { ?>
										<?php if ($i==0) { ?>
                                            if (navigator.geolocation) {
                                                navigator.geolocation.getCurrentPosition(function(position) {
                                                    var LAT_LNG = new google.maps.LatLng(position.coords.longitude, position.coords.latitude);
                                                });
                                            }
                                            if(!LAT_LNG){
                                                var LAT_LNG = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);   
                                            }
											var mapOptions = {
												center: LAT_LNG,
												zoom: 11
											};
											offices_map = new google.maps.Map(document.getElementById("currency_widget_map"), mapOptions);
										<?php } ?>
									
										LAT_LNG_<?php echo $store['id']; ?> = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);
										office_marker_362 = new google.maps.Marker({
											position: LAT_LNG_<?php echo $store['id']; ?>,
											map: offices_map,
                                            id: <?php echo $store['id']; ?>,
											title: '<?php echo $store['Name'][$language_id]; ?>',
                                            address: '<?php echo $store['Address'][$language_id]; ?>',
                                            regime: '<?php echo $store['Regime'][$language_id]; ?>',
                                            phone: '<?php echo $store['Phone']; ?>'
										});
                                          info = '<div><p>Аптека: <?php echo $store['Name'][$language_id]; ?></p><p>Адрес: <?php echo $store['Address'][$language_id]; ?></p><p>Режим работы: <?php echo $store['Regime'][$language_id]; ?></p><p>Телефон: <?php echo $store['Phone']; ?></p></div>'  
                                          makeInfoWin(office_marker_362, info, offices_map);                        
									<?php $i++; } ?>
                                    
                                    function makeInfoWin(marker, data, map) {
                                        
                                      var infowindow = new google.maps.InfoWindow({ content: data });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            if( prev_infowindow ) {
                                               prev_infowindow.close();
                                            }
                                            prev_infowindow = infowindow;
                                            infowindow.open(map,marker);

                                            $('.store-list-item').removeClass('active');
                                            var office_id = 'store-'+marker.id;
                                            $('[data-id='+office_id+']').addClass('active');
                                            offices_map.setZoom(15);
                                            map.setCenter(marker.getPosition());

                                            var top2 = $('.active').position().top;
                                            if (parseInt(parseInt($('.store-list').scrollTop())) != 0){
                                                var top = parseInt($('.store-list').scrollTop()) + parseInt(top2);
                                            }else{
                                              var top = parseInt(top2);
                                            }
                                            
                                            console.log(top2);

                                            $('.store-list').animate({ scrollTop: top }, 500);
                                            

                                        });  

                                      }
                                    
                            }
                        
                        google.maps.event.addDomListener(window, 'load', initialize_offices_map);
    
                        $(document).ready(function() {
						   $('.store-list-item').on("click", function(event) {
								$('.store-list-item').removeClass('active');
								var office_id = $(this).data('value');
								$(this).addClass('active');
					
								// Map Office Marker Switch
								if (typeof window["LAT_LNG_" + office_id.replace('store-','')] !== 'undefined') {
									var LAT_LNG = window["LAT_LNG_" + office_id.replace('store-','')];
									offices_map.setCenter(LAT_LNG);
                                    offices_map.setZoom(15);

								}
							});

						});


                        $(document).ready(function() {
                           $('.city').click(function(event) {
                                
                                var city = $(this).data('value');
                                
                                $('a').removeClass('btn-city-activeee');
                                $(this).addClass('btn-city-activeee');
                                $.get("https://maps.google.com/maps/api/geocode/json?address=Беларусь"+city+"&sensor=false",
                                    function(data) {     
                                        var lat = data.results[0].geometry.location.lat;
                                        var lng = data.results[0].geometry.location.lng;                      
                                        var LAT_LNG = new google.maps.LatLng(lat, lng);
                                        
                                        offices_map.panTo(LAT_LNG);
                                        if (city == "Минск"){
                                            offices_map.setZoom(11);
                                        }else{
                                            offices_map.setZoom(13);
                                        }
                                       
                                     
                                  }
                                );
                                
                                
                            });

                        });

                        $(document).ready(function() {
                           $('#city_select').change(function(event) {
                                
                                var city = $(this).val();
                                
                                $.get("https://maps.google.com/maps/api/geocode/json?address=Беларусь"+city+"&sensor=false",
                                    function(data) {     
                                        var lat = data.results[0].geometry.location.lat;
                                        var lng = data.results[0].geometry.location.lng;                      
                                        var LAT_LNG = new google.maps.LatLng(lat, lng);
                                        
                                        offices_map.panTo(LAT_LNG);
                                        if (city == "Минск"){
                                            offices_map.setZoom(11);
                                        }else{
                                            offices_map.setZoom(13);
                                        }
                                       
                                     
                                  }
                                );
                                
                                
                            });

                        });

                    })(jQuery); 
                    
                    
                    </script>
            	</div>
            </div>
            <div class="buttons">
            	<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          	</div>
            <?php echo $content_bottom; ?>
        </div>
    	<?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?> 

