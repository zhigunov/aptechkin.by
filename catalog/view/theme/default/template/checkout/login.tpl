<div class="row">
 <!-- <div class="col-sm-6">
    <h2><?php echo $text_new_customer; ?></h2>
    <p><?php echo $text_checkout; ?></p>
    <div class="radio">
      <label>
        <?php if ($account == 'register') { ?>
        <input type="radio" name="account" value="register" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="register" />
        <?php } ?>
        <?php echo $text_register; ?></label>
    </div>
    <?php if ($checkout_guest) { ?>
    <div class="radio">
      <label>
        <?php if ($account == 'guest') { ?>
        <input type="radio" name="account" value="guest" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="guest" />
        <?php } ?>
        <?php echo $text_guest; ?></label>
    </div>
    <?php } ?>
    <p><?php echo $text_register_account; ?></p>
    <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>-->
  <div class="col-sm-2" ></div>
  <div class="col-sm-8" id="login_block" >
    <h2><?php echo $text_returning_customer; ?></h2>
    <p id="sendsms">Смс с паролем придет на указанный телефон в течение нескольких минут</p>
    <div class="col-sm-8">
      <div class="form-group">
        <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
        <input type="text" name="telephone" value="" placeholder="+375 (__) ___-__-__" id="input-telephone" class="form-control" />
      </div>
    </div>
    <div class="col-sm-4">
      <!-- <a href="" data-loading-text="Идет отправка смс" id="apisms">Получить пароль по смс</a> -->
    </div>
    <div class="col-sm-8">
      <div class="form-group">
                    <p><i class="fa fa-info-circle"></i> Если вы регистрируетесь впервые или забыли пароль, укажите номер телефона и нажмите "Получить пароль по смс".<br>
Если у вас есть пароль,укажите ваш номер телефона и нажмите "Есть пароль"</p></div>
                    <input type="button" id="havepass" value="Есть пароль" class="btn btn-primary" />
                    <input type="button" id="apisms" value="Получить пароль по смс" data-loading-text="Идет отправка смс" class="btn btn-primary" />
                    <p style="margin: 0 0;">&nbsp</p>
                    <label style="display: none;" class="control-label" for="input-password" id="label-input-password"><?php echo $entry_password; ?></label>
                    <input style="display: none;" type="password" name="password" value="" placeholder="Пароль" id="input-password" class="form-control" />
                    <p style="margin: 0 0;">&nbsp</p>
                    
                    <!-- <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" /> -->
                    <input style="display: none;" type="submit" value="<?php echo $button_login; ?>" id="button-login" class="btn btn-primary" />

      
    </div>
     
</div>

<script type="text/javascript">
jQuery(function($){
   $("#input-telephone").mask("+375 (99) 999-99-99");
});
</script>

<script>


  // Register
  $(document).delegate('#apisms', 'click', function() {
    $.ajax({
      url: 'index.php?route=checkout/register/savetel',
      type: 'post',
      data: $('#collapse-checkout-option :input'),
      dataType: 'json',
      beforeSend: function() {
        $('#apisms').button('loading');
      },
      complete: function() {
        $('#apisms').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');
        $('#sendsms').css('display', "block");
        $('#label-input-password').css('display', "block");
        $('#input-password').css('display', "block");
        $('#button-login').css('display', "block");
        //alert(json['password']);
        //alert(json['password']);


      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  return false;
  });

  $(document).delegate('#havepass', 'click', function() {
    $('#label-input-password').css('display', "block");
    $('#input-password').css('display', "block");
    $('#button-login').css('display', "block");
  });
</script>
