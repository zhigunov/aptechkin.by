
<form class="form-horizontal checkout-map-form">
  <?php if ($addresses) { ?>
  <!--<div class="radio" style="display: none">
    <label>
      <input type="radio" name="payment_address" value="existing" checked="checked" />
      <?php echo $text_address_existing; ?></label>
  </div>-->
  <div id="payment-existing" style="display: none;">
    <select name="address_1" id="address_123" class="form-control">
    <option value=""></option>
      <!-- <?php foreach ($addresses as $address) { ?>
      <?php if ($address['apteka_id'] == $address_id) { ?>
      <option id="option-store<?php echo $address['apteka_id']; ?>" value="<?php echo $address['adress']; ?>" selected="selected"><?php echo $address['adress']; ?></option>
      <?php } else { ?>
      <option id="option-store<?php echo $address['apteka_id']; ?>" value="<?php echo $address['adress']; ?>"><?php echo $address['adress']; ?></option>
      <?php } ?>
      <?php } ?> -->
    </select>
  </div>








<div class="container">
    <div class="row">
      <div id="content123" class="col-sm-10">
        <div id="payment-existing" >
          <span style="font-size: 18px;">Ваш город: </span>
          <select id="city_select" class="form-control" style="display: inline; width: auto; font-size: 14px;">
            <?php foreach ($cities as $city) { ?>
              <?php if ($city == "Минск"){ ?>
                <option selected="selected"><?php echo $city; ?></option>
              <?php }else{ ?>
                <option><?php echo $city; ?></option>
              <?php } ?>
            <?php } ?>
            <!-- <?php foreach ($addresses as $address) { ?>
            <?php if ($address['apteka_id'] == $address_id) { ?>
            <option id="option-store<?php echo $address['apteka_id']; ?>" value="<?php echo $address['adress']; ?>" selected="selected"><?php echo $address['adress']; ?></option>
            <?php } else { ?>
            <option id="option-store<?php echo $address['apteka_id']; ?>" value="<?php echo $address['adress']; ?>"><?php echo $address['adress']; ?></option>
            <?php } ?>
            <?php } ?> -->
          </select>
        </div>
        <div class="buttons" style="display: none;">
          <?php foreach ($cities as $city) { ?>
            <?php if ($city == "Минск"){ ?>
                <div class="pull-right"><a class="btn btn-city btn-city-activeee city" data-value="<?php echo $city; ?>" ><?php echo $city; ?></a></div>
            <?php }else{ ?>
                <div class="pull-right"><a class="btn btn-city city" data-value="<?php echo $city; ?>" ><?php echo $city; ?></a></div>
            <?php } ?>
        <?php } ?>
        </div>
        <!-- <h1><?php echo $heading_title; ?></h1> -->
          <div class="map-index row">
            <div class="col-xs-12 col-sm-4 storepickup-data">
              <ul class="list-group store-list" data-name="store-select-dropdown">
                <?php $i=0; foreach ($stores as $store) { ?>
                  <li class="list-group-item store-list-item<?php echo ($i==0) ? ' active' : ''; ?>" id="store<?php echo $store['id']; ?>" data-value="store-<?php echo $store['id']; ?>" data-address="<?php echo $store['Address'][$language_id]; ?> Апт№<?php echo $store['Number']; ?>" data-company="<?php echo $store['Idseti'][$language_id]; ?>">    
                      <h3 class="store-name"><?php echo $store['Name'][$language_id]; ?></h3>
                      <p><?php echo $store['Address'][$language_id]; ?>&nbsp;</p>
                      <p><?php echo $store['Regime'][$language_id]; ?>&nbsp;</p>
                      <p style="display: none">Апт№<?php echo $store['Number']; ?></p>
                      <div class="store-office">
                          <span class="phone-label"><i class="fa fa-phone"></i></span>
                          <a href="tel:<?php echo $store['Phone']; ?>"><span class="phone-number"><?php echo $store['Phone']; ?></span></a>
                      </div>
                  </li>
                <?php $i++; } ?>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-8">
              <div class="widget-map-holder">
                <div id="currency_widget_map"></div>
              </div>
              <script type="text/javascript">
                (function($) {
                        function initialize_offices_map() {
                  <?php $i=0; foreach ($stores as $store) { ?>
                    <?php if ($i==0) { ?>
                                            if (navigator.geolocation) {
                                                navigator.geolocation.getCurrentPosition(function(position) {
                                                    var LAT_LNG = new google.maps.LatLng(position.coords.longitude, position.coords.latitude);
                                                });
                                            }
                                            if(!LAT_LNG){
                                                var LAT_LNG = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);   
                                            }
                      var mapOptions = {
                        center: LAT_LNG,
                        zoom: 11
                      };
                      offices_map = new google.maps.Map(document.getElementById("currency_widget_map"), mapOptions);
                    <?php } ?>
                  
                    LAT_LNG_<?php echo $store['id']; ?> = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);
                    office_marker_362 = new google.maps.Marker({
                      position: LAT_LNG_<?php echo $store['id']; ?>,
                      map: offices_map,
                                            id: <?php echo $store['id']; ?>,
                                            number: <?php echo $store['Number']; ?>,
                                            company: '<?php echo $store['Idseti'][$language_id]; ?>',
                      title: '<?php echo $store['Name'][$language_id]; ?>',
                                            address: '<?php echo $store['Address'][$language_id]; ?>',
                                            regime: '<?php echo $store['Regime'][$language_id]; ?>',
                                            phone: '<?php echo $store['Phone']; ?>'
                    });
                                          info = '<div><p>Аптека: <?php echo $store['Name'][$language_id]; ?></p><p>Адрес: <?php echo $store['Address'][$language_id]; ?></p><p>Режим работы: <?php echo $store['Regime'][$language_id]; ?></p><p>Телефон: <?php echo $store['Phone']; ?></p></div>'  
                                          makeInfoWin(office_marker_362, info, offices_map);                        
                  <?php $i++; } ?>
                                    var prev = false;
                                    function makeInfoWin(marker, data, map) {
                                        
                                      var infowindow = new google.maps.InfoWindow({ content: data });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            if(prev)
                                              prev.close();
                                            infowindow.open(map,marker);
                                            prev = infowindow;

                                            $('.store-list-item').removeClass('active');
                                            var office_id = 'store-'+marker.id;
                                            $('[data-value='+office_id+']').addClass('active');
                                            
                                            var top2 = $('.active').position().top;
                                            if (parseInt(parseInt($('.store-list').scrollTop())) != 0){
                                                var top = parseInt($('.store-list').scrollTop()) + parseInt(top2);
                                            }else{
                                              var top = parseInt(top2);
                                            }
                                        
                                            $('.store-list').animate({ scrollTop: top }, 500);
                                            
                                            document.getElementById("address_123").options[0].text = marker.address+' Апт№'+marker.number;
                                            document.getElementById("address_123").options[0].value = marker.address+' Апт№'+marker.number;
                                            $('#confirm-pharm').html(marker.address+' Апт№'+marker.number);
                                            document.getElementById("input-payment-company").value = marker.company;

                                        });  

                                      }
                                    
                            }
                        
                        google.maps.event.addDomListener(window, 'load', initialize_offices_map);
    
                        $(document).ready(function() {
                          var address_select = $('.store-list-item').data('address');
                          var company_select = $('.store-list-item').data('company');
                          document.getElementById("address_123").options[0].text = address_select;
                          document.getElementById("address_123").options[0].value = address_select;
                          $('#confirm-pharm').html(address_select);
                          document.getElementById("input-payment-company").value = company_select;

                           $('.store-list-item').on("click", function(event) {
                            var address_select = $(this).data('address');
                            var company_select = $(this).data('company');
                            document.getElementById("address_123").options[0].text = address_select;
                            document.getElementById("address_123").options[0].value = address_select;
                            $('#confirm-pharm').html(address_select);
                            document.getElementById("input-payment-company").value = company_select;
                            $('.store-list-item').removeClass('active');
                            var office_id = $(this).data('value');
                            $(this).addClass('active');
                      
                            // Map Office Marker Switch
                            if (typeof window["LAT_LNG_" + office_id.replace('store-','')] !== 'undefined') {
                              var LAT_LNG = window["LAT_LNG_" + office_id.replace('store-','')];
                              offices_map.setCenter(LAT_LNG);
                                                offices_map.setZoom(15);

                            }
                          });

                        });


                        $(document).ready(function() {
                           $('.city').click(function(event) {
                                
                                var city = $(this).data('value');
                                
                                $('a').removeClass('btn-city-activeee');
                                $(this).addClass('btn-city-activeee');
                                $.get("https://maps.google.com/maps/api/geocode/json?address=Беларусь"+city+"&sensor=false",
                                    function(data) {     
                                        var lat = data.results[0].geometry.location.lat;
                                        var lng = data.results[0].geometry.location.lng;                      
                                        var LAT_LNG = new google.maps.LatLng(lat, lng);
                                        
                                        offices_map.panTo(LAT_LNG);
                                        if (city == "Минск"){
                                            offices_map.setZoom(11);
                                        }else{
                                            offices_map.setZoom(13);
                                        }
                                       
                                     
                                  }
                                );
                                
                                
                            });

                        });

                        $(document).ready(function() {
                           $('#city_select').change(function(event) {
                                
                                var city = $(this).val();
                                
                                $.get("https://maps.google.com/maps/api/geocode/json?address=Беларусь"+city+"&sensor=false",
                                    function(data) {     
                                        var lat = data.results[0].geometry.location.lat;
                                        var lng = data.results[0].geometry.location.lng;                      
                                        var LAT_LNG = new google.maps.LatLng(lat, lng);
                                        
                                        offices_map.panTo(LAT_LNG);
                                        if (city == "Минск"){
                                            offices_map.setZoom(11);
                                        }else{
                                            offices_map.setZoom(13);
                                        }
                                       
                                     
                                  }
                                );
                                
                                
                            });

                        });

                    })(jQuery); 
                    
                    
              </script>
            </div>
          </div>
        </div>
    </div>
    <p style="font-size: 22px;">Выбранная аптека: <span id="confirm-pharm"></span></p>
</div>









  <div class="radio" style="display: none">
    <label>
      <input type="radio" name="payment_address" value="new" checked="checked"/>
      <?php echo $text_address_new; ?></label>
  </div>
  <?php } ?>
  <br />
  <div id="payment-new" style="display: none">
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-firstname"><?php echo $entry_firstname; ?></label>
      <div class="col-sm-10">
        <input type="text" name="firstname" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-payment-firstname" class="form-control" />
      </div>
    </div>
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-lastname"><?php echo $entry_lastname; ?></label>
      <div class="col-sm-10">
        <input type="text" name="lastname" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-payment-lastname" class="form-control" />
      </div>
    </div>
    <div class="form-group" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-company"><?php echo $entry_company; ?></label>
      <div class="col-sm-10">
        <input type="text" name="company" value="" placeholder="<?php echo $entry_company; ?>" id="input-payment-company" class="form-control" />
      </div>
    </div>
    <!--<div class="form-group required">
      <label class="col-sm-2 control-label" for="input-payment-address-1"><?php echo $entry_address_1; ?></label>
      <div class="col-sm-10">
        <input type="text" name="address_1" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-payment-address-1" class="form-control" />
      </div>
    </div>-->
    <div class="form-group" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-address-2"><?php echo $entry_address_2; ?></label>
      <div class="col-sm-10">
        <input type="text" name="address_2" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-payment-address-2" class="form-control" />
      </div>
    </div>
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-city"><?php echo $entry_city; ?></label>
      <div class="col-sm-10">
        <input type="text" name="city" value="" placeholder="<?php echo $entry_city; ?>" id="input-payment-city" class="form-control" />
      </div>
    </div>
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-postcode"><?php echo $entry_postcode; ?></label>
      <div class="col-sm-10">
        <input type="text" name="postcode" value="" placeholder="<?php echo $entry_postcode; ?>" id="input-payment-postcode" class="form-control" />
      </div>
    </div>
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-country"><?php echo $entry_country; ?></label>
      <div class="col-sm-10">
        <select name="country_id" id="input-payment-country" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] == $country_id) { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group required" style="display: none">
      <label class="col-sm-2 control-label" for="input-payment-zone"><?php echo $entry_zone; ?></label>
      <div class="col-sm-10">
        <select name="zone_id" id="input-payment-zone" class="form-control">
        </select>
      </div>
    </div>
    <?php foreach ($custom_fields as $custom_field) { ?>
    <?php if ($custom_field['location'] == 'address') { ?>
    <?php if ($custom_field['type'] == 'select') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'radio') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="radio">
            <label>
              <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'checkbox') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'text') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'textarea') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo $custom_field['value']; ?></textarea>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'file') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <button type="button" id="button-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
        <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" />
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'date') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group date">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'time') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group time">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'datetime') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group datetime">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php } ?>
    <?php } ?>
  </div>
  <div class="buttons clearfix">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-address" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
    </div>
  </div>
</form>
<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
  if (this.value == 'new') {
    $('#payment-existing').hide();
    $('#payment-new').show();
  } else {
    $('#payment-existing').show();
    $('#payment-new').hide();
  }
});
//--></script>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group:first').before(this);
  }
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $(node).parent().find('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.time').datetimepicker({
  pickDate: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value=""><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#collapse-payment-address select[name=\'zone_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');
//--></script>