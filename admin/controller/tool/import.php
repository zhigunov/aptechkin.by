<?php
class ControllerToolImport extends Controller {
	private $error = array();

	public function index() {

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// Открываем созданный dbf-файл
	    $dbh = dbase_open("export/import/specpric.DBF",2);
	    if(!$dbh) exit("Ошибка - невозможно открыть файл");
	    // Число записей в файле
	    $numrecords = dbase_numrecords($dbh);
	    // В цикле заполняем массив $arr записями из файла
	    for($i = 1; $i < $numrecords; $i++)
	    {
	      $arr[] = dbase_get_record_with_names($dbh, $i);
	    }
		//var_dump($arr);
	    foreach ($arr as $arkey => $arvalue) {
	      	foreach ($arvalue as $key => $value) {
		      	if ($key == "POSTAV"||$key == "NAIM"||$key == "FIRMA"||$key == "COUNTRY"||$key == "TYPE"||$key == "ATX3"||$key == "MNN"||$key == "SRGOD"){
		      		$arr[$arkey][$key] = iconv('CP866', 'UTF-8//IGNORE', $value);

		      	}
				if ($key == "USLOV"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "БезРецепта"){
						$arr[$arkey][$key] = "Нет";
					}
					if(trim($value) == "RX"){
						$arr[$arkey][$key] = "Да";
					}
				}
				if ($key == "SRGOD"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == ".  ."){
						$arr[$arkey][$key] = "Не имеет огрничений";
					}
				}
				if ($key == "TYPE"){
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "Лек"){
						$arr[$arkey][$key] = "Лекарственные средства";
					}
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "НДС"){
						$arr[$arkey][$key] = "БАД, косметика и др.";
					}
					if(trim(iconv('CP866', 'UTF-8//IGNORE', $value)) == "ИМН"){
						$arr[$arkey][$key] = "Изделия медицинского назанчения";
					}
				}
				if (trim(iconv('CP866', 'UTF-8//IGNORE', $arvalue['TYPE'])) != 'Лек'){
					$arr[$arkey]['MNN'] = "";
				}
			}
	    }

	    // Закрываем dbf-файл
	    dbase_close($dbh);

  		$data['arr'] = array( );
  		foreach ($arr as $ar) {
  			$data['arr'][] = $ar;
  		}

		$this->load->model('tool/import');
		$manufacturers = array();
		$manufacturers = $this->model_tool_import->getManufacturers();

		$manufacturers_to_add = array();
		$flman = 0;
		$flman_to_add = 0;
		foreach ($arr as $arkey => $arvalue) {
	      	foreach ($arvalue as $key => $value) {
		      	if ($key == "FIRMA"){
		      		foreach ($manufacturers as $keym => $valuem) {
		      			if(strcasecmp($keym, trim($value)) == 0){
		      				$flman = 1;
			      		}
			      	}
			      	if ($flman == 0){
			      		foreach ($manufacturers_to_add as $value_to_add) {
			      			if (strcasecmp($value_to_add, trim($value)) == 0){
			      				$flman_to_add = 1;
			      			}
			      		}
			      		if ($flman_to_add == 0){
			      			$manufacturers_to_add[] = addslashes(trim($value));
			      		}
			      	}
			      	$flman = 0;
					$flman_to_add = 0;
		      	}
		    }
	    }
	    //var_dump($manufacturers_to_add);
	    $database =& $this->db;
	    //var_dump($manufacturers_to_add);
		$this->model_tool_import->storeManufacturersIntoDatabase($database, $manufacturers_to_add);
		//$this->model_tool_import->delManufacturersIntoDatabase($database);


		$categories = $this->model_tool_import->getCategories();

		$categories_to_add = array();
		$flcat = 0;
		$flcat_to_add = 0;
		foreach ($arr as $arkey => $arvalue) {
			foreach ($arvalue as $key => $value) {
				if ($key == "TYPE"){
					foreach ($categories as $keyc => $valuec) {
						if($keyc == trim($value)){
							$flcat = 1;
						}
					}
					if ($flcat == 0){
						foreach ($categories_to_add as $value_to_add) {
							if ($value_to_add['name'] == trim($value)){
								$flcat_to_add = 1;
							}
						}
						if ($flcat_to_add == 0){
							$categories_to_add[] = array('parent' => 0, 'name' => addslashes(trim($value)));
						}
					}
					$flcat = 0;
					$flcat_to_add = 0;
				}
			}
		}
		$this->model_tool_import->storeCategoriesIntoDatabase($database, $categories_to_add);
		$categories = $this->model_tool_import->getCategories();
		$categories_to_add = array();
		foreach ($arr as $arkey => $arvalue) {
			foreach ($arvalue as $key => $value) {
				if ($key == "ATX3"){
					foreach ($categories as $keyc => $valuec) {
						if($keyc == trim($value)){
							$flcat = 1;
						}
					}
					if ($flcat == 0){
						foreach ($categories_to_add as $value_to_add) {
							if ($value_to_add['name'] == trim($value)){
								$flcat_to_add = 1;
							}
						}
						if ($flcat_to_add == 0){
							$trim = trim($arvalue['TYPE']);
							$parent = $this->model_tool_import->getCategoryId($trim);

							$categories_to_add[] = array('parent' => $parent, 'name' => addslashes(trim($value)));
						}
					}
					$flcat = 0;
					$flcat_to_add = 0;
				}
			}
		}

		$database =& $this->db;

		$this->model_tool_import->storeCategoriesIntoDatabase($database, $categories_to_add);
		//$this->model_tool_import->delCategoriesIntoDatabase($database);

		//var_dump($arr);
		$true = $this->model_tool_import->storeProductsIntoDatabase($database, $arr);

		$ch = curl_init();

		// установка URL и других необходимых параметров
		curl_setopt($ch, CURLOPT_URL, "http://aptechkin.by/scripts/photos-updater.php");
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// загрузка страницы и выдача её браузеру
		curl_exec($ch);

		// завершение сеанса и освобождение ресурсов
		curl_close($ch);

		$ch = curl_init();

		// установка URL и других необходимых параметров
		curl_setopt($ch, CURLOPT_URL, "aptechkin.by/scripts/annotations-updater.php");
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// загрузка страницы и выдача её браузеру
		curl_exec($ch);

		// завершение сеанса и освобождение ресурсов
		curl_close($ch);

        if ($true = TRUE){
			$data['result_import'] = 'Импорт успешно завершен';
		}else{
			$data['result_import'] = 'При импорте произошла ошибка';
		}

		$this->response->setOutput($this->load->view('tool/import.tpl', $data));
	}
}

