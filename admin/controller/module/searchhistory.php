<?php
class ControllerModuleSearchhistory extends Controller {

	public function index() {
		$this->load->language('module/searchhistory');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['export'] = $this->language->get('export');
		$data['token'] = $this->session->data['token'];
		$data['text_loading'] = 'Загрузка';
		$data['download'] = $this->url->link('module/searchhistory/download', 'token=' . $this->session->data['token'], 'SSL');
		
		
		if (isset($this->request->get['limit'])) {
			$data['limit'] = $this->request->get['limit'];
		} elseif (!empty($module_info)) {
			$data['limit'] = $module_info['limit'];
		} else {
			$data['limit'] = 25;
		}
		//var_dump($data['limit']);

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$search_history = $this->sql();
		$data['search_history'] = array_slice($search_history, $data['limit']*($page-1), $data['limit']);
		
		$pagination = new Pagination();
		$pagination->total = count($search_history);
		$pagination->page = $page;
		$pagination->limit = $data['limit'];
		$pagination->url = $this->url->link('module/searchhistory', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/searchhistory.tpl', $data));
	}

	public function download(){
		$cwd = getcwd();
			chdir( DIR_SYSTEM.'library/PHPExcel' );
	        require_once( 'Classes/PHPExcel.php' );
	        require_once('Classes/PHPExcel/Writer/Excel5.php');
	        chdir( $cwd );

			// Создаем объект класса PHPExcel
			$xls = new PHPExcel();
			// Устанавливаем индекс активного листа
			$xls->setActiveSheetIndex(0);
			// Получаем активный лист
			$sheet = $xls->getActiveSheet();
			// Подписываем лист
			$sheet->setTitle('История поиска');

			$sheet->getColumnDimension('A')->setWidth(3);
			$sheet->getColumnDimension('B')->setWidth(30);
			$sheet->getColumnDimension('C')->setWidth(20);
			$sheet->getColumnDimension('D')->setWidth(20);

			$sheet->mergeCells('A1:D1');
			$sheet->setCellValue('A1','ИСТОРИЯ ПОИСКА');
			$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCells('A2:D2');
			$sheet->setCellValue('A2','Дата создания: '.gmdate("d M Y H:i:s"));
			$sheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$search_history = array_reverse($this->sql());
			$i = 3;
			foreach ($search_history as $search_item) {
			    $sheet->setCellValue('A'.($i), $search_item['search_history_id']);
			    $sheet->setCellValue('B'.($i), $search_item['subject']);
			    $sheet->setCellValue('C'.($i), $search_item['date']);
			    $sheet->setCellValue('D'.($i), $search_item['time']);
			    $i++;
			}
			

			header ( "Expires: " . gmdate("D,d M YH:i:s") . " GMT" );
			header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
			header ( "Cache-Control: no-cache, must-revalidate" );
			header ( "Pragma: no-cache" );
			header ( "Content-type: application/vnd.ms-excel" );
			header ( "Content-Disposition: attachment; filename=search_history.xls" );
			$objWriter = new PHPExcel_Writer_Excel5($xls);
	 		$objWriter->save('php://output');
	}

	public function sql(){
		$sql  = "SELECT * FROM  `".DB_PREFIX."search_history`";
		$result = $this->db->query( $sql );
		$search_history = array();
		foreach (array_reverse($result->rows) as $row) {
			$search_history[] = (array('search_history_id' => $row['search_history_id'], 'subject' => $row['subject'],'date' => str_replace(" ", ".", $row['date']), 'time' => str_replace(" ", ":", $row['time'])));
		}
		return $search_history;
	}

}
