<?php
class ControllerModuleExportdbf extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/Exportdbf');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/exportdbf.tpl', $data));
	}

	public function install() {
		$this->load->model('extension/event');

		$this->model_extension_event->addEvent('exportdbf', 'pre.order.history.add', 'module/exportdbf/addfile');
	}

	public function uninstall() {
		$this->load->model('extension/event');

		$this->model_extension_event->deleteEvent('exportdbf');
	}
}