<?php

Class ControllerModuleExportorder extends Controller{

	function index(){
		

		// $this->load->language('module/export_order');
		$this->load->model('setting/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_module'] = $this->language->get('text_module');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['saveTemplate'] = $this->language->get('saveTemplate');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['date_zone'] = $this->language->get('date_zone');
		$data['statuss'] = $this->language->get('statuss');
		$data['from'] = $this->language->get('from');
		$data['to'] = $this->language->get('to');
		$data['all_orders'] = $this->language->get('all_orders');
		$data['colums'] = $this->language->get('colums');
		$data['export'] = $this->language->get('export');
		$data['action'] = $this->url->link('module/exportorder/generate', 'token=' . $this->session->data['token'], 'SSL');
		$data['action_product'] = $this->url->link('module/exportorder/generate_product', 'token=' . $this->session->data['token'], 'SSL');
		$data['saveTemplateURL'] = $this->url->link('module/exportorder/saveTemplate', 'token=' . $this->session->data['token'], 'SSL');
		$data['tempList'] = $this->language->get('tempList');
		$data['list_elements'] = $this->getList();
        $data['list_elements_product'] = $this->getList_product();
		$data['token'] = $this->session->data['token'];
		$data['templateList'] = $this->model_setting_setting->getSetting('export_order');
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/account', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);


		$this->load->model('tool/export_order');
		$data['order_status'] = $this->model_tool_export_order->getStatusList();


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('module/export_order.tpl', $data));

	}
	
	public function her(){
		echo 'her';
	}

	private function getList(){
		return  array(
			"order_id",
			"customer_id",
			"email",
			"telephone",
			"payment_address_1",
			"total",
			"partner",
			"order_status",
			"currency_code",
			"ip",
			"date_added",
			"count_products");
	}

	private function getList_product(){
		return  array(
			"count_products",
			"product_id",
			"name",
			"price",
			"manufacturer");
	}

	public function generate(){		
		$this->load->model('tool/export_order');
		$data = $this->model_tool_export_order->generate($this->request->post,$this->getList());
		$arr_col = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');

		$count = count($data['0']);
		$max = $count-1;
		include 'exel/PHPExcel.php';
		/** PHPExcel_Writer_Excel2007 */
		include ('exel/PHPExcel/Writer/Excel2007.php');
		include ('exel/PHPExcel/IOFactory.php');
		$pExcel = new PHPExcel();
		$pExcel->setActiveSheetIndex(0);
		$aSheet = $pExcel->getActiveSheet();
		$pExcel->createSheet();
		// Ориентация страницы и  размер листа
		$aSheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$aSheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		// Поля документа
		$aSheet->getPageMargins()->setTop(1);
		$aSheet->getPageMargins()->setRight(0.75);
		$aSheet->getPageMargins()->setLeft(0.75);
		$aSheet->getPageMargins()->setBottom(1);
		// Название листа
		$aSheet->setTitle('Заказы');
		// Шапка и футер (при печати)
		$aSheet->getHeaderFooter()->setOddHeader('& Экспорт заказов');
		// Настройки шрифта
		$pExcel->getDefaultStyle()->getFont()->setName('Arial');
		$pExcel->getDefaultStyle()->getFont()->setSize(11);
		$aSheet->mergeCells($arr_col[0].'1:'.$arr_col[$max].'1');
		$aSheet->getRowDimension('1')->setRowHeight(20);
		$aSheet->setCellValue('A1','Экспорт заказов');

		$row = 2;
		$i = 0;

		foreach ($data as $key => $val) {
			$aSheet->getRowDimension($row)->setRowHeight(20);
			$i = 0;
			foreach ($val as $v) {
				$aSheet->setCellValue($arr_col[$i].$row,$v);
				if($row == 2){
					$ia = 0;
					foreach ($arr_col as $n) {
						if($ia <= $max){
							$aSheet->getColumnDimension($n)->setAutoSize(true);
						}
						$ia++;
					}
				}
				$i++;
			}
			$row++;
		}

		// массив стилей
		$style_wrap = array(
    	// рамки
   			'borders'=>array(
	        // внешняя рамка
		        'outline' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_THICK,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        ),
		        // внутренняя
		        'allborders'=>array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'CCCCCC'
		            )
		        )
    		)
		);

		$aSheet->getStyle($arr_col[0].'1:'.$arr_col[$max].(2))->applyFromArray($style_wrap);

		$style_header = array(
		    // Шрифт
		    'font'=>array(
		        'bold' => true,
		        'name' => 'Times New Roman',
		        'size' => 15,
		        'color'=>array(
		            'rgb' => '006464'
		        )
		    ),
		    // Выравнивание
		    'alignment' => array(
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    ),
		    // Заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => '99CCCC'
		        )
		    ),
		    'borders'=>array(
		        'bottom'=>array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        )
		    )
		);
		$aSheet->getStyle($arr_col[0].'1:'.$arr_col[$max].'1')->applyFromArray($style_header);

		$style_item= array(
		    // Шрифт
		    'font'=>array(
		        'bold' => false,
		        'name' => 'Times New Roman',
		        'size' => 12,
		        'color'=>array(
		            'rgb' => '000000'
		        )
		    ),
		    // Выравнивание
		    'alignment' => array(
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    )
		);
		$aSheet->getStyle($arr_col[0].'3:'.$arr_col[$max].$row)->applyFromArray($style_item);


		// Стили для слогана компании (вторая строка)
		$style_slogan = array(
		    // шрифт
		    'font'=>array(
		        'bold' => true,
		        'italic' => true,
		        'name' => 'Times New Roman',
		        'size' => 12,
		        'color'=>array(
		            'rgb' => '006464'
		        )
		    ),
		    // выравнивание
		    'alignment' => array(
		        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    ),
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => 'FAFAFA'
		        )
		    ),
		    //рамки
		    'borders' => array(
		        'bottom' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        )
		    )
		);
		$aSheet->getStyle('A2:'.$arr_col[$max].'2')->applyFromArray($style_slogan);

		// Стили для текта возле даты
		$style_tdate = array(
		    // выравнивание
		    'alignment' => array(
		        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
		    ),
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => '888888'
		        )
		    ),
		    // рамки
		    'borders' => array(
		        'right' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_NONE
		        )
		    )
		);

		// Стили для даты
		$style_date = array(
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => 'EEEEEE'
		        )
		    ),
		    // рамки
		    'borders' => array(
		        'left' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_NONE
		        )
		    ),
		);
		$aSheet->getStyle('E4')->applyFromArray($style_date);


		header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition:attachment;filename="simple.xlsx"');
		$objWriter = new PHPExcel_Writer_Excel2007($pExcel);
		$objWriter->save('php://output');

	}

	public function generate_product(){
				
		$this->load->model('tool/export_order');
		$data = $this->model_tool_export_order->generate_product($this->request->post, $this->getList_product());
		$arr_col = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
		$count = count($data['0']);
		$max = $count-1;
		include 'exel/PHPExcel.php';
		/** PHPExcel_Writer_Excel2007 */
		include ('exel/PHPExcel/Writer/Excel2007.php');
		include ('exel/PHPExcel/IOFactory.php');
		$pExcel = new PHPExcel();
		$pExcel->setActiveSheetIndex(0);
		$aSheet = $pExcel->getActiveSheet();
		$pExcel->createSheet();
		// Ориентация страницы и  размер листа
		$aSheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$aSheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		// Поля документа
		$aSheet->getPageMargins()->setTop(1);
		$aSheet->getPageMargins()->setRight(0.75);
		$aSheet->getPageMargins()->setLeft(0.75);
		$aSheet->getPageMargins()->setBottom(1);
		// Название листа
		$aSheet->setTitle('Товары');
		// Шапка и футер (при печати)
		$aSheet->getHeaderFooter()->setOddHeader('& Экспорт заказанных товаров');
		// Настройки шрифта
		$pExcel->getDefaultStyle()->getFont()->setName('Arial');
		$pExcel->getDefaultStyle()->getFont()->setSize(11);
		$aSheet->mergeCells($arr_col[0].'1:'.$arr_col[$max].'1');
		$aSheet->getRowDimension('1')->setRowHeight(20);
		$aSheet->setCellValue('A1','Экспорт заказанных товаров');

		$row = 2;
		$i = 0;

		foreach ($data as $key => $val) {
			$aSheet->getRowDimension($row)->setRowHeight(20);
			$i = 0;
			foreach ($val as $v) {
				$aSheet->setCellValue($arr_col[$i].$row,$v);
				if($row == 2){
					$ia = 0;
					foreach ($arr_col as $n) {
						if($ia <= $max){
							$aSheet->getColumnDimension($n)->setAutoSize(true);
						}
						$ia++;
					}
				}
				$i++;
			}
			$row++;
		}

		// массив стилей
		$style_wrap = array(
    	// рамки
   			'borders'=>array(
	        // внешняя рамка
		        'outline' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_THICK,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        ),
		        // внутренняя
		        'allborders'=>array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'CCCCCC'
		            )
		        )
    		)
		);

		$aSheet->getStyle($arr_col[0].'1:'.$arr_col[$max].(2))->applyFromArray($style_wrap);

		$style_header = array(
		    // Шрифт
		    'font'=>array(
		        'bold' => true,
		        'name' => 'Times New Roman',
		        'size' => 15,
		        'color'=>array(
		            'rgb' => '006464'
		        )
		    ),
		    // Выравнивание
		    'alignment' => array(
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    ),
		    // Заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => '99CCCC'
		        )
		    ),
		    'borders'=>array(
		        'bottom'=>array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        )
		    )
		);
		$aSheet->getStyle($arr_col[0].'1:'.$arr_col[$max].'1')->applyFromArray($style_header);

		$style_item= array(
		    // Шрифт
		    'font'=>array(
		        'bold' => false,
		        'name' => 'Times New Roman',
		        'size' => 12,
		        'color'=>array(
		            'rgb' => '000000'
		        )
		    ),
		    // Выравнивание
		    'alignment' => array(
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    )
		);
		$aSheet->getStyle($arr_col[0].'3:'.$arr_col[$max].$row)->applyFromArray($style_item);


		// Стили для слогана компании (вторая строка)
		$style_slogan = array(
		    // шрифт
		    'font'=>array(
		        'bold' => true,
		        'italic' => true,
		        'name' => 'Times New Roman',
		        'size' => 12,
		        'color'=>array(
		            'rgb' => '006464'
		        )
		    ),
		    // выравнивание
		    'alignment' => array(
		        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
		        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
		    ),
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => 'FAFAFA'
		        )
		    ),
		    //рамки
		    'borders' => array(
		        'bottom' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array(
		                'rgb'=>'006464'
		            )
		        )
		    )
		);
		$aSheet->getStyle('A2:'.$arr_col[$max].'2')->applyFromArray($style_slogan);

		// Стили для текта возле даты
		$style_tdate = array(
		    // выравнивание
		    'alignment' => array(
		        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
		    ),
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => '888888'
		        )
		    ),
		    // рамки
		    'borders' => array(
		        'right' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_NONE
		        )
		    )
		);

		// Стили для даты
		$style_date = array(
		    // заполнение цветом
		    'fill' => array(
		        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
		        'color'=>array(
		            'rgb' => 'EEEEEE'
		        )
		    ),
		    // рамки
		    'borders' => array(
		        'left' => array(
		            'style'=>PHPExcel_Style_Border::BORDER_NONE
		        )
		    ),
		);
		$aSheet->getStyle('E4')->applyFromArray($style_date);


		header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition:attachment;filename="simple.xlsx"');
		$objWriter = new PHPExcel_Writer_Excel2007($pExcel);
		$objWriter->save('php://output');

	}

	public function saveTemplate(){

		$this->load->model('setting/setting');
		$temp = $this->model_setting_setting->getSetting('export_order', 0);

		if(!empty($this->request->get['tempname'])){
			$temp[$this->request->get['tempname']] = $this->request->post;
		} else {
			$name = 'New_Template_'.count($temp)+1;
			$temp[$name] = $this->request->post;
		}
		$this->model_setting_setting->editSetting('export_order', $temp, 0);
		$this->response->redirect($this->url->link('module/export_order', 'token=' . $this->session->data['token'], 'SSL'));
	}

}