<?php
// English   Multilingual SEO      Author: Slait
// Heading
$_['heading_title']		  							= 'SEO редактор';
$_['module_title']		  							= 'SEO редактор';
  
// Tab seo editor
$_['tab_seo_editor']      							= 'SEO редактор';
$_['tab_seo_editor_product']						= 'Товары';
$_['tab_seo_editor_category']						= 'Категории';
$_['tab_seo_editor_information']					= 'Статьи';
$_['tab_seo_editor_manufacturer']					= 'Производители';
$_['tab_seo_editor_common']							= 'Стандартные страницы';
$_['tab_seo_editor_special']						= 'Специальные страницы';
$_['text_editor_query']								= 'Запрос';
$_['text_editor_query_common']						= 'Query (value after route=)';
$_['text_editor_query_special']						= 'Query (ex: custom_id=1)';
$_['text_editor_image']								= 'Изображение';
$_['text_editor_name']								= 'Название';
$_['text_editor_title']								= 'Заголовок';
$_['text_editor_meta_title']						= 'Meta title';
$_['text_editor_meta_keyword']						= 'Meta keyword';
$_['text_editor_meta_description']					= 'Meta description';
$_['text_editor_url']								= 'SEO url';
$_['text_editor_tag']								= 'Теги';
$_['text_editor_h1']								= 'Seo H1';
$_['text_editor_related']							= 'Связанные продукты';
$_['text_seo_new_alias_title']						= 'Вставить новый url алиас';
$_['text_seo_new_alias_info']						= 'Переписать URL, что использовать параметр, например index.php?route=<b>account/account</b><br/>Вам нужно выделить <b>account/account</b> (все что после route=)<br/>В поле SEO url укажите сокращенную ссылку: <b>my-account</b>';
$_['text_seo_new_spec_alias_info']					= 'Если вы хотите переписать url из других модулей, укажите их здесь.<br/>Для примера index.php?<b>blog_news_id=123</b><br/>В поле запрос добавьте: <b>blog_news_id=123</b><br/>В поле SEO url добавьте: <b>a-great-url-for-my-great-news</b>';

// Tab seo configuration
$_['tab_seo_options']      							= 'Настройки SEO';
$_['text_seo_tab_general_1']						= 'Основные настройки';
$_['text_seo_tab_general_2']						= 'Префикс языка';
$_['text_seo_tab_general_3']						= 'Hreflang';
$_['text_info_general']								= 'Эти настройки влияют на осноыные значения SEO, они вступают в силу немедленно. Вы можете изменить их в любое время.';
$_['text_info_general_2']							= 'Языковый префикс позволяет добавлять язык в url. Пример: website.com/en/<br/>Это удобно если ваш сайт имеет несколько языков.';
$_['text_info_general_3']							= 'Тег Hreflang помогает поисковым системам определить язык страницы..<br/>После активации тег будет включен на всех страницах, а так же в карту сайта (Каналы продвижения > seo package sitemap).<br/> Подробнее: <a href="https://support.google.com/webmasters/answer/189077?hl=en" target="new">Здесь</a>';
$_['text_seo_absolute']								= 'Абсолютный путь:<span class="help">Позволяют использовать то же ключевое слово для подкатегорий (Пример: /men/shoes /women/shoes)</span>';
$_['text_seo_extension']							= 'Расширения:<span class="help">Добавить расширение в конце (Пример: .html)</span>';
$_['text_seo_flag']									= 'Префикс языка:<span class="help">Добавить префикс языка на сайт (/en, /ru, ...)</span>';
$_['text_seo_flag_upper']							= 'Префикс в верхнем регистре:<span class="help">/EN /RU</span>';
$_['text_seo_flag_default']							= 'Без префикса по умолчанию:<span class="help">Основной язык не будет иметь префикса</span>';
$_['text_seo_urlcache']								= 'URL кеш:<span class="help">Увеличиваем скорость загрузки страниц с помощью кэширования URL</span>';
$_['text_seo_banner']								= 'Переписать ссылки баннера:<span class="help">Динамическое создание SEO ссылки на баннеры (используется в баннер, карусель, слайд-шоу модуле)</span>';
$_['text_seo_banner_help']							= 'В баннеров разделе, не вводите в SEO ссылку (/category/product_name), введитессылку по умолчанию OpenCart: <b>index.php?route=product/product&path=10_21&product_id=54</b>.<br />Вы также можете удалить index.php, пример: <b>product/product&path=23&product_id=48</b>';
$_['text_seo_hreflang']								= 'Включить hreflang тег:';

// Tab store seo
$_['tab_seo_store']      							= 'SEO магазинов';
$_['text_info_store']								= 'В этом разделе вы можете настроить Заголовки, H1, meta keywords и description для главой страницы магазина и каждого языка! <br/> Все, что введенный здесь будут обходить значения, введенные в настройках OpenCart.';
$_['entry_store_seo_title']      					= 'Meta Title:';
$_['entry_store_title']      						= 'Свой H1:';
$_['entry_store_desc']      						= 'Meta Description:';
$_['entry_store_keywords']							= 'Meta Keywords:';

// Tab keyword options
$_['tab_seo_transform']								= 'Ключевые слова';
$_['text_info_transform']							= 'Все эти параметры для ключевых слов или для массового обновления.';
$_['text_seo_whitespace']							= 'Пробелы:<span class="help">Заменить пробел на символ...</span>';
$_['text_seo_lowercase']							= 'Нижний регистр:<span class="help">QWERTY => qwerty</span>';
$_['text_seo_duplicate']							= 'Дублирование:<span class="help">Разрешить дублировать ключевое слово для нескольких продуктов</span>';
$_['text_seo_ascii']								= 'ASCII режим:<span class="help">Заменить символы по их ascii эквиваленту<br/>"éàôï" => "eaoi"</span>';
$_['text_seo_autofill']								= 'Полностью автоматически';
$_['text_seo_autofill_on']							= 'Включить:';
$_['text_seo_autofill_desc']						= 'Автозаполнение:<span class="help">Если поле пустое, система автоматически заполнит его<br/><br/>Это работает для: <br/>- товаров<br/>- категорий<br/>- статей</span>';
$_['text_seo_autourl']								= 'Авто URL:<span class="help">If left blank on insert or edit, seo url keyword will be generated automatically using the parameter set in "Mass update" tab<br/>This works for products, categories and informations</span>';
$_['text_seo_autotitle']							= 'Auto title and desc for other langs:<span class="help">If left blank on insert or edit, titles and descriptions of other languages will copy the default language title and description<br/>This works for products, categories and informations</span>';
$_['text_insert']									= 'Добавить';
$_['text_edit']										= 'Изменить';

// Tab friendly urls
$_['tab_seo_friendly']								= 'Дружественные URL';
$_['text_seo_export_urls']							= 'Экспорт URL';
$_['text_seo_export_urls_tooltip'] 					= 'Экспорт Дружественные URL и отправить их разработчику для интеграции в официальном пакете';
$_['text_seo_reset_urls']  							= 'ВОсстановить URL по умолчанию';
$_['text_seo_reset_urls_tooltip']					= 'If the current language does not have predefined urls the module will load english version';
$_['text_info_friendly']							= 'Here you can manage the friendly urls, edit them as you want.<br/>You have also the possibility to add new url, it works for example for any custom module you installed, just fill the 1st field with the value in route (?route=mymodule/action) and the 2nd field with the keyword you want to appear in the url.';
$_['text_seo_friendly']								= 'Дружественные URL:<span class="help">Включите эту опцию, чтобы использовать дружественные URL-адреса для стандартных и специальных страниц (изменить их можно в закладке SEO редактор)</span>';
$_['text_seo_remove_urls'] 							= 'Удалить все URL';
$_['text_seo_add_url']      						= 'Добавить новое поле';

// Tab full product path
$_['tab_seo_fpp']									= 'Менеджер путей';
// Text
$_['text_fpp_mode']   								= 'Режим путей товаров:';
$_['text_fpp_mode_0']   							= 'Прямая ссылка';
$_['text_fpp_mode_1']   							= 'Кратчайший путь';
$_['text_fpp_mode_2']   							= 'Полный путь';
$_['text_fpp_mode_3']   							= 'Путь производителя';

$_['text_fpp_bc_mode'] 								= 'Хлебные крошки:';
$_['text_fpp_breadcrumbs_fix'] 						= 'Генерировать хлебные крошки:';
$_['text_fpp_breadcrumbs_0']   						= 'По умолчанию';
$_['text_fpp_breadcrumbs_1']   						= 'Генерировать если пустые';
$_['text_fpp_breadcrumbs_2']   						= 'Генерировать всегда';

$_['text_fpp_mode_help']   							= '<span class="help"><b>Прямая ссылка:</b> get direct link to product, no category included (ex: /product_name)<br/>
																		  <b>Кратчайший путь:</b> get shortest path by default, can be altered by banned categories (ex: /category/product_name)<br/>
																		  <b>Полный путь:</b> get largest path by default, can be altered by banned categories (ex: /category/sub-category/product_name)<br/>
																		  <b>Путь производителя:</b> get manufacturer path instead of categories (ex: /manufacturer/product_name)</span>';
$_['text_fpp_breadcrumbs_help']   					= '<span class="help"><b>По умолчанию:</b> default opencart behaviour: will display breadcrumbs coming from categories<br/>
																		  <b>Генерировать если пустые:</b> generate breadcrumbs only when it is not already available, so category breadcrumb is preserved (recommended)<br/>
																		  <b>Генерировать всегда:</b> overwrite also the category breadcrumbs, so the only breadcrumbs you will get is the one generated by the module<br/></span>';
$_['text_fpp_bypasscat'] 							= 'Переписать путь товара в категории:';
$_['text_fpp_bypasscat_help'] 						= '<span class="help">If disabled, the product link from categories remains the same in order to preserve normal behavior and breadcrumbs.<br/>If enabled, the product link from categories is overwritten with path generated by the module.<br>In any case canonical link is updated with good value so google will only see the url generated by the module for a given product.</span>';
$_['text_fpp_directcat'] 							= 'Direct link for categories:';
$_['text_fpp_directcat_help'] 						= '<span class="help">Enable to have direct url of a category without the parent, breadcrumbs will be preserved</span>';
$_['text_fpp_homelink'] 							= 'Rewrite home link:';
$_['text_fpp_homelink_help'] 						= '<span class="help">Set homepage link to mystore.com instead of mystore.com/index.php?route=common/home</span>';
$_['text_fpp_depth']   								= 'Max levels:';
$_['text_fpp_depth_help']   						= '<span class="help">Maximum category depth you want to display, for example if you have a product in /cat/subcat/subcat/product and set this option to 2 the link will become /cat/subcat/product<br/>This option works in largest and shortest path modes</span>';
$_['text_fpp_unlimited']   							= 'Неограниченный';
$_['text_fpp_remove_search']   						= 'Удалить параметры поиска:';
$_['text_fpp_remove_search_help']   				= '<span class="help">Remove the search parameter (?search=something) from product url in search results</span>';
$_['entry_category']  	 							= 'Исключить категории:<span class="help">Choose the categories that will never be displayed in case of multiple paths</span>';

// Tab mass update
$_['tab_seo_update']       							= 'Массовое обновление';
$_['text_info_update']     							= 'Будьте осторожны при использовании этой функции, так как это приведет к перезаписи всех ваших ключевых слов.<br/>Вы можете использовать функцию, чтобы проверить результат перед обновлением.<br/>Отметьте те языки которые нужно обновить.';
$_['text_cleanup']									= 'Очистить:<span class="help">Удалите старые URL-адреса из базы данных, если у вас возникли проблемы с некоторыми URL-адресами</span>';
$_['text_cache']									= 'URL кеш:<span class="help">Создать или удалить кэш URL</span>';
$_['text_cache_create_btn']							= 'Создать кеш';
$_['text_cache_delete_btn']							= 'Очистить кеш';
$_['text_cleanup_btn']								= 'Очистить';
$_['text_cleanup_done']								= 'Clean up done, %d entries deleted';
$_['text_seo_languages']   							= 'Выбрать языке';
$_['text_seo_simulate']    							= 'Включить:<span class="help">Никакие изменения не будут внесены, пока эта кнопка включена</span>';
$_['text_seo_empty_only']    						= 'Обновление только пустые значения:<span class="help">Отключите (OFF), чтобы переписать все значения</span>';
$_['text_enable']   	 		 					= 'Включен';
$_['text_deleted']   	 	 						= 'Выключен';

// Tab cron
$_['tab_seo_cron'] 									= 'Крон';
$_['text_info_cron']								= 'You can make mass update using cron jobs, copy the file <b>seo_package_cli.php</b> from "_extra files" folder (preferably into a directory outside of web root) and configure you cron with the path to that file.<br/>The script will use the settings configured on this page.<br/>A report is created on each update in /system/logs/';
$_['text_seo_cron_update'] 							= 'Обновить:';

// Tab about
$_['tab_seo_about']			 						= 'О модуле';

$_['text_seo_no_language']    						= 'Не выбран язык';
$_['text_seo_fullscreen']    						= 'На весь экран';
$_['text_seo_show_old']    							= 'Display old value';
$_['text_seo_change_count']    						= 'lines changed';
$_['text_seo_old_value']    						= 'Старое значение';
$_['text_seo_new_value']    						= 'Новое значение';
$_['text_seo_simulation_mode']    					= '<span>SIMULATION MODE</span><br/>Нет изменений';
$_['text_seo_write_mode']		    				= '<span>WRITE MODE</span><br/>Изменения были сохранены';
$_['text_seo_product']								= 'Товар';
$_['text_seo_category']								= 'Категория';
$_['text_seo_manufacturer']							= 'Производитель';
$_['text_seo_information']							= 'Статья';
$_['text_seo_cache']								= 'Название';
$_['text_seo_cleanup']								= 'Entry (url)';
$_['text_seo_type_product']							= 'Товары';
$_['text_seo_type_category']						= 'Категории';
$_['text_seo_type_manufacturer']					= 'Производители';
$_['text_seo_type_information']						= 'Статьи';
$_['text_seo_type_cache']							= 'Кеш';
$_['text_seo_type_cleanup']							= 'Clean up';
$_['text_seo_generator_product']					= 'Товары:';
$_['text_seo_generator_product_desc']				= '<span class="help">Приметы:<br/><b>[name]</b>: Название товара<br/><b>[model]</b>: Модель<br/><b>[category]</b>: Категория<br/><b>[brand]</b> : Бренд<br/><b>[desc]</b>: Описание<br/><br/>Прочее: <b>[upc]</b> <b>[sku]</b> <b>[ean]</b> <b>[jan]</b> <b>[isbn]</b> <b>[mpn]</b> <b>[location]</b> <b>[lang]</b> <b>[lang_id]</b> <b>[prod_id]</b> <b>[cat_id]</b></span>';
$_['text_seo_generator_category']					= 'Категории:';
$_['text_seo_generator_category_desc']				= '<span class="help">Примеры:<br/><b>[name]</b>: Название категории<br/><b>[desc]</b>: Описание<br/><br/>Прочее: <b>[lang]</b> <b>[lang_id]</b> <b>[cat_id]</b></span>';
$_['text_seo_generator_information']				= 'Статьи:';
$_['text_seo_generator_information_desc']			= '<span class="help">Примеры:<br/><b>[name]</b>: Заголовок<br/><b>[desc]</b>: Описание<br/><br/>Прочее: <b>[lang]</b> <b>[lang_id]</b></span>';
$_['text_seo_generator_manufacturer']				= 'Производитель:';
$_['text_seo_generator_manufacturer_desc']			= '<span class="help">Примеры:<br/><b>[name]</b>: Название производителя';
$_['text_seo_mode_url']								= 'SEO URLs';
$_['text_seo_mode_title']							= 'Meta Title';
$_['text_seo_mode_keyword'] 						= 'Meta Keywords';
$_['text_seo_mode_description']						= 'Meta Description';
$_['text_seo_mode_related']							= 'Похожие товары';
$_['text_seo_mode_tag']								= 'Теги';
$_['text_seo_mode_create']							= 'Генерировать';
$_['text_seo_mode_delete']							= 'Удалить';
$_['text_seo_generator_url']						= 'Генерировать URLs';
$_['text_seo_generator_title']						= 'Генерировать Meta Title';
$_['text_seo_generator_keywords'] 					= 'Генерировать Meta Keywords';
$_['text_seo_generator_desc']						= 'Генерировать Meta Description';
$_['text_seo_generator_tag']						= 'Генерировать теги';
$_['text_seo_generator_h1']							= 'Генерировать SEO H1';
$_['text_seo_generator_related']					= 'Generate Related Products';
$_['text_seo_generator_related_no']					= 'Qty:';
$_['text_seo_generator_related_relevance']			= 'Приоритет (0-10):';

$_['text_seo_result']    							= 'Результат:';

$_['text_module']        							= 'Модули';
$_['text_success']         							= 'Настройки модуля обновлены!';

$_['text_man_ext']				 					= 'Manufacturer extended';

$_['text_seo_confirm']		 						= 'Вы уверены?';


// Full product path



// Error
$_['error_permission'] 								= 'У Вас нет прав для управления этим модулем!';
?>