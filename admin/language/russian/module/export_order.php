<?

$_['heading_title'] 	= 'Экспорт заказов';
$_['text_module']		= 'Экспорт заказов в эксель';
$_['button_cancel'] 	= 'Назад';
$_['text_select_all']	= 'Выбрать все';
$_['text_unselect_all'] = 'Снять все';
$_['date_zone']			= 'Временая зона';
$_['all_orders']		= 'Все заказы';
$_['statuss']			= 'Статусы';
$_['from']				= 'От';
$_['to']				= 'до';
$_['saveTemplate']      = 'Сохранить как шаблон';
$_['tempList']          = 'Шаблоны';
$_['colums']				= 'Поля';
$_['export']				= 'Экспортировать';
