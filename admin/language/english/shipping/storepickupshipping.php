<?php
// Heading
$_['heading_title']    = 'StorePickup by iSenseLabs';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified StorePickup!';
$_['text_edit']        = 'Edit StorePickup Shipping';

// Entry
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify StorePickup!';