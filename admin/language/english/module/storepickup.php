<?php
// Language strings
$_['heading_title']               = 'StorePickup';
$_['error_permission']            = 'Warning: You do not have permission to modify module StorePickup!';
$_['text_success']                = 'Success: You have modified module StorePickup!';
$_['text_enabled']                = 'Enabled';
$_['text_disabled']               = 'Disabled';
$_['button_cancel']				  = 'Cancel';
$_['save_changes']				  = 'Save changes';
$_['text_default']				  = 'Default';
$_['text_module']				  = 'Module';
?>