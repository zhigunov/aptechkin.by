<?php

Class ModelToolExportorder extends Model{


	public function getStatusList(){
		$sql = $this->db->query('SELECT * FROM '.DB_PREFIX.'order_status WHERE language_id = "'.(int)$this->config->get('config_language_id').'" ');
		return $sql->rows;
	}

	public function generate_product($data,$list){

		$sql = "SELECT quantity as count_products, product_id, name, price FROM `" . DB_PREFIX . "order_product` op";

        $AND = 0;
        $OR  = 1;

		if(!isset($data['all_orders'])){

			if(isset($data['date_zone-from'])){
				$sql .= ' WHERE order_id IN (SELECT order_id FROM `oc_order` WHERE order_status_id <> 0 AND DATE_FORMAT(`date_added`, "%Y-%m-%d") >= "'.$data['date_zone-from'].'"';
			}

			if(isset($data['date_zone_to'])){
				$sql .= ' AND DATE_FORMAT(`date_added`, "%Y-%m-%d") <= "'.$data['date_zone_to'].'")';
			}else{
				$sql .= ")";
			}
            $AND = 1;
            $OR = 0;
		}else{
			$sql .= ' WHERE 1';
            $AND = 1;
            $OR = 0;
		}

        if(isset($data['status'])){
            $sql .= ' AND (SELECT order_status_id FROM `oc_order` o WHERE o.order_id = op.order_id ';
            foreach ($data['status'] as $key => $value) {
                if($value == 1){
                    if($AND != 0){
                        $sql .= ' AND (';
                    } elseif($OR == 0){
                        $sql .= ' OR ';
                    } else {
                        $sql .= '( ';
                    }
                    $sql .= 'order_status_id = ' . $key;
                    $AND = 0;
                    $OR = 0;
                }
            }
            $sql .= '))';
        }

		$result = $this->db->query($sql);
		$array = array();
		foreach ($list as $l) {
            if(isset($data['elemnt'][$l])){
                $array[0][$l] = $l;
            }
		}
		$array[0]['sum'] = 'sum';
		foreach ($result->rows as $key => $val) {
			$sql1 = "SELECT m.name FROM `" . DB_PREFIX . "manufacturer` m LEFT JOIN " . DB_PREFIX . "product p ON m.manufacturer_id = p.manufacturer_id WHERE p.product_id = " . $val['product_id'];
			$result1 = $this->db->query($sql1);
			if(isset($result1->row['name'])){
				$result->rows[$key]['manufacturer'] = $result1->row['name'];
			}else{
				$sql2 = "SELECT m.name FROM `" . DB_PREFIX . "manufacturer` m LEFT JOIN " . DB_PREFIX . "product p ON m.manufacturer_id = p.manufacturer_id WHERE p.model = '" . $val['name'] . "'";
				$result2 = $this->db->query($sql2);
				if(isset($result2->row['name'])){
					$result->rows[$key]['manufacturer'] = $result2->row['name'];
				}else{
					$result->rows[$key]['manufacturer'] = 'NULL';
				}
			}
		}
		foreach ($result->rows as $key => $value) {
            foreach ($list as $l) {
                if (isset($data['elemnt'][$l])) {
                    $array[$key + 1][$l] = $value[$l];
                }
            }
            $array[$key + 1]['sum'] = $value['count_products'] * $value['price'];
		}
		

		return $array;
	}
	public function generate($data,$list){

		$sql = 'SELECT * FROM `'.DB_PREFIX.'order` WHERE ';
		$AND = 0;
		$OR  = 1;
		if(!isset($data['all_orders'])){

			if(isset($data['date_zone-from'])){
				$sql .= 'DATE_FORMAT(`date_added`, "%Y-%m-%d") >= "'.$data['date_zone-from'].'" ';
			}

			if(isset($data['date_zone_to'])){
				$sql .= 'AND DATE_FORMAT(`date_added`, "%Y-%m-%d") <= "'.$data['date_zone_to'].'" ';
			}
			$AND = 1;
			$OR = 0;
		}

		if(isset($data['status'])){
			foreach ($data['status'] as $key => $value) {
				if($value == 1){
					if($AND != 0){
						$sql .= ' AND (';
					} elseif($OR == 0){
						$sql .= ' OR ';
					} else {
						$sql .= '( ';
					}
				$sql .= ' `order_status_id` = "'.$key.'" ';
				$AND = 0;
				$OR = 0;
				}
			}
		$sql .= ')';
		}

		$sql .= ' ORDER BY `order_id` ASC ';
		$result = $this->db->query($sql);
		$array = array();
		
		foreach ($list as $l) {
			if(isset($data['elemnt'][$l])){
				$array[0][$l] = $l;
			}
		}
		$i = 1;



			foreach ($result->rows as $val) {
				foreach ($list as $l) {
					if(isset($data['elemnt'][$l])){
						if($l == 'count_products'){
							$count = $this->db->query('SELECT SUM(quantity) as count_products FROM `'.DB_PREFIX.'order_product` WHERE order_id = "'.$val['order_id'].'"');
							$array[$i][$l] = $count->row['count_products'];
						} elseif($l == 'invoice'){
							$array[$i][$l] = $val['invoice_prefix'].$val['invoice_no'];
						} elseif($l == 'order_status'){
							$status = $this->db->query('SELECT * FROM `'.DB_PREFIX.'order_status` WHERE `order_status_id` = "'.$val['order_status_id'].'"');
							$array[$i][$l] = $status->row['name'];
						} else {
							$array[$i][$l] = $val[$l];
						}
					}
				}
				$i++;
			}
			return $array;
		

	}
}