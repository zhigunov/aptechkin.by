<?php

$production = !(getenv('DEV_CONFIG'));

if ($production) {
    $site_url = 'aptechkin.by';
} else {
    $site_url = 'preorder.loc';
}


// HTTP
define('HTTP_SERVER', 'http://' . $site_url . '/admin/');
define('HTTP_CATALOG', 'http://' . $site_url . '/');

// HTTPS
define('HTTPS_SERVER', 'https://' . $site_url . '/admin/');
define('HTTPS_CATALOG', 'https://' . $site_url . '/');

// DIR
define('DIR_APPLICATION', __DIR__ . '/');
define('DIR_SYSTEM', __DIR__ . '/../system/');
define('DIR_LANGUAGE', __DIR__ . '/language/');
define('DIR_TEMPLATE', __DIR__ . '/view/template/');
define('DIR_CONFIG', __DIR__ . '/../system/config/');
define('DIR_IMAGE', __DIR__ . '/../image/');
define('DIR_CACHE', __DIR__ . '/../system/storage/cache/');
define('DIR_DOWNLOAD', __DIR__ . '/../system/storage/download/');
define('DIR_LOGS', __DIR__ . '/../system/storage/logs/');
define('DIR_MODIFICATION', __DIR__ . '/../system/storage/modification/');
define('DIR_UPLOAD', __DIR__ . '/../system/storage/upload/');
define('DIR_CATALOG', __DIR__ . '/../catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'ct55373_aptechkn');
define('DB_PASSWORD', 'kNMS6tqm');
define('DB_DATABASE', 'ct55373_aptechkn');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
