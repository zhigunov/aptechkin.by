<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <h1>История поиска на сайте</h1>
    <table style="width:75%; border-collapse: collapse; border: 2px solid;">
    	<tr style="border: 2px solid;">
    		<td style="border: 2px solid;"><h3>Id</h3></td>
    		<td style="border: 2px solid;"><h3>Параметр поиска</h3></td>
    		<td style="border: 2px solid;"><h3>Дата</h3></td>
    		<td style="border: 2px solid;"><h3>Время</h3></td>
    	</tr>
    	<?php foreach($search_history as $search_item){ ?>
    		<tr style="border: 2px solid;">
    			<td style="border: 2px solid;"><h4><?php echo $search_item['search_history_id']; ?></h4></td>
    			<td style="border: 2px solid;"><h4><?php echo $search_item['subject']; ?></h4></td>
    			<td style="border: 2px solid;"><h4><?php echo $search_item['date']; ?></h4></td>
    			<td style="border: 2px solid;"><h4><?php echo $search_item['time']; ?></h4></td>
    		</tr>
    	<?php } ?>
    </table>
    <form action="<?php echo $download; ?>" method="post">
    <input type="submit" id="button-download" class="btn btn-primary btn-lg btn-block" style="width: 75%;" value="<?php echo $export; ?>"/>
    </form>
    <?php echo $pagination; ?>
</div>
<?php echo $footer; ?> 
