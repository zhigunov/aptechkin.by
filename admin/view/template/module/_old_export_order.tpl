<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      
    </div>
    <div class="content" style="width: 45%; display: inline-block; position: relative;">
    <p>Экспорт заказов</p>
    <div class="buttons">
        <!-- <a onclick="saveTemplate();" class="button"><?php echo $saveTemplate; ?></a> -->
        <button type="button" onclick="$('#form').submit();" class="btn btn-primary" data-original-title="<?php echo $export; ?>"><i class="fa fa-save"></i></button>
        <!-- <a onclick="$('#form').submit();" class="button"><?php echo $export; ?></a> -->
        <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <!-- <tr>
            <td><?php echo $tempList; ?></td>
            <td class="tempList">  
                <?php foreach ($templateList as $key => $value) {?>
                    <li style="display: inline-block; font-size: 16px; padding: 10px;"><a onclick="loadTemplate('<? echo $key; ?>')"><?php echo $key; ?></a></li>
                <? } ?>
            </td>
          </tr> -->
          <tr>
            <td><?php echo $date_zone; ?></td>
            <td>  
              <?php echo $from; ?>: <input type="text" name="date_zone-from" id="date_zone-from" value="" size="12" class="date" />    
               <?php echo $to; ?>: <input type="text" name="date_zone_to" id="date_zone_to" value="" size="12" class="date" /> <br>
              <input type="checkbox" name="all_orders" value="1"> <?php echo $all_orders;?>
            </td>
          </tr>
          <tr><td>&nbsp</td></tr>
          <tr>
            <td><?php echo $statuss;?></td>
            <td><div class="scrollbox" style="height: 160px; overflow: auto;">
                <?php $class = 'odd'; ?>
                <? foreach ($order_status as $status) { ?>
                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                <div class="<?php echo $class ?>">
                  <input type="checkbox" name="status[<?php echo $status['order_status_id']; ?>]" value="1" checked="checked" /><?php echo $status['name']; ?>
                </div>
                <? } ?>
              </div>
              <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
          </tr>
          <tr><td>&nbsp</td></tr>
          <tr>
            <td><?php echo $colums;?></td>
            <td><div class="scrollbox" style="height: 160px; overflow: auto;">
                <?php $class = 'odd'; ?>
                <? foreach ($list_elements as $element) { ?>
                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                <div class="<?php echo $class;?>">
                  <input type="checkbox" name="elemnt[<?php echo $element;?>]" value="1" checked="checked" /><?php echo $element; ?>
                </div>
                <? } ?>
              </div>
              <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
          </tr>
          
        </table>
      </form>
    </div>
    <div class="content" style="width: 45%; display: inline-block; position: relative;">
    <p>Экспорт заказанных товаров</p>
    <div class="buttons">
        <!-- <a onclick="saveTemplate();" class="button"><?php echo $saveTemplate; ?></a> -->
        <button type="button" onclick="$('#form_product').submit();" class="btn btn-primary" data-original-title="<?php echo $export; ?>"><i class="fa fa-save"></i></button>
        <!-- <a onclick="$('#form').submit();" class="button"><?php echo $export; ?></a> -->
        <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
     <form action="<?php echo $action_product; ?>" method="post" enctype="multipart/form-data" id="form_product">
        <table class="form">
          <tr>
            <td><?php echo $date_zone; ?></td>
            <td>  
              <?php echo $from; ?>: <input type="text" name="date_zone-from" id="date_zone-from" value="" size="12" class="date" />    
               <?php echo $to; ?>: <input type="text" name="date_zone_to" id="date_zone_to" value="" size="12" class="date" /> <br>
              <input type="checkbox" name="all_orders" value="1"> <?php echo $all_orders;?>
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
  $('.date').datetimepicker({format: 'YYYY-MM-DD'});
});


  function loadTemplate(temp){
    var list = '<?php echo json_encode($templateList); ?>';
    var template = JSON.parse(list);
    eval('var myTemp = template.'+temp+';'); 
    $('input').removeAttr("checked");
    $.each(myTemp,function(key,val){
      if(key == 'date_zone-from' || key == 'date_zone_to'){
         $('#'+key).datetimepicker('setDate', val);
      } else if(key == 'all_orders'){
         $('input[name='+key+']').attr('checked','checked');
      } else if(key == 'elemnt'){
        $.each(val,function(eKey, eVal){
          console.log(eKey);
          $('input[name="elemnt['+eKey+']"]').attr('checked','checked');
        });
      } else if(key == 'status'){
        $.each(val,function(eKey, eVal){
          console.log(eKey);
          $('input[name="status['+eKey+']"]').attr('checked','checked');
        });
      }
    });
  }

  function saveTemplate(){
    var tempname = prompt('Введите название шаблона', 'New Template');
    $('#form').attr('action','index.php?route=module/export_order/saveTemplate&tempname='+tempname+'&token=<?php echo $token; ?>');
    $('#form').submit();
  }
</script>
<?php echo $footer; ?>