<?php 
    $storedata_name = $moduleName.'[StoreData]['.$storedata['id'].']';
    $storedata_data = (isset($moduleData['StoreData'][$storedata['id']])) ? $moduleData['StoreData'][$storedata['id']] : array();
?>
<div id="storedata_<?php echo $storedata['id']; ?>" class="tab-pane storeDataInfo" style="width:99%;overflow:hidden;">
	<div class="row">
	  <div class="col-md-3">
        <h5><strong><span class="required">* </span>Store <?php echo $storedata['id']; ?> status:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enable or disable the selected store.</span>
      </div>
      <div class="col-md-3">
        <select id="Checker" name="<?php echo $storedata_name; ?>[Enabled]" class="form-control">
              <option value="yes" <?php echo (!empty($storedata_data['Enabled']) && $storedata_data['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
              <option value="no"  <?php echo (empty($storedata_data['Enabled']) || $storedata_data['Enabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
        </select>
      </div>
    </div>
    <br />
	<ul class="nav nav-tabs storedata_tabs">
    	<h5><strong>Multi-lingual settings:</strong></h5>
		<?php $i=0; foreach ($languages as $language) { ?>
			<li <?php if ($i==0) echo 'class="active"'; ?>><a href="#tab-<?php echo $storedata['id']; ?>-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>"/> <?php echo $language['name']; ?></a></li>
		<?php $i++; }?>
	</ul>
    <div class="tab-content">
		<?php $i=0; foreach ($languages as $language) { ?>
            <div id="tab-<?php echo $storedata['id']; ?>-<?php echo $language['language_id']; ?>" language-id="<?php echo $language['language_id']; ?>" class="row-fluid tab-pane language <?php if ($i==0) echo 'active'; ?>">
                <div class="row">
                  <div class="col-md-3">
					         <h5><strong>Название аптеки:</strong></h5>
                  </div>
                  <div class="col-md-6">
					           <input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $storedata_name; ?>[Name][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($storedata_data['Name'][$language['language_id']])) echo $storedata_data['Name'][$language['language_id']]; else echo "Store Name 1"; ?>" />
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-md-3">
                   <h5><strong>Номер аптеки:</strong></h5>
                  </div>
                  <div class="col-md-6">
                     <input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $storedata_name; ?>[Number]" value="<?php if(!empty($storedata_data['Number'])) echo $storedata_data['Number']; else echo ""; ?>" />
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-md-3">
                    <h5><strong>Город(без "г.") для центрирования карты:</strong></h5>
                  </div>
                  <div class="col-md-6">
                    <input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $storedata_name; ?>[City][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($storedata_data['City'][$language['language_id']])) echo $storedata_data['City'][$language['language_id']]; else echo "Минск"; ?>" />
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <h5><strong>Идентификатор сети аптек:</strong></h5>
                  </div>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="<?php echo $storedata_name; ?>[Idseti][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($storedata_data['Idseti'][$language['language_id']])) echo $storedata_data['Idseti'][$language['language_id']]; ?>" />
                  </div>
                </div>

                <br />
                <div class="row">
                  <div class="col-md-3">
					<h5><strong>Адрес аптеки:</strong></h5>
                  </div>
                  <div class="col-md-6">
					<input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $storedata_name; ?>[Address][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($storedata_data['Address'][$language['language_id']])) echo $storedata_data['Address'][$language['language_id']]; else echo "Test City, Random boulevard, Example street 45"; ?>" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
          <h5><strong>Режим работы:</strong></h5>
                  </div>
                  <div class="col-md-6">
          <input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $storedata_name; ?>[Regime][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($storedata_data['Regime'][$language['language_id']])) echo $storedata_data['Regime'][$language['language_id']]; else echo "Режим работы уточняйте по телефону"; ?>" />
                  </div>
                </div>
                <hr />
			</div>
        <?php $i++; } ?>
	</div>
    <br />
	<div class="row">
      <div class="col-md-3">
        <h5><strong>Телефон:</strong></h5>
      </div>
      <div class="col-md-3">
		 <input type="text" class="form-control" name="<?php echo $storedata_name; ?>[Phone]" value="<?php if (isset($storedata_data['Phone'])) echo $storedata_data['Phone']; else echo 'xx-xxxx-xx'; ?>" />
      </div>
    </div>
    <hr />
    <h5><strong>Координаты:</strong></h5>
    <br />
    <div class="row">
      <div class="col-md-3">
        <h5><strong>Longtitude/Latidude:</strong></h5>
      </div>
      <div class="col-md-3">
		 <input type="text" class="GoogleMapsLongitude form-control" name="<?php echo $storedata_name; ?>[Longtitude]" value="<?php if (isset($storedata_data['Longtitude'])) echo $storedata_data['Longtitude']; else echo '42.6973336'; ?>" id="GoogleMapsLongitude_<?php echo $storedata['id']; ?>" />
      </div>
      <div class="col-md-3">
		 <input type="text" class="GoogleMapsLatitude form-control" name="<?php echo $storedata_name; ?>[Latitude]" value="<?php if (isset($storedata_data['Latitude'])) echo $storedata_data['Latitude']; else echo '23.323'; ?>" id="GoogleMapsLatitude_<?php echo $storedata['id']; ?>" />
      </div>
      <div class="col-md-3">
		  <button class="btn btn-info GoogleMapsPreviewButton" id="GoogleMapsPreviewButton">Предпросмотр</button>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-3">
        <h5><strong>Карта предпросмотр:</strong></h5>
      </div>
      <div class="col-md-3">
		<div class="GoogleMapsPreviewDiv" data-longitude-selector="#GoogleMapsLongitude_<?php echo $storedata['id']; ?>" data-latitude-selector="#GoogleMapsLatitude_<?php echo $storedata['id']; ?>" data-apikey-selector="#GoogleMapsAPIKey" id="GoogleMapsPreviewDiv_<?php echo $storedata['id']; ?>"></div>
      </div>
    </div>
    <br />
    <?php if (isset($newAddition) && $newAddition==true) { ?>
    	<script type="text/javascript">
			<?php foreach ($languages as $language) { ?>
				$('#message_<?php echo $storedata['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
						height: 200
				});
				$('#messageD_<?php echo $storedata['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
						height: 200
				});
			<?php } ?>
		</script>
    <?php } ?>
</div>