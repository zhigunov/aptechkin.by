<?php

$production = !(getenv('DEV_CONFIG'));

if ($production) {
    $site_url = 'aptechkin.by';
} else {
    $site_url = 'preorder.loc';
}
//$site_url = '46.101.135.93';
// HTTP
define('HTTP_SERVER', 'http://' . $site_url . '/');

// HTTPS
define('HTTPS_SERVER', 'https://' . $site_url . '/');

// DIR
define('DIR_APPLICATION', __DIR__ . '/catalog/');
define('DIR_SYSTEM', __DIR__ . '/system/');
define('DIR_LANGUAGE', __DIR__ . '/catalog/language/');
define('DIR_TEMPLATE', __DIR__ . '/catalog/view/theme/');
define('DIR_CONFIG', __DIR__ . '/system/config/');
define('DIR_IMAGE', __DIR__ . '/image/');
define('DIR_CACHE', __DIR__ . '/system/storage/cache/');
define('DIR_DOWNLOAD', __DIR__ . '/system/storage/download/');
define('DIR_LOGS', __DIR__ . '/system/storage/logs/');
define('DIR_MODIFICATION', __DIR__ . '/system/storage/modification/');
define('DIR_UPLOAD', __DIR__ . '/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'ct55373_aptechkn');
define('DB_PASSWORD', 'kNMS6tqm');
define('DB_DATABASE', 'ct55373_aptechkn');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
