<ul id="menu">
  <li id="dashboard"><a href="<?php echo $home; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li>
  <?php if ((array_search('catalog/attribute', $permission_access) != false)||(array_search('catalog/attribute_group', $permission_access) != false)||
            (array_search('catalog/category', $permission_access) != false)||(array_search('catalog/download', $permission_access) != false)||
            (array_search('catalog/filter', $permission_access) != false)||(array_search('catalog/information', $permission_access) != false)||
            (array_search('catalog/manufacturer', $permission_access) != false)||(array_search('catalog/option', $permission_access) != false)||
            (array_search('catalog/product', $permission_access) != false)||(array_search('catalog/recurring', $permission_access) != false)||
            (array_search('catalog/review', $permission_access) != false)){ ?>
  <li id="catalog"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
    <ul>
      <?php if (array_search('catalog/category', $permission_access) != false){ ?>
      <li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/product', $permission_access) != false){ ?>
      <li><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/recurring', $permission_access) != false){ ?>
      <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/filter', $permission_access) != false){ ?>
      <li><a href="<?php echo $filter; ?>"><?php echo $text_filter; ?></a></li>
      <?php } ?>
      <?php if ((array_search('catalog/attribute', $permission_access) != false)||(array_search('catalog/attribute_group', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_attribute; ?></a>
        <ul>
          <?php if (array_search('catalog/attribute', $permission_access) != false){ ?>
          <li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?></a></li>
          <?php } ?>
          <?php if (array_search('catalog/attribute_group', $permission_access) != false){ ?>
          <li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if (array_search('catalog/option', $permission_access) != false){ ?>
      <li><a href="<?php echo $option; ?>"><?php echo $text_option; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/manufacturer', $permission_access) != false){ ?>
      <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/download', $permission_access) != false){ ?>
      <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/review', $permission_access) != false){ ?>
      <li><a href="<?php echo $review; ?>"><?php echo $text_review; ?></a></li>
      <?php } ?>
      <?php if (array_search('catalog/information', $permission_access) != false){ ?>
      <li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('extension/installer', $permission_access) != false)||(array_search('extension/modification', $permission_access) != false)||
            (array_search('extension/analytics', $permission_access) != false)||(array_search('extension/captcha', $permission_access) != false)||
            (array_search('extension/feed', $permission_access) != false)||(array_search('module/complete_seo', $permission_access) != false)||
            (array_search('extension/fraud', $permission_access) != false)||(array_search('extension/module', $permission_access) != false)||
            (array_search('extension/payment', $permission_access) != false)||(array_search('extension/shipping', $permission_access) != false)||
            (array_search('extension/total', $permission_access) != false)){ ?>
  <li id="extension"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span><?php echo $text_extension; ?></span></a>
    <ul>
      <?php if (array_search('extension/installer', $permission_access) != false){ ?>
      <li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/modification', $permission_access) != false){ ?>
      <li><a href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/analytics', $permission_access) != false){ ?>
      <li><a href="<?php echo $analytics; ?>"><?php echo $text_analytics; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/captcha', $permission_access) != false){ ?>
      <li><a href="<?php echo $captcha; ?>"><?php echo $text_captcha; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/feed', $permission_access) != false){ ?>
      <li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>

      <li><a href="<?php echo $link_complete_seo; ?>">SEO редактор</a></li>
      
      <?php } ?>
      <?php if (array_search('module/complete_seo', $permission_access) != false){ ?>
      <li><a href="/admin/index.php?route=module/complete_seo&token=f2sIBHmB1baK4Jnm3GvVPALUmXiun4Hl">SEO редактор</a></li>
      <?php } ?>
      <?php if (array_search('extension/fraud', $permission_access) != false){ ?>
      <li><a href="<?php echo $fraud; ?>"><?php echo $text_fraud; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/module', $permission_access) != false){ ?>
      <li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/payment', $permission_access) != false){ ?>
      <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/shipping', $permission_access) != false){ ?>
      <li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li>
      <?php } ?>
      <?php if (array_search('extension/total', $permission_access) != false){ ?>
      <li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li>
      <?php } ?>

      <?php if ($openbay_show_menu == 1) { ?>
      <li><a class="parent"><?php echo $text_openbay_extension; ?></a>
        <ul>
          <li><a href="<?php echo $openbay_link_extension; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
          <li><a href="<?php echo $openbay_link_orders; ?>"><?php echo $text_openbay_orders; ?></a></li>
          <li><a href="<?php echo $openbay_link_items; ?>"><?php echo $text_openbay_items; ?></a></li>
          <?php if ($openbay_markets['ebay'] == 1) { ?>
          <li><a class="parent"><?php echo $text_openbay_ebay; ?></a>
            <ul>
              <li><a href="<?php echo $openbay_link_ebay; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
              <li><a href="<?php echo $openbay_link_ebay_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
              <li><a href="<?php echo $openbay_link_ebay_links; ?>"><?php echo $text_openbay_links; ?></a></li>
              <li><a href="<?php echo $openbay_link_ebay_orderimport; ?>"><?php echo $text_openbay_order_import; ?></a></li>
            </ul>
          </li>
          <?php } ?>
          <?php if ($openbay_markets['amazon'] == 1) { ?>
          <li><a class="parent"><?php echo $text_openbay_amazon; ?></a>
            <ul>
              <li><a href="<?php echo $openbay_link_amazon; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
              <li><a href="<?php echo $openbay_link_amazon_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
              <li><a href="<?php echo $openbay_link_amazon_links; ?>"><?php echo $text_openbay_links; ?></a></li>
            </ul>
          </li>
          <?php } ?>
          <?php if ($openbay_markets['amazonus'] == 1) { ?>
          <li><a class="parent"><?php echo $text_openbay_amazonus; ?></a>
            <ul>
              <li><a href="<?php echo $openbay_link_amazonus; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
              <li><a href="<?php echo $openbay_link_amazonus_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
              <li><a href="<?php echo $openbay_link_amazonus_links; ?>"><?php echo $text_openbay_links; ?></a></li>
            </ul>
          </li>
          <?php } ?>
          <?php if ($openbay_markets['etsy'] == 1) { ?>
          <li><a class="parent"><?php echo $text_openbay_etsy; ?></a>
            <ul>
              <li><a href="<?php echo $openbay_link_etsy; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
              <li><a href="<?php echo $openbay_link_etsy_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
              <li><a href="<?php echo $openbay_link_etsy_links; ?>"><?php echo $text_openbay_links; ?></a></li>
            </ul>
          </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('design/layout', $permission_access) != false)||(array_search('design/banner', $permission_access) != false)){ ?>
  <li id="design"><a class="parent"><i class="fa fa-television fa-fw"></i> <span><?php echo $text_design; ?></span></a>
    <ul>
      <?php if (array_search('design/layout', $permission_access) != false){ ?>
      <li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li>
      <?php } ?>
      <?php if (array_search('design/banner', $permission_access) != false){ ?>
      <li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('sale/order', $permission_access) != false)||(array_search('sale/order_recurring', $permission_access) != false)||
            (array_search('sale/return', $permission_access) != false)||(array_search('sale/voucher', $permission_access) != false)||
            (array_search('sale/voucher_theme', $permission_access) != false)||(array_search('payment/pp_express', $permission_access) != false)){ ?>
  
			 	  <li><a class="parent">
			 	  	
			 	  		<i class="fa fa-rocket fa-fw"></i> <span><?php echo 'Pavo Theme Control'; ?></span></a>
			 	  		<ul>
			 	  			<li><a href="<?php echo $pavo_link; ?>">Theme Control</a></li>
			 	  			<li><a href="<?php echo $megamenu_link; ?>">MegaMenu</a></li>
			 	  		</ul>	
			 	  </li>	
				  <li id="sale">
			  <a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
    <ul>
      <?php if (array_search('sale/order', $permission_access) != false){ ?>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <?php } ?>
      <?php if (array_search('sale/order_recurring', $permission_access) != false){ ?>
      <li><a href="<?php echo $order_recurring; ?>"><?php echo $text_order_recurring; ?></a></li>
      <?php } ?>
      <?php if (array_search('sale/return', $permission_access) != false){ ?>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <?php } ?>
      <?php if ((array_search('sale/voucher', $permission_access) != false)||(array_search('sale/voucher_theme', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_voucher; ?></a>
        <ul>
          <?php if (array_search('sale/voucher', $permission_access) != false){ ?>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <?php } ?>
          <?php if (array_search('sale/voucher_theme', $permission_access) != false){ ?>
          <li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if (array_search('payment/pp_express', $permission_access) != false){ ?>
      <li><a class="parent"><?php echo $text_paypal ?></a>
        <ul>
          <li><a href="<?php echo $paypal_search ?>"><?php echo $text_paypal_search ?></a></li>
        </ul>
      </li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('customer/customer', $permission_access) != false)||(array_search('customer/customer_group', $permission_access) != false)||
            (array_search('customer/custom_field', $permission_access) != false)){ ?>
  <li id="customer"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_customer; ?></span></a>
    <ul>
      <?php if (array_search('customer/customer', $permission_access) != false){ ?>
      <li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li>
      <?php } ?>
      <?php if (array_search('customer/customer_group', $permission_access) != false){ ?>
      <li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li>
      <?php } ?>
      <?php if (array_search('customer/custom_field', $permission_access) != false){ ?>
      <li><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('marketing/marketing', $permission_access) != false)||(array_search('marketing/affiliate', $permission_access) != false)||
            (array_search('marketing/coupon', $permission_access) != false)||(array_search('marketing/contact', $permission_access) != false)){ ?>
  <li><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
    <ul>
      <?php if (array_search('marketing/marketing', $permission_access) != false){ ?>
      <li><a href="<?php echo $marketing; ?>"><?php echo $text_marketing; ?></a></li>
      <?php } ?>
      <?php if (array_search('marketing/affiliate', $permission_access) != false){ ?>
      <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
      <?php } ?>
      <?php if (array_search('marketing/coupon', $permission_access) != false){ ?>
      <li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li>
      <?php } ?>
      <?php if (array_search('marketing/contact', $permission_access) != false){ ?>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('setting/setting', $permission_access) != false)||(array_search('setting/store', $permission_access) != false)||
            (array_search('user/user', $permission_access) != false)||(array_search('user/api', $permission_access) != false)||
            (array_search('user/user_permission', $permission_access) != false)||(array_search('localisation/country', $permission_access) != false)||
            (array_search('localisation/currency', $permission_access) != false)||(array_search('localisation/geo_zone', $permission_access) != false)||
            (array_search('localisation/language', $permission_access) != false)||(array_search('localisation/length_class', $permission_access) != false)||
            (array_search('localisation/location', $permission_access) != false)||(array_search('localisation/order_status', $permission_access) != false)||(array_search('localisation/return_action', $permission_access) != false)||(array_search('localisation/return_reason', $permission_access) != false)||(array_search('localisation/return_status', $permission_access) != false)||(array_search('localisation/stock_status', $permission_access) != false)||(array_search('localisation/tax_class', $permission_access) != false)||(array_search('localisation/tax_rate', $permission_access) != false)||(array_search('localisation/weight_class', $permission_access) != false)||(array_search('localisation/zone', $permission_access) != false)||(array_search('tool/backup', $permission_access) != false)||(array_search('tool/error_log', $permission_access) != false)||(array_search('tool/import', $permission_access) != false)||(array_search('tool/upload', $permission_access) != false)){ ?>
  <li id="system"><a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php echo $text_system; ?></span></a>
    <ul>
      <?php if (array_search('setting/setting', $permission_access) != false){ ?>
      <li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li>
      <?php } ?>
      <?php if ((array_search('user/user', $permission_access) != false)||(array_search('user/api', $permission_access) != false)||
            (array_search('user/user_permission', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_users; ?></a>
        <ul>
          <?php if (array_search('user/user', $permission_access) != false){ ?>
          <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
          <?php } ?>
          <?php if (array_search('user/user_permission', $permission_access) != false){ ?>
          <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
          <?php } ?>
          <?php if (array_search('user/api', $permission_access) != false){ ?>
          <li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if ((array_search('localisation/country', $permission_access) != false)||(array_search('localisation/currency', $permission_access) != false)||(array_search('localisation/geo_zone', $permission_access) != false)||(array_search('localisation/language', $permission_access) != false)||(array_search('localisation/length_class', $permission_access) != false)||(array_search('localisation/location', $permission_access) != false)||(array_search('localisation/order_status', $permission_access) != false)||(array_search('localisation/return_action', $permission_access) != false)||(array_search('localisation/return_reason', $permission_access) != false)||(array_search('localisation/return_status', $permission_access) != false)||(array_search('localisation/stock_status', $permission_access) != false)||(array_search('localisation/tax_class', $permission_access) != false)||(array_search('localisation/tax_rate', $permission_access) != false)||(array_search('localisation/weight_class', $permission_access) != false)||(array_search('localisation/zone', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_localisation; ?></a>
        <ul>
          <?php if (array_search('localisation/location', $permission_access) != false){ ?>
          <li><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/language', $permission_access) != false){ ?>
          <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/currency', $permission_access) != false){ ?>
          <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/stock_status', $permission_access) != false){ ?>
          <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/order_status', $permission_access) != false){ ?>
          <li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li>
          <?php } ?>
          <?php if ((array_search('localisation/return_status', $permission_access) != false)||(array_search('localisation/return_action', $permission_access) != false)||(array_search('localisation/return_reason', $permission_access) != false)){ ?>
          <li><a class="parent"><?php echo $text_return; ?></a>
            <ul>
              <?php if (array_search('localisation/return_status', $permission_access) != false){ ?>
              <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li>
              <?php } ?>
              <?php if (array_search('localisation/return_action', $permission_access) != false){ ?>
              <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li>
              <?php } ?>
              <?php if (array_search('localisation/return_reason', $permission_access) != false){ ?>
              <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li>
              <?php } ?>
            </ul>
          </li>
          <?php } ?>
          <?php if (array_search('localisation/country', $permission_access) != false){ ?>
          <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/zone', $permission_access) != false){ ?>
          <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/geo_zone', $permission_access) != false){ ?>
          <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
          <?php } ?>
          <?php if ((array_search('localisation/tax_class', $permission_access) != false)||(array_search('localisation/tax_rate', $permission_access) != false)){ ?>
          <li><a class="parent"><?php echo $text_tax; ?></a>
            <ul>
              <?php if (array_search('localisation/tax_class', $permission_access) != false){ ?>
              <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
              <?php } ?>
              <?php if (array_search('localisation/tax_rate', $permission_access) != false){ ?>
              <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li>
              <?php } ?>
            </ul>
          </li>
          <?php } ?>
          <?php if (array_search('localisation/length_class', $permission_access) != false){ ?>
          <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
          <?php } ?>
          <?php if (array_search('localisation/weight_class', $permission_access) != false){ ?>
          <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if ((array_search('tool/upload', $permission_access) != false)||(array_search('tool/backup', $permission_access) != false)||(array_search('tool/import', $permission_access) != false)||(array_search('tool/error_log', $permission_access) != false)||(array_search('tool/search_history', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_tools; ?></a>
        <ul>
          <?php if (array_search('tool/upload', $permission_access) != false){ ?>
          <li><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li>
          <?php } ?>
          <?php if (array_search('tool/backup', $permission_access) != false){ ?>
          <li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
          <?php } ?>
          <?php if (array_search('tool/import', $permission_access) != false){ ?>
          <li><a href="<?php echo $import; ?>"><?php echo $text_import; ?></a></li>
          <?php } ?>
          <?php if (array_search('tool/error_log', $permission_access) != false){ ?>
          <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
          <?php } ?>
          <?php if (array_search('tool/search_history', $permission_access) != false){ ?>
          <li><a href="<?php echo $search_history; ?>"><?php echo $text_search_history; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
  <?php if ((array_search('report/affiliate', $permission_access) != false)||(array_search('report/affiliate', $permission_access) != false)||(array_search('report/affiliate_login', $permission_access) != false)||(array_search('report/customer_activity', $permission_access) != false)||(array_search('report/customer_credit', $permission_access) != false)||(array_search('report/customer_login', $permission_access) != false)||(array_search('report/customer_online', $permission_access) != false)||(array_search('report/customer_order', $permission_access) != false)||(array_search('report/customer_reward', $permission_access) != false)||(array_search('report/marketing', $permission_access) != false)||(array_search('report/product_purchased', $permission_access) != false)||(array_search('report/product_viewed', $permission_access) != false)||(array_search('report/sale_coupon', $permission_access) != false)||(array_search('report/sale_order', $permission_access) != false)||(array_search('report/sale_return', $permission_access) != false)||(array_search('report/sale_shipping', $permission_access) != false)||(array_search('report/sale_tax', $permission_access) != false)){ ?>
  <li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
    <ul>
      <?php if ((array_search('report/sale_order', $permission_access) != false)||(array_search('report/sale_tax', $permission_access) != false)||(array_search('report/sale_shipping', $permission_access) != false)||(array_search('report/sale_return', $permission_access) != false)||(array_search('report/sale_coupon', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_sale; ?></a>
        <ul>
          <?php if (array_search('report/sale_order', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/sale_tax', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/sale_shipping', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/sale_return', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/sale_coupon', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if ((array_search('report/product_viewed', $permission_access) != false)||(array_search('report/product_purchased', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_product; ?></a>
        <ul>
          <?php if (array_search('report/product_viewed', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/product_purchased', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if ((array_search('report/customer_online', $permission_access) != false)||(array_search('report/customer_activity', $permission_access) != false)||(array_search('report/customer_order', $permission_access) != false)||(array_search('report/customer_reward', $permission_access) != false)||(array_search('report/customer_credit', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_customer; ?></a>
        <ul>
          <?php if (array_search('report/customer_online', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_customer_online; ?>"><?php echo $text_report_customer_online; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/customer_activity', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_customer_activity; ?>"><?php echo $text_report_customer_activity; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/customer_order', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/customer_reward', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/customer_credit', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <?php if ((array_search('report/marketing', $permission_access) != false)||(array_search('report/affiliate', $permission_access) != false)||(array_search('report/affiliate_activity', $permission_access) != false)){ ?>
      <li><a class="parent"><?php echo $text_marketing; ?></a>
        <ul>
          <?php if (array_search('report/marketing', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_marketing; ?>"><?php echo $text_marketing; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/affiliate', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_affiliate; ?>"><?php echo $text_report_affiliate; ?></a></li>
          <?php } ?>
          <?php if (array_search('report/affiliate_activity', $permission_access) != false){ ?>
          <li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
    </ul>
  </li>
  <?php } ?>
</ul>
